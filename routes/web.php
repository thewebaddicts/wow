<?php

use App\Http\Controllers\AboutUsController;
use App\Http\Controllers\GuestController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\NewsPagecontroller;
use App\Http\Controllers\DetailsPageController;
use App\Http\Controllers\CookiesController;
use App\Http\Controllers\PrivacyPolicyController;
use App\Http\Controllers\TermsandConditionsController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\BrandsController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\VoucherController;
use App\Http\Controllers\CartController;
use App\Models\BrandsModel;
use App\Models\SlideshowModel;
use twa\ecomauth\facades\EcomUsersRegistrationFacade;
use twa\ecomprofile\facades\EcomAddressesFacade;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(["middleware" => [\App\Http\Middleware\SEO::class]], function () {
    Route::middleware(twa\ecomgeneral\middleware\storeDefaultRouteMiddleware::class)->get('/', function () {
//   return view('pages.corporate.default');
    });

    Route::group([
        'prefix' => '/{store_prefix}/{language_prefix}/',
        'middleware' => [twa\ecomgeneral\middleware\storeRoutesMiddleware::class, twa\ecomgeneral\middleware\storeDefaultDataMiddleware::class],
    ], function () {
        Route::get('/mili', function () {

            \twa\ecomproducts\controllers\EcomProductsSearchController::generateJson();

        });
        Route::get('/', [HomepageController::class, 'render'])->name('home');
        Route::get('about', [AboutUsController::class, 'render'])->name('about');
//    Route::get('/balloons/list/{id}/{slug}', function($store_prefix,$lang,$id,$slug){ return view('pages.page',
//        [ 'collections' => ecom('collections')->condition('display_homepage', 1)->condition('id',$id)->condition('IncludeProducts', 1)->get()
//            , 'products' => ecom('products')->condition('display', "card")->includeFilters()->addFilters(request()->input('filter', []))->get()->byCollection($id)]);})->name('products-collection');


        Route::get('/products/{slug}', [AboutUsController::class, 'renderBalloons'])->name('balloon');

//
        Route::get('/balloons', [AboutUsController::class, 'renderBalloons'])->name('balloon');
        Route::get('/events', [AboutUsController::class, 'renderEvents'])->name('events');
        Route::get('/events/get-a-quote', [AboutUsController::class, 'getaquote'])->name('events');


        Route::get('/product/{slug}',
            function ($store_prefix, $lang, $slug) {
                return ecom('products')->addRecommendations()->includeFilters()->addPeopleAlsoBought()->condition('slug', $slug)->condition('display', "full")->condition('includeVariationDetails', 1)->condition('forceVariationSorting', 1)->condition('ecomLinkStructure', '{store}/{lang}/product/{slug}')->get()->render();
            })->name('product');


        Route::get('/arrangement/{id}/{slug}/',
            function ($store_prefix, $lang, $slug, $id) {
                return view('pages.products.arrangement');
            })->name('product');


        Route::post('boxes/your-box', [AboutUsController::class, 'postcreateBox'])->name('postbox');
        Route::get('boxes/create-your-box', [AboutUsController::class, 'createBox'])->name('your-box');
//    Route::get('/boxes/your-box/{category}/{id}/{slug}/', [AboutUsController::class, 'createBoxid'])->name('your-box-id');
        Route::get('/boxes/your-box/{category}/{slug}/{variationid}', function ($store, $lang, $category, $id) {
            return view('pages.box.your-box-id', ["category" => $category,
                "box" => ecom('products')->condition('display', 'card')->condition('ignoreListing', 0)->condition('includeVariationDetails', 0)->condition('cancelled', 0)->addField('weight')->get()->toArray(), "id" => $id]);
        })->name('build-your-box');


        Route::get('/boxes/your-box/wrapping-pappers/', function ($store, $lang) {
            return view('pages.box.wrap');
        })->name('choose-balloon');

//    Route::get('/boxes/your-box/gift-card/', function ($store, $lang) {
//        return view('pages.box.gift-card');
//    })->name('choose-gift-card');

//
//    Route::get('/register', function () {
//        if (session('login'))
//            return redirect()->route('account');
//        return view('pages.login');
//    })->name('register');


        Route::post('/contact-us', [ContactUsController::class, 'store'])->name('contact-us');
        Route::get('/search', function ($store, $lang) {
            return view('pages.products.search',
                ["balloons" => ecom('products')->condition('cancelled', 0)->ByTerm(request()->input('term'))]);
        })->name('search');

        Route::get('/login/google', function () {
            return ecom('users')->SocialLogin('google');
        })->name('login.google');

        Route::get('/login/facebook', function () {
            return ecom('users')->SocialLogin('facebook');
        })->name('login.facebook');

        Route::get('/terms-and-conditions', [TermsandConditionsController::class, 'render'])->name('termsandconditions');
        Route::get('/privacy-policy', [PrivacyPolicyController::class, 'render'])->name('privacypolicy');
        Route::get('/cookies', [CookiesController::class, 'render'])->name('cookies');

        Route::get('account/login', function () {
            if (session('login'))
                return redirect()->route('account-profile');
            return view('pages.login');
        })->name('login');

        Route::get('/checkout', [\App\Http\Controllers\CheckoutController::class, 'checkout'])->name('checkout');
        Route::get('/checkout/placeorder', [\App\Http\Controllers\CheckoutController::class, 'checkoutpayment'])->name('billing');

        Route::post('/login', [EcomUsersRegistrationFacade::class, 'login'])->name('loginpost');
        Route::get('/login', [EcomUsersRegistrationFacade::class, 'login'])->name('loginpost');

        Route::post('/register', [EcomUsersRegistrationFacade::class, 'store'])->name('register');

//    Route::get('account/login', [AboutUsController::class, 'login']);
        Route::group(['middleware' => [twa\ecomauth\middleware\AuthenticationRequiredCheck::class]], function () {
            Route::post('/profile/edit', [\App\Http\Controllers\UsersController::class, 'updateProfile'])->name('profile-edit');
            Route::get('/profile/edit', [\App\Http\Controllers\UsersController::class, 'updateProfile'])->name('account-profile');
            Route::get('/change/password', [\App\Http\Controllers\UsersController::class, 'changePassword'])->name('account-profile');
            Route::post('/change/password', [\App\Http\Controllers\UsersController::class, 'changePassword'])->name('changePassword');
            Route::get('account', [AboutUsController::class, 'account'])->name('account-profile');
            Route::get('addresses', [AboutUsController::class, 'addresses'])->name('addresses');
            Route::get('addresses/create', [AboutUsController::class, 'addressesCreate'])->name('addresses-create');
            Route::get('/account/addresses/delete/{id}', [EcomAddressesFacade::class, 'deleteAddress'])->name('delete-address');
            Route::get('/account-add-address ', [EcomAddressesFacade::class, 'renderAdd']);
            Route::get('account/addresses/add', [EcomAddressesFacade::class, 'renderAdd'])->name('account-addresses');
            Route::post('account/addresses/add', [EcomAddressesFacade::class, 'saveAddress']);

        });
        ///
        ///
        ///

        Route::get('/forgot', function () {
            return view('pages.forgot');
        })->name('forgot');
        Route::post('/forgot', [EcomUsersRegistrationFacade::class, 'forgot'])->name('forgot-password');
        //add address ecom

        //
        Route::post('/checkout-order', function () {
            return (new \twa\ecomgeneral\helpers\purchasesHelper)->placeOrder();
        })->name('checkout-placeorder');


        Route::group(['middleware' => [App\Http\Middleware\CheckNoAuthentication::class]], function () {
            Route::get('/guest/checkout', function () {
                return view('pages.checkout.guest.checkout-1');
            })->name('guest-checkout-1');
            Route::post('/guest/checkout', [GuestController::class, 'store'])->name('store-guest-checkout');
            Route::get('/guest/checkout/place/order', function () {
                return view('pages.checkout.guest.checkout-2');
            })->name('guest-checkout-complete');
            Route::post('/guest/checkout/place/order', [GuestController::class, 'placeOrder'])->name('place-order-guest');

        });

        Route::post('/voucher/apply', [VoucherController::class, 'applyVoucher'])->name('apply-voucher');
        Route::post('/voucher/remove', [VoucherController::class, 'removeVoucher']);

        Route::get('saved_products', [AboutUsController::class, 'savedProducts'])->name('saved_products');

        Route::get('orders', [AboutUsController::class, 'orders'])->name('orders');
        Route::post('/newsletter', [\App\Http\Controllers\HomeController::class, 'subscribeEcom'])->name('newsletter');
        Route::get('/logout', [EcomUsersRegistrationFacade::class, 'logout'])->name('logout');
//    Route::get('boxes/your-box/{id}', [AboutUsController::class, 'createBoxidcustomize'])->name('your-box-id-customize');
        Route::group(['middleware' => [twa\ecomgeneral\middleware\storeDefaultDataMiddleware::class], 'prefix' => '/cart'], function () {

            Route::get('/', function () {
                return ecom('cart')->render();
            })->name('cart');

            Route::post('/update-quantity', [CartController::class, 'updateQuantity']);
            Route::post('/remove', [CartController::class, 'removeItem']);
            Route::get('/dropdown', function () {
                return view('iframes.cart');
            })->name('cart-dropdown');
            Route::get('/get-summary', [CartController::class, 'getCartSummary']);
            Route::get('/get-details', [CartController::class, 'getCartDetails']);
        });
    });
});
