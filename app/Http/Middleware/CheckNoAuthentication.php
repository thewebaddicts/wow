<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;


class CheckNoAuthentication
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        $myIntendedURL = $_SERVER['REQUEST_URI'];
        session()->put('url.intended', $myIntendedURL);

        if(isset(request()->user['login']) && request()->user['login'] != false){

            return redirect(ecom('url')->prefix()."/home");
        }else{

            return $next($request);

        }
    }
}
