<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use twa\ecomauth\controllers\EcomGuestController;
use twa\ecomcart\controllers\EcomCartController;

class GuestController extends Controller
{
    public static function settingAddress($name, $type, $id = null)
    {

        $name->id = $id;
        $name->store = request()->store['id'];
        $name->label = request()->input($type . '_label');
        $name->first_name = request()->input($type . '_first_name');
        $name->last_name = request()->input($type . '_last_name');
        $name->country_code = request()->input($type . '_country');
        $name->state = request()->input($type . '_state');
        $name->city = request()->input($type . '_city');
        $name->street = request()->input($type . '_street');
        $name->floor = request()->input($type . '_floor');
        $name->building = request()->input($type . '_building');
        $name->phone_country_code = request()->input($type . '_phone_country_code');
        $name->phone = request()->input($type . '_phone');
        $name->postal_code = request()->input($type . '_zip_code');
        $name->delivery_note = $type == 'shipping' ? request()->input('delivery_note') : null;

        return $name;

    }

    public static function storeAddressDB($address, $user_id)
    {

        $addressesClass = new \twa\ecomprofile\controllers\EcomUsersAddressesController;

        $address_array = json_decode(json_encode($address), 1);
        $Address = $addressesClass->store($address_array, $user_id);

        if ($Address->getStatusCode() != 200) {
            $ResponseObject = $Address->getOriginalContent();

            return \twa\ecomgeneral\controllers\HelpersController::processAPIError($ResponseObject);
        }

        $addresses = collect($Address->getOriginalContent()["data"])->recursive();
        $last_address = $addresses->last();

        return response($last_address, 200);
    }

    public static function getAddressInfo($prefix, $user_id, $new = false)
    {

        ${$prefix . '_address'} = new \StdClass;
        if ($new) {
            $id = null;
        } else {
            if (session('info') && session('info')->{$prefix . '_address'})
                $id = session('info')->{$prefix . '_address'}->id;
            else
                $id = null;
        }

        ${$prefix . '_address'} = self::settingAddress(${$prefix . '_address'}, $prefix, $id);

        $response = self::storeAddressDB(${$prefix . '_address'}, $user_id);
        try {
            $status = $response->getStatusCode();
        } catch (\Throwable $e) {
            $status = 400;
        }

        if ($response->getStatusCode() == 200) {
            $address = $response->getOriginalContent();
            ${$prefix . '_address'}->id = $address->id;
            ${$prefix . '_address'}->country_name = $address->country->name;
            ${$prefix . '_address'}->country_id = $address->countries_id;
        } else {
            return $response;
        }

        return ${$prefix . '_address'};
    }

    public static function store($store_prefix, $language_prefix, Request $request)

    {

        $validator = Validator::make(request()->all(),
            [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'phone' => 'required',
            ])->validate();

        //Check if in session user id, do not create a new one
        if (session('info') && session('info')->user) {

            $user = session('info')->user;

        } else {

            $user = (new EcomGuestController)->store(["first_name" => request()->input('first_name'), "last_name" => request()->input('last_name'), "email" => request()->input('email'), "phone" => request()->input('phone'), "phone_country_code" => request()->input('phone_country_code'),]);
            if (request()->header('store')) {
                $store = request()->header('store');
            } else {
                $store = request()->store['id'];
            }
            $Cart = (new \twa\ecomcart\controllers\EcomCartController)->getCartModel(["store" => $store,]);

            $Cart->ecom_users_id = $user->id;
            $Cart->save();
        }

        $info = new \StdClass;

        //Set User Info

        $info->user = $user;

        //Set User Shipping Address
        $shipping_address = self::getAddressInfo('shipping', $user->id);

        $info->shipping_address = $shipping_address;

        //Set User Billing Address

        if ($request->input('billing_address_type') == 'same') {

            $info->billing_address = $info->shipping_address;
        } else {

            $billing_address = self::getAddressInfo('billing', $user->id, true);
            $info->billing_address = $billing_address;
        }

        //Shipping Method - Static because we only have DHL and DHL doesn't support domestic delivery
        //  $shipping_methods = ecom('shipping_methods')->list();
        if (isset($info->shipping_address->country_id)) {


            if ($info->shipping_address->country_id == 230) {

                $info->shipping_method = 2;
            } else {

                $info->shipping_method = 1;
            }
            //Payment Method - Only have Credit Card

            $payment_methods = ecom('payment_methods')->list();
            $info->payment_method = intval($request->input('payment_method'));
            $info->billing_same = $request->input('billing_address_type');

            session(['info' => $info]);

            return redirect()->route('guest-checkout-complete', ['shipping_address' => $info->shipping_address->id, 'billing_address' => $info->billing_address->id, 'shipping_method' => $info->shipping_method, 'payment_method' => $info->payment_method]);
        } else {
            return redirect(ecom('url')->prefix().'/guest/checkout'.'?notification_title=Incorrect phone number'.'&notification_message=You have entered an incorrect phone number');



        }
    }

    public function placeOrder($store_prefix, $language_prefix, Request $request)
    {
        $guest_id = session('info')->user->id;
        return ecom('purchases')->placeOrder($guest_id);
    }


}

