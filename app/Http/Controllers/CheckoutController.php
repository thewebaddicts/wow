<?php

namespace App\Http\Controllers;
use App\Models\FooterCarouselModel;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public static function checkout()
    {

        if (session('login'))
        return view('pages.checkout.checkout');
        else
            return redirect()->route('login',["notification_id" => 1016]);

    }
    public static function checkoutpayment()
    {
        return view('pages.checkout.placeorder');
    }
    public static function checkoutordersummary()
    {

        return view('pages.checkout.checkout-order');
    }
}
