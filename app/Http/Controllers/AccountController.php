<?php

namespace App\Http\Controllers;
use App\Models\FooterCarouselModel;
use App\Models\PageProductModel;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public static function accountPage()
    {
        return view('pages.account');
    }
    public static function render()
    {
    return view('pages.account-address');
}
    public static function accountWishlist()
    {
        return view('pages.account-wishlist');
    }
    public static function accountOrder()
    {
        return view('pages.account-order');
    }
    public static function accountOrderDetails()
    {
        return view('pages.account-order-details');
    }
    public static function accountaddAddress()
    {
        return view('pages.account-add-address');
    }
}
