<?php

namespace App\Http\Controllers;
use App\Models\FooterCarouselModel;
use App\Models\AdditionallinksModel;
use Illuminate\Http\Request;

class DetailsPageController extends Controller
{
    public static function render($store_prefix,$lang,$id,$slug)
    {
        
    $news = AdditionallinksModel::where('cancelled',0)->find($id);

    return view('pages.article', ['news' => $news]);
}
}