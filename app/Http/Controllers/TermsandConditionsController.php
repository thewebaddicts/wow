<?php

namespace App\Http\Controllers;

use App\Models\FooterCarouselModel;
use Illuminate\Http\Request;

class TermsandConditionsController extends Controller
{
    public static function render()
    {
        

        $footercarousel = FooterCarouselModel::where('cancelled', 0)->get();
        return view('pages.termsandconditions', ['footercarousel' => $footercarousel]);
    }
}