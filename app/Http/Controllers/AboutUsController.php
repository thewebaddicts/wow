<?php

namespace App\Http\Controllers;

use App\Models\AboutUsModel;

use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public static function render()
    {
        $about = AboutUsModel::where('cancelled', 0)->get();

        return view('pages.about', ['about' => $about]);
    }
//Route::get('/products/list/{id}/{slug}', function ($store_prefix, $lang, $id, $slug) {
//    return view('pages.balloons', ['balloons' => ecom('products')->condition('display', "card")->includeFilters()->addFilters(request()->input('filter', []))->ByMenuQuery($id)]);
//})->name('balloons');

    public static function renderBalloons($store, $lang, $slug)
    {

        $balloons = ecom('products')->condition('display', 'card')->condition('cancelled', 0)->condition('display', '1')->setMinPrice(request()->input('filter_price_min'))->setMaxPrice(request()->input('filter_price_max'))->includeFilters()->addFilters(request()->input('filter', []))->byCollectionOrMenuSlug($slug);

        if (request()->sortBy == 'alphaA') {
            $balloons = ecom('products')->condition('cancelled', 0)->condition('display', '1')->condition('order', 'label_asc')->condition('display', "card")->setMinPrice(request()->input('filter_price_min'))->setMaxPrice(request()->input('filter_price_max'))->includeFilters()->addFilters(request()->input('filter', []))->byCollectionOrMenuSlug($slug);
        }
        if (request()->sortBy == 'alphaD') {

            $balloons = ecom('products')->condition('cancelled', 0)->condition('display', '1')->condition('order', 'label_desc')->condition('display', "card")->setMinPrice(request()->input('filter_price_min'))->setMaxPrice(request()->input('filter_price_max'))->includeFilters()->addFilters(request()->input('filter', []))->byCollectionOrMenuSlug($slug);
        }
        if (request()->sortBy == 'priceA') {
            $balloons = ecom('products')->condition('cancelled', 0)->condition('order', 'price_asc')->condition('display', '1')->condition('display', "card")->setMinPrice(request()->input('filter_price_min'))->setMaxPrice(request()->input('filter_price_max'))->includeFilters()->addFilters(request()->input('filter', []))->byCollectionOrMenuSlug($slug);

        }
        if (request()->sortBy == 'priceD') {

            $balloons = ecom('products')->condition('cancelled', 0)->condition('order', 'price_desc')->condition('display', '1')->condition('display', "card")->setMinPrice(request()->input('filter_price_min'))->setMaxPrice(request()->input('filter_price_max'))->includeFilters()->addFilters(request()->input('filter', []))->byCollectionOrMenuSlug($slug);

        }


        return view('pages.balloons', ['balloons' => $balloons]);

    }


    public static function renderEvents()
    {
        $events = AboutUsModel::where('cancelled', 0)->get();

        return view('pages.events', ['events' => $events]);
    }

    public static function createBox()
    {
//        $boxes = ecom('products')->condition('display', 'card')->condition('cancelled', 0)->condition('display', '1')->setMinPrice(request()->input('filter_price_min'))->setMaxPrice(request()->input('filter_price_max'))->includeFilters()->addFilters(request()->input('filter', []))->get();

//    $boxes = ecom('products')->condition('cancelled', 0)->condition('display', "full")->condition('display', 1)->condition('includeVariationDetails',1)->get()->toArray();

        return view('pages.box.your-box');
    }

    public static function postcreateBox()
    {


        foreach (request()->input('cms_attributes_id') as $id) {

            if (request()->input('cms_attributes_quantity' . $id) > 0) {

                $pro = ecom('products')->condition('id', $id)->get();

                $arr[] = ['id' => $id,
                    'quantity' => request()->input('cms_attributes_quantity' . $id),
                    'sku' => $pro->products['data'][0]['sku']];
            }
        }
        $response = ecom('cart')->addToCart(request()->input('product_ids'), request()->input('quantity'), $arr);
        foreach (request()->input('cms_attributes_id') as $id) {
            if (request()->input('cms_attributes_quantity' . $id) > 0) {
                $modifierid = (int)request('modifierid');
                $jsonmodifier[] = ["id" => $modifierid,
                    "message" => request('customization_message_' . $id)
];
                $modifier= json_encode($jsonmodifier);

                $response = ecom('cart')->addToCart($id, request()->input('cms_attributes_quantity' . $id), 'related', $modifier);
            }
        }
        return redirect(ecom('url')->prefix() . '/cart');
    }

    public static function createBoxid()
    {


        return view('pages.box.your-box-id');
    }

    public static function getaquote()
    {


        return view('pages.getaquote');
    }

    public static function login()
    {


        return view('pages.login');
    }

    public static function account()
    {


        return view('pages.account');
    }

    public static function addresses()
    {
        return view('pages.addresses');
    }

    public static function addressesCreate()
    {
        return view('pages.addresses-create');
    }

    public static function savedProducts()
    {
        if (session('login')) {
            return view('pages.saved-products');
        } else {
            return redirect(route('login'));
        }
    }

    public static function orders()
    {
        return view('pages.orders');
    }

}

