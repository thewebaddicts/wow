<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{

    public function index()
    {
        try {
            session()->forget('info');
            session()->forget('otp_verified');
        } catch (\Throwable $e) {

        }


        return ecom('cart')->render();
    }

    public function getCartSummary()
    {

        $cartList = ecom('cart')->getAsObject();

        return response()->json([
            'status' => true,
            'view' => view('components.checkout.order-summary', ['cartList' => $cartList, 'type' => 'checkout'])->render()
        ], 200);

    }

    public function getCartDetails()
    {
        $cartList = ecom('cart')->getAsObject();

        return response()->json([
            'status' => true,
            'view' => view('pages.cart.details', ['cartList' => $cartList])->render()
        ], 200);
    }

    public function updateQuantity()
    {

        $cartList = ecom('cart')->updateQuantity(request()->cartItemID, request()->direction);

        $listing = view('pages.cart.listing', ['cartList' => $cartList])->render();
        $breakdown = view('pages.cart.details', ['cartList' => $cartList])->render();
        $total = $cartList->count;

        return response()->json(['status' => true, 'link' => route('cart'), 'listing' => $listing, 'breakdown' => $breakdown, 'total' => $total], 200);
    }

    public function removeInvalidItems()
    {
        $cartList = ecom('cart')->removeInvalidItems(request()->invalid_items);

        $listing = view('pages.cart.listing', ['cartList' => $cartList])->render();
        $breakdown = view('pages.cart.details', ['cartList' => $cartList])->render();
        $total = $cartList->count;

        return response()->json(['status' => true, 'link' => route('cart'), 'listing' => $listing, 'breakdown' => $breakdown, 'total' => $total], 200);

    }

    public function updateProductQuantity()
    {
        ecom('cart')->updateCartItem(request()->cartItemID, ['quantity' => request()->quantity]);

        $cartList = ecom('cart')->getAsObject();

        $listing = view('pages.cart.listing', ['cartList' => $cartList])->render();
        $breakdown = view('pages.cart.details', ['cartList' => $cartList])->render();
        $total = $cartList->count;


        return response()->json(['status' => true, 'link' => route('cart'), 'listing' => $listing, 'breakdown' => $breakdown, 'total' => $total], 200);
    }

    public function removeItem()
    {
        $cartList = ecom('cart')->removeItem(request()->cartItemID);
        $listing = view('pages.cart.listing', ['cartList' => $cartList])->render();
        $breakdown = view('pages.cart.details', ['cartList' => $cartList])->render();
        $total = $cartList->count;
        return response()->json(['status' => true, 'link' => route('cart'), 'listing' => $listing, 'breakdown' => $breakdown, 'total' => $total], 200);
    }


    public static function GenerateBuildYourBoxLabel($item)
    {
        if (!is_array($item->cms_attributes)) {
            return false;
        }
        $WhatsInTheBoxIDs = array_keys($item->cms_attributes['selection']);
        $items = DB::table('ecom_products_box_ingredients')->select('id', 'label', 'sku')->whereIn('id', $WhatsInTheBoxIDs)->where('cancelled', 0)->get();
        $return = [];
        foreach ($items as $row) {
            $return[] = "<div class='selection-".$row->id."'>".$item->cms_attributes['selection'][$row->id] . "x " . $row->label . " (" . $row->sku . ") </div>";
        }

        return implode('', $return);
    }

    public static function CustomValidation($Items, $validation)
    {


        $Items = collect($Items)->filter(function ($item) {
            if (isset($item["cms_attributes"]["type"]) && $item["cms_attributes"]["type"] == "buildyourbox") {
                return true;
            }
        });


        $PathItems = clone $Items;


        $Items = $Items->pluck('cms_attributes.selection');


        $NewArray = [];
        foreach ($Items as $item) {
            foreach ($item as $key => $value) {
                if (isset($NewArray[$key])) {
                    $NewArray[$key] += $value;
                } else {
                    $NewArray[$key] = (int)$value;
                }
            }
        }

        $invalid_ingredients = [];
        $keys = array_keys($NewArray);

        $ingredients = DB::table('ecom_products_box_ingredients')->select('id', 'in_stock', 'stock')->whereIn('id', $keys)->where('cancelled', 0)->get();




        foreach ($ingredients as $ingredient) {
            $diff = $NewArray[$ingredient->id] -  $ingredient->stock;
            if ($ingredient->in_stock != 1 || $diff > 0) {
                $validation["boxes_stock_invalid"] [$ingredient->id] = $diff;
            }
        }


        foreach ($PathItems as $item) {
            $path = $item->cms_attributes['path'];
            $options = $item->cms_attributes['selection'];
            $params = ['category' => $path['category'], 'id' => $path['product_id'], 'slug' => $path['slug']];
            $invalid = false;
            foreach ($options as $key => $option) {
                $params['quantity[' . $key . ']'] = $option;
                if (isset($validation['boxes_stock_invalid'][$key])) {
                    $params['quantity_invalid[' . $key . ']'] = $validation["boxes_stock_invalid"] [$key];
                    $invalid= true;
                }
            }

            $params['cart_item_id'] = $item->id;

            if($invalid){
                $validation["boxes_stock_update_link"] [$item->id] = route('build-your-box', $params);
            }

        }

        return $validation;

    }


    public static function GenerateCustomLink($item, $validation = null)
    {

        if (self::GetProductType($item) == 'product') {
            return "";
        }

        if(isset($validation['boxes_stock_update_link'][$item->id])){
            return $validation['boxes_stock_update_link'][$item->id];
        }


        $path = $item->cms_attributes['path'];
        $options = $item->cms_attributes['selection'];
        $params = ['category' => $path['category'], 'id' => $path['product_id'], 'slug' => $path['slug']];
        foreach ($options as $key => $option) {
            $params['quantity[' . $key . ']'] = $option;
        }
        $params['cart_item_id'] = $item->id;

        return route('build-your-box', $params);

    }

    public static function GetProductType($item){
        $item = json_decode(json_encode($item));
        if (!isset($item->cms_attributes->type) || $item->cms_attributes->type != "buildyourbox") {
            return "product";
        }

        return "buildyourbox";
    }

    public static function getAvailableQuantities($Items){

        $Items = json_decode(json_encode($Items),1);



        $Items = collect($Items)->filter(function ($item) {
            if (isset($item["cms_attributes"]["type"]) && $item["cms_attributes"]["type"] == "buildyourbox") {
                return true;
            }
        });

        $Items = $Items->pluck('cms_attributes.selection');



        $NewArray = [];
        foreach ($Items as $item) {
            foreach ($item as $key => $value) {
                if (isset($NewArray[$key])) {
                    $NewArray[$key] += $value;
                } else {
                    $NewArray[$key] = (int)$value;
                }
            }
        }

        return $NewArray;

    }

}
