<?php

namespace App\Http\Controllers;

use App\Mail\emailtemplate;
use App\Models\FooterCarouselModel;
use App\Models\ContactFormModel;
use App\Models\EmailTemplateModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    public static function render()
    {
        $footercarousel = FooterCarouselModel::where('cancelled', 0)->get();
        return view('pages.contactus', ['footercarousel' => $footercarousel]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'date' => 'required',
            'time' => 'required',
            'address' => 'required',
            'subject' => 'required',


        ]);

        $form = new ContactFormModel();
        $form->name = request()->input('name');
        $form->last_name = request()->input('last_name');
        $form->email = request()->input('email');
        $form->event_details = request()->input('subject');
        $form->phone_number = request()->input('phone_country_code') . ' ' . request()->input('phone');
        $form->city = request()->input('city');
        $form->date = request()->input('date');
        $form->time = request()->input('time');
        $form->state = request()->input('state');
        $form->event_type = request()->input('type-event');
        $form->address = request()->input('address');
        $form->save();
        $template = EmailTemplateModel::where('cancelled', 0)->where('location', 'contact_corporate')->first();

        if ($template) {
            $dictionary = json_decode(json_encode([
                "title" => 'Contact form',
                "form_data" => [
                    "First Name" => $request->input('name'),
                    "Last Name" => $request->input('last_name'),
                    "Email" => $request->input('email'),
                    "Subject" => $request->input('subject'),
                    "Phone" => $request->input('phone'),
                    "City" => $request->input('city'),
                    "State" => $request->input('state'),
                    "Event-type" => $request->input('type-event')

                ]
            ]));


            $company_email = explode(',', $template->email);

            foreach ($company_email as $email) {

                Mail::to(request()->input('email_contact'))->queue(new emailtemplate("contact_corporate_client", $dictionary));
                Mail::to($email)->send(new emailtemplate('contact_corporate', $dictionary));
            }
        }

        return redirect()->route('events', ["notification_id" => 1015]);
    }
}
