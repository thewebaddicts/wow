<?php

namespace App\Http\Controllers;
use App\Models\FooterCarouselModel;
use App\Models\BrandsModel;
use Illuminate\Http\Request;

class BrandsController extends Controller
{
    public static function render()
    {

        $alphabets=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','components','t','u','v','w','x','y','z'];
        if(request()-> sortBy == 'alphaA') {
            foreach ($alphabets as $letter) {
                $brands_letter = BrandsModel::where('cancelled', 0)->where('label', 'LIKE', $letter . '%')->where('group', 'brand', 'Brand')->get();
                $brands[$letter] = $brands_letter;
            }
        }
        elseif(request()-> sortBy == 'alphaD') {
            foreach (array_reverse($alphabets) as $letter) {
                $brands_letter = BrandsModel::where('cancelled', 0)->where('label', 'LIKE', $letter . '%')->where('group', 'brand', 'Brand')->get();
                $brands[$letter] = $brands_letter;
            }
        }
        else
            foreach ($alphabets as $letter) {
                $brands_letter = BrandsModel::where('cancelled', 0)->where('label', 'LIKE', $letter . '%')->where('group', 'brand', 'Brand')->get();
                $brands[$letter] = $brands_letter;
            }
        return view('pages.brands' , ['brands'=>$brands]);

    }
}