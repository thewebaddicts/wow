<?php

namespace App\Http\Controllers;

use App\Models\StoreNewsletterModel;
use App\Models\Notifications;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function subscribeEcom(Request $request)
    {
        $voucher = 'first';
        $cartList = ecom('cart')->applyVoucher($voucher);

        $store = ecom('stores')->getCurrent();
        $form = new StoreNewsletterModel();
        $form->email = request()->input('email');
        $form->store_id = $store->id;
        $form->save();


        return redirect()->route('home', ["notification_id" => 1001]);


    }
}
