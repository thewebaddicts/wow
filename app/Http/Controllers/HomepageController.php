<?php

namespace App\Http\Controllers;

use App\Models\MainproductModel;
use Illuminate\Http\Request;
use App\Models\SlideshowModel;
use App\Models\ShopbycategoryModel;
use App\Models\Banner;
use App\Models\Events;

use App\Models\FooterCarouselModel;
use App\Models\NavMenuModel;
use App\Models\AdditionallinksModel;
class HomepageController extends Controller
{
    public static function render()
    {
        $slideshow = SlideshowModel::where('cancelled', 0)->get();
        $additional_links = AdditionallinksModel::where('cancelled', 0)->get();
        $banner = Banner::where('cancelled', 0)->get();

        $events = Events::where('cancelled', 0)->where('display_home',1)->get();


//        $shopbycategory = ShopbycategoryModel::where('cancelled', 0)->limit(4)->get();
//        $mainproduct = MainproductModel::where('cancelled', 0)->first();
//        $nav_menu = NavMenuModel::where('cancelled', 0)->get();

        return view('pages.home',['slideshows'=>$slideshow,'additional_links'=>$additional_links,'banner'=>$banner,'events'=>$events]);
    }

}

