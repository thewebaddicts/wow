<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    public static function updateProfile()
    {

        $Response = (new \twa\ecomauth\controllers\EcomUsersRegistration)->update(["token" => request()->user["user"]->token, 'first_name' => request()->input('first_name'), 'last_name' => request()->input('last_name'), 'email' => request()->input('email'), 'phone' => request()->input('phone'), 'phone_country_code' => request()->input('phone_country_code'), 'phone_original' => request()->input('phone')]);
        $ResponseObject = $Response->getOriginalContent();
        if ($Response->status() == "200") {
            return redirect()->route('account-profile', ['notification_title' => $ResponseObject['success']['title'], 'notification_message' => $ResponseObject['success']['message']]);
        } else {
            return redirect()->route('account-profile', ['notification_title' => $ResponseObject['error']['title'], 'notification_message' => $ResponseObject['error']['message']])->withInput(request()->input())->withErrors($ResponseObject['error']['message']);
        }


    }
    public static function changePassword()
    {
        $validator = Validator::make(request()->all(),
            [
                'current_password' => 'required',
                'password' => 'required|min:7',
                'confirm_password' => ['same:password'],
            ]);

        $Response = (new \twa\ecomauth\controllers\EcomUsersRegistration)->changepass(["token" => request()->user["user"]->token, "oldpassword" => request()->input('current_password'), "password" => request()->input('password')]);


        $ResponseObject = $Response->getOriginalContent();
        if ($Response->status() == "200") {
            return redirect()->route('account-profile', ['notification_title' => $ResponseObject['success']['title'], 'notification_message' => $ResponseObject['success']['message']])->withInput(request()->input())->withErrors($ResponseObject['success']['message']);
        } else {
            return redirect()->route('account-profile', ['notification_title' => $ResponseObject['error']['title'], 'notification_message' => $ResponseObject['error']['message']])->withInput(request()->input())->withErrors($ResponseObject['error']['message']);
        }

    }
}
