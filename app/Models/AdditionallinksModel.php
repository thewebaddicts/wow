<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdditionallinksModel extends Model
{
    protected $table= 'additional_links';

    public function getImageAttribute()
    {
        return  env('DATA_URL') . "/news/" . $this->id . "." . $this->extension_image . "?v=." . $this->version;
    }
}
