<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MainproductModel extends Model
{
    protected $table = 'mainproduct';
    use HasFactory;
}
