<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SlideshowModel extends Model
{
    protected $table = 'slideshow';
    use HasFactory;
}
