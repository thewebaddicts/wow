<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactFormModel extends Model
{
//    protected $table = 'store_contact_form';
    protected $table = 'get_a_quote_contact_form';
    use HasFactory;
}
