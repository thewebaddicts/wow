<?php

return [
    'success' => [
        "action" => "redirect", //can be either redirect or display
        "redirect" => "https://google.com/?success",
        "function" => "myfunction()" //this function is executed upon success
    ],
    'fail' => [
        "action" => "redirect", //can be either redirect or display
        "redirect" => "https://google.com/?success",
    ],
    'default_aramex' => [
        "provider" => "aramex",
        "data" => ["ARAMEX_ACCOUNT_ENTITY" => "BEY", "ARAMEX_ACCOUNT_NUMBER" => "60507042", "ARAMEX_ACCOUNT_PIN" => "331432", "ARAMEX_ACCOUNT_USERNAME" => "anthony@thecrazycity.com", "ARAMEX_ACCOUNT_PASSWORD" => "Anthony12345!", "ARAMEX_ACCOUNT_FULLNAME" => "The Crazy City", "ARAMEX_ACCOUNT_COMPANY" => "The Crazy City", "ARAMEX_ACCOUNT_PHONE" => "81891786", "ARAMEX_ACCOUNT_MOBILE" => "81891786", "ARAMEX_ACCOUNT_FAX" => "", "ARAMEX_ACCOUNT_EMAIL" => "anthony@thecrazycity.com", "ARAMEX_ACCOUNT_ZIPCODE" => "0000", "ARAMEX_ACCOUNT_ADDRESS" => "", "ARAMEX_ACCOUNT_CITY" => "Beirut", "ARAMEX_COUNTRY_CODE" => "LB"]
    ],
    'aramex' => [
        "provider" => "aramex",
        "data" => ["ARAMEX_ACCOUNT_ENTITY" => "BEY", "ARAMEX_ACCOUNT_NUMBER" => "60507042", "ARAMEX_ACCOUNT_PIN" => "331432", "ARAMEX_ACCOUNT_USERNAME" => "anthony@thecrazycity.com", "ARAMEX_ACCOUNT_PASSWORD" => "Anthony12345!", "ARAMEX_ACCOUNT_FULLNAME" => "The Crazy City", "ARAMEX_ACCOUNT_COMPANY" => "The Crazy City", "ARAMEX_ACCOUNT_PHONE" => "81891786", "ARAMEX_ACCOUNT_MOBILE" => "81891786", "ARAMEX_ACCOUNT_FAX" => "", "ARAMEX_ACCOUNT_EMAIL" => "anthony@thecrazycity.com", "ARAMEX_ACCOUNT_ZIPCODE" => "0000", "ARAMEX_ACCOUNT_ADDRESS" => "", "ARAMEX_ACCOUNT_CITY" => "Beirut", "ARAMEX_COUNTRY_CODE" => "LB"]
    ],
];