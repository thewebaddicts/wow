<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'google' => [
        'client_id' => '595461623578-mhuibq00fc19tqmekfkgd9cgdhnhjf02.apps.googleusercontent.com',
        'client_secret' => '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',
        'redirect' => env('APP_URL').'/ecom/users/login/google/callback',
    ],

    'facebook' => [
        'client_id' => '446921354049296',
        'client_secret' => '3e5cb4b23a0c91e30fcbb927de6f34c5',
//        'redirect' => 'https://qareb.com/login/facebook/callback',
        'redirect' => env('APP_URL').'/ecom/users/login/facebook/callback',
    ],

];
