<?php

//return [ 'BNP' => [
//        "provider" => "BNP",
//        "data" => [ ]
//    ]
//];


return [
    'success' => [
        "action" => "redirect", //can be either redirect or display
        "debug" => true,
        "redirect" => "/ae/en?notification_title=Order Successfully Placed&notification_message=Your order was placed successfully. You will receive a confirmation email shortly containing your order brief and your tracking details",
    ],
    'fail' => [
        "action" => "redirect", //can be either redirect or display
        "redirect" => "/ae/en?notification_title=Transaction Failed&notification_message=Please try again later",
    ],
    'BNP' => [
    "provider" => "BNP",
    "data" => ["merchant_identification" => "BNP_DALYA_ECOM_FR_EUR", "hmac_key" => "4Yz(f=H73x)Xm_E9*D2n6!oNMr8[5]bW", "blowfish" => "Bk3!4M?y(9iCTz[7"]
    ],
    'TransactionGlobal' => [
        "provider" => "TransactionGlobal",
        "data" => ["merchant_username" => "111222", "merchant_password" => "secret123", "merchant_key" => "123", "hmac_key_test" => "Yk8?3]bZLo2[!6RgT)i9c4=F(X7pQ5a_",]
    ],
    "simplify1" => [
        "provider" => "simplify",
        "authorization_type" => "redirect",
        "currencies" => ["AED"],
        "data" => [
            "public_key" => "sbpb_NzhkMjZkMjctZGVjZS00MTE5LWE2YjUtNDAxMzMxYTFiZjhk",
            "private_key" => "g0MEDRVc86Gznaz/TmnOmC1iOX6iemuBjLWZM2Mo9Xp5YFFQL0ODSXAOkNtXTToq",
            "multiplier" => 100,
            "logo_url" => "https://patchi.twalab.com/assets/svg/logo.svg",
        ]
    ]
];
