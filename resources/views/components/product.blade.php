<?php $products = ecom('favorites')->get();

?>

@if(isset($products) && count( $products)>0)
    @foreach( $products AS $product)

        @php
            if( ecom('users')->loginCheck()){
                 $favorite_active = ecom('favorites')->check($product['id']);

             }else{

                 $favorite_active = false;
             }
        @endphp
        @if($favorite_active==true)
            <div class="balloons-product">
                <a href="product/{{$product['slug']}}"></a>
                <picture>
                    <img id="expandedImg"
                         src="{{ env('DATA_URL') }}/products/{{ $product['id'] }}.{{ $product['extension_image'] }}?v={{ $product['version'] }}">

                </picture>
                <div>
                    <h2>
                        {{$product['label']}}
                    </h2>


                    <span class="price">
{{$product['unit_price_formatted']}}
                            </span>
                    @if(request()->segment(3)== 'saved_products')
                        <i style="float: right" onclick="wishList(this)" class="fa-heart fa-solid"></i>
                    @endif
                </div>


            </div>
        @endif
    @endforeach
@endif

