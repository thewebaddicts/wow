@php
    $item = json_decode(json_encode(ecom('products')->condition('ignoreListing',1)->condition('id',$item_id)->get()));
    $item = $item->products->data[0];
@endphp

{{--@if(isset($cartList->items->items))--}}
{{--    @foreach($cartList->items->items as $item)--}}
<div class="item-added animation-toast">
    <span>Item successfully added to bag</span>
    <div class="item-flex">


        <picture><img id="expandedImg" src="{{$item->thumb}}">

        </picture>
        <div><span>{{$item->label}}</span><span>{{$item->unit_price_formatted}}</span></div>

<span>{{$quantity}}</span>
    </div>
    <div class="item-btn">
        <a href="/">CONTINUE SHOPPING</a>
        <a href="{{ecom('url')->prefix()}}/cart">VIEW BAG</a>
    </div>
</div>


{{--<div class="pop-up">--}}
{{--  <a>Choose your balloons</a>--}}
{{--</div>--}}
{{--    @endforeach--}}
{{--@endif--}}


{{--{{ $item->quantity = $quantity }}--}}
{{--{{ gtag()->addEvent('add_to_cart',json_decode(json_encode($item),1)) }}--}}
{{--{{ gtag()->track() }}--}}
