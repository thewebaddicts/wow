<div class="main">
    <div class="titles">
        <span class="title">PRODUCT NAME</span>

        <span class="title">QUANTITY</span>
        <span class="title">TOTAL PRICE</span>
    </div>
    <sep></sep>
    <sep></sep>
    @if($cartList->items->items)

        @foreach($cartList->items->items as $item)
            @if($item->cms_attributes!=="related")

                <div class="loop-cart">


                    <div class="main-1">

                        <sep></sep>

                        <div class="row">
                            <picture><img id="expandedImg"
                                          src="{{ env('DATA_URL') }}/products/{{ $item->details->id}}.{{  $item->details->extension_image}}?v={{  $item->details->version }}">
                            </picture>
                            <div class="row-info">
                                <span class="row-info-title">{{$item->details->label}}</span>
                                <span class="price">{{$item->details->unit_price_formatted}}</span>
                                <sep></sep>
                                @if($item->cms_attributes !== null)

                                    <div id="box-items">

                                        @foreach($item->cms_attributes as $product)

                                            <span id="{{$product->quantity}}">{{$product->quantity}}x</span>
                                            <span id="{{$product->sku}}">({{$product->sku}})</span>
                                            <br>


                                        @endforeach
                                    </div>
                                @endif
                                @if($item->modifier_details)
                                    @foreach($item->modifier_details as $modifier)
                                        <div class="customize-items-cart">

                                            <span>{{$modifier->label}}</span>
                                            <span>{{$modifier->message}}</span>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>

                    </div>
                    <div class="main-2">


                        {{--                        <div class="cart-addtocart">--}}
                        {{--                            <form id="mainform" style="display: contents">--}}

                        {{--                                <div><span onmousedown="mouseDown_d()" onmouseup="mouseUp()"--}}
                        {{--                                           onmouseleave="mouseLeave()">-</span>--}}

                        {{--                                    <input type="hidden" name="product_id" value=>--}}
                        {{--                                    <span class="balloons-addtocart-number">          <input type="number" @if($item->details->stock_quantity < 1) disabled @endif step="1" min="1"--}}
                        {{--                                                                                             name="quantity" value="{{$item->quantity}}" title="Qty"--}}
                        {{--                                                                                             class="input-text qty text" size="4" pattern="" inputmode=""></span>--}}

                        {{--                                    <span onmousedown="mouseDown_i()" onmouseup="mouseUp()"--}}
                        {{--                                          onmouseleave="mouseLeave()">+</span>--}}
                        {{--                                </div>--}}


                        {{--                            </form>--}}
                        {{--                        </div>--}}
                        <div class="quantity buttons_added">
                    <span class="quantity-container" style="">
                    <button type="button" @if($item->details->stock_quantity < 1) disabled @endif class="minus"
                            onclick="@isset($item->id) updateCartQuantity('{{$item->id}}' , -1)@endif"><i
                                class="fa fa-minus"></i></button>
                    <input type="text" @if($item->details->stock_quantity < 1) disabled
                           @endif step="1" min="1"
                           name="quantity" value="{{$item->quantity}}" title="Qty"
                           class="input-text qty text" size="4" pattern="" inputmode="">
                    <button type="button" @if($item->details->stock_quantity < 1) disabled @endif class="plus"
                            onclick="@isset($item->id) updateCartQuantity('{{$item->id}}' , 1) @endif"><i
                                class="fa fa-plus"></i></button>

                    </span>
                        </div>


                    </div>
                    <div class="main-3">


                        <span class="price">{{$item->final_price_formatted}}</span>

                    </div>
                    <span class="settings" style="padding-top:unset "
                          onclick="removeItemFromCart({{$item->id}})">
                                             <img src="/assets/svgs/Icon material-delete.svg">

                        <span class="remove-address" id="remove-address">REMOVE</span>
                           </span>
                    {{--                                    <span class="remove-card" onclick="removeItemFromCart({{$item->id}})"--}}
                    {{--                                          id="remove-address">    <img--}}
                    {{--                                                style="    margin-right: 5px;"--}}
                    {{--                                                src="/assets/svgs/Icon material-delete.svg">REMOVE</span>--}}
                </div>
            @else
                <div class="loop-cart children">


                    <div class="main-1">


                        <div class="row related">
                            <picture><img id="expandedImg"
                                          src="{{ env('DATA_URL') }}/products/{{ $item->details->id}}.{{  $item->details->extension_image}}?v={{  $item->details->version }}">
                            </picture>
                            <div class="row-info">

                                <span class="row-info-title">{{$item->details->label}}</span>

                                {{--                                                <input type="hidden" data-quantity="{{$item->details->stock_quantity}}">--}}
                                <input type="hidden" data-sku="{{$item->details->sku}}">
                                <span class="price">{{$item->details->unit_price_formatted}}</span>
                                <sep></sep>


                            </div>
                        </div>

                    </div>
                    <div class="main-2">
                        @if($item->cms_attributes !== null)
                        @endif
                        @if($item->modifier_details)

                            @foreach($item->modifier_details as $modifier)
                                <div class="customize-items-cart">
                                    <span>{{$modifier->label}}</span>
                                    <span>{{$modifier->message}}</span>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="main-3">


                        <span class="price">{{$item->final_price_formatted}}</span>

                    </div>

                    <span class="settings" style="padding-top:unset "
                          onclick="removeItemFromCart({{$item->id}})">
                                             <img src="/assets/svgs/Icon material-delete.svg">

                        <span class="remove-address" id="remove-address">REMOVE</span>
                           </span>
                </div>

            @endif
        @endforeach
    @else
        <div class="empty">
            <div>

                <h3>
                    Your Cart is Empty</h3></div>
        </div>
    @endif
</div>