<?php $purchases = ecom('purchases')->list(); ?>
@isset($purchases[0])

    <main id="orders-details">
        <h3>Orders Details</h3>
        <table style="width:100%">
            <tr>
                <th>Product</th>
                <th>Price</th>

            </tr>
            <tr>


                @php

                    $items = json_decode(json_encode($purchases[0]['items']))
                @endphp
                @foreach($items->items->items as $item)

                    <td>{{$item->details->label}}</td>
                    <td>{{$item->final_price_formatted}}</td>
                @endforeach
            </tr>
        </table>
        <table style="width:100%">
            <tr>
                <th>Subtotal</th>
                <td>{{$items->items->subtotal_formatted}}</td>
            </tr>
            <tr>
                <th>Shipping</th>
                <td>{{$items->items->shipping_formatted}}</td>
            </tr>
            <tr>
                <th>Payment Method</th>
                <td>@if($purchases[0]->payment_type == 'cod') Cash On Delivery @else Pay with Credit Card @endif</td>
            </tr>
            <tr>

                <th>Total</th>
                <td>{{$items->items->total_formatted}}</td>
            </tr>
        </table>
    </main>
@endisset
{{--<td>{{$purchase->id}}</td>--}}
{{--<td>{{$purchase->created_at}}</td>--}}
{{--<td>{{$purchase->status}}</td>--}}
{{--<td>{{$items->subtotal}}for {{$items->count}} </td>--}}