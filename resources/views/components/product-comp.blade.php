
<div class="best-sellers-container-carousel owl-carousel carousel single-carousel " data-carousel-nav="false" data-carousel-autoplay="true" data-carousel-items="6" data-carousel-autowidth="true" data-carousel-loop="true">


        @php $product = json_decode(json_encode($product)); @endphp
        @php

            if(ecom('users')->loginCheck()){

                     $favorite_active = ecom('favorites')->check($product->id);

                 }else{
                     $favorite_active = false;
                 }

        @endphp


        <div class="single-product">

            <picture>
                <img src="{{ env('DATA_URL') }}{{ $product->image}}">
            </picture>
            <div>
                <h2>
                    {{$product->label}}
                </h2>
                <h3>

                    {{$product->small_description}}
                </h3>
                <span>
                               {{$product-> unit_price_formatted}}


                            </span>
            </div>


            <div data-pid="{{$product->id}}"
                 class="favorite-toggle @if($favorite_active) favorite-toggle-active @endif">
                    <span class="hart favorite-toggle" onclick="ToggleFavoritesInit()">
{{--                        <i class="far fa-heart fav"></i>--}}
                        <i onclick="wishList(this)" class="fa-heart fa-regular @if($favorite_active) fa-solid @endif"></i>

{{--                        <i class="fas fa-heart" id="myImg"></i>--}}
                  </span>
            </div>


            {{--                                    <div style="    display: flex;justify-content: flex-end;" data-pid="{{$product->id}}"--}}
            {{--                                         class="favorite-toggle @if($favorite_active) favorite-toggle-active @endif"--}}
            {{--                                         href="javascript:;">--}}
            {{--                                            <span onclick="ToggleFavoritesInit()">Add to wishlist<i class="far fa-heart"></i> <i--}}
            {{--                                                        class="fas fa-heart" id="myImg" style="color: #998484"></i></span>--}}
            {{--                                    </div>--}}


        </div>




</div>
