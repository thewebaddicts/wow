<div>

    <a id="expandedImgBackground" class="asp asp-1-1 contain" data-fancybox="gallery" class="" style="display: block; text-decoration: none ; color : black ; border:1px solid #eee" href="{{$product['thumb']}}" >
        <img id="expandedImg"  src="{{$product['thumb']}}" width="100%" height="100%" >
    </a>

    <div style="display: flex; gap:15px;flex-wrap: wrap; margin-top:15px">
        @php
            $gallery = json_decode(json_encode($product['gallery']));
        @endphp

{{--        <div style="width: 80px ; height: 80px; border:1px solid #eee">--}}
{{--            <a href="{{$product['image']}}" data-fancybox="gallery" class="asp asp-1-1 contain">--}}
{{--                <img src="{{$product['thumb']}}" alt="" width="100%" height="100%" >--}}
{{--            </a>--}}
{{--        </div>--}}


        @foreach ($gallery as $galleries)
            <div class="gallery-product" >
            <a href="{{$galleries->image}}" data-fancybox="gallery" class="asp asp-1-1 contain">
                <img src="{{ $galleries->thumb}}" alt="" width="100%" height="100%" style=" border:1px solid #eee">
            </a>
            </div>
        @endforeach
    </div>


</div>
