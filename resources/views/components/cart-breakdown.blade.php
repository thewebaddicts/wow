<?php     $cart = ecom('cart')->getAsObject();?>

@php


    $code = isset($cart->voucher_code) ? $cart->voucher_code : "";

$code_value = "";
$valid = false;

if(isset($code) && !empty($code)){
$code_value = $code;
$valid = true;
}

@endphp
<form method="post" name="voucherForm" id="voucherForm" action="{{route('apply-voucher')}}" class="coupon-form"
      onsubmit="return false">
    @csrf
    <div class="coupon-code">

        <span>APPLY COUPON</span>

        <div>
            <input style="opacity: 1;position: relative" type="text" placeholder="Coupon Code"
                   onkeyup="if (event.key === 'Enter' || event.keyCode === 13) { applyVoucher($(this).next('input'));}"
                   id="coupon-control" name="promo_code">
            <input style="opacity: 1;" class="checkout-btn" type="submit" @if($valid) value="Remove"
                   @else  value="Apply" @endif
                   data-apply-text="Apply"
                   data-remove-text="Remove"

                   data-type="@if($valid){{'remove'}}@endif"
                   onclick="applyVoucher($(this))">

        </div>
    </div>


</form>

<div class="cart-sub">
    <div class="small-title" id="order-summary">Order Summary</div>
    <sep></sep>
    @foreach($cart->items->items as $carts)
        @if($carts->quantity>0)

            <div class="cart-row">
                <picture><img
                            src="{{ env('DATA_URL') }}/products/{{ $carts->details->id }}.{{ $carts->details->extension_image }}?v={{ $carts->details->version }}">
                </picture>

                <div class="info">
                    <span class="info-title">{{$carts->details->label}}</span>
                    <span class="info-desc">{{$carts->details->small_description}}</span>
                    <span class="info-price">{{$carts->details->unit_price_formatted}}</span>
                </div>
                <div class="quantity">

                    {{$carts->quantity}}
                </div>
                <div class="price">{{$carts->details->unit_price_formatted}}</div>
            </div>
        @endif
    @endforeach
    <sep></sep>
    <sep></sep>

    <div class="subtotal"><span>SUBTOTAL</span><span>{{$cart->breakdown->Subtotal}}</span></div>
    <sep></sep>
    <sep></sep>

    <div class="subtotal"><span>SHIPPING FEES</span><span>{{$cart->breakdown->Shipping}}</span></div>
    @isset($cart->breakdown->discount)
        <sep></sep>
        <sep></sep>

        <div class="subtotal"><span>COUPON DISCOUNT</span><span>{{$cart->breakdown->discount}}</span></div>
    @endisset
    <sep></sep>

    <sep></sep>
    <div class="total"><span>TOTAL</span><span>{{$cart->grandTotal_formated}}</span></div>
</div>
