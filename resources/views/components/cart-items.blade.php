<?php $cartList= ecom('cart')->getAsObject(); ?>

@if(isset($cartList->items->items))
    @foreach($cartList->items->items as $item)
        <div class="item-added">
            <span>Item successfully added to bag</span>
            <div class="item-flex">


                <picture><img id="expandedImg" src="{{ env('DATA_URL') }}/{{$item->details->image }}">

                </picture>
                <div><span>{{$item->details->label}}</span><span>{{$item->details->unit_price_formatted}}</span></div>

                <span>{{$cartList->count}}</span>
            </div>
            <div class="item-btn">
                <a href="/">CONTINUE SHOPPING</a>
                <a href="{{ecom('url')->prefix()}}/cart">VIEW BAG</a>
            </div>
        </div>
    @endforeach
@endif


