@php
    $footer_carousel = getter('footer_carousel')->get();
@endphp
<section class="footer">
    <div class="content-second">
        <a class="effect-hover"  href="https://www.instagram.com/wowsurprises.uae/">
        <h2><span>Follow us on </span>@wowsurprises.uae</h2>
        </a>
        {{--        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</p>--}}
        <div class="carousel owl-carousel  footer-carousel" data-carousel-dots="true" data-carousel-items="8"
             data-carousel-loop="true" data-carousel-nav="false" data-carousel-autoplay="true"
             data-carousel-autowidth="true">
            @isset( $footer_carousel )
                @foreach( $footer_carousel as $footer_image)

                    <div class="slide link">
                        <a class="social-media-links" href="{{$footer_image->link}}"></a>
                        <img src="{{env('DATA_URL')}}/footer_carousel/{{$footer_image->id}}.{{$footer_image->extension_image}}">
                    </div>

                @endforeach
            @endisset
        </div>

    </div>
</section>