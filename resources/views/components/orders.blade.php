<?php $purchases = ecom('purchases')->list();?>

<main id="orders">

    <h3>Orders</h3>
    @foreach($purchases as $purchase)

        <table style="width:100%">
            <tr>
                <th>Orders</th>
                <th>Date</th>
                <th>Status</th>
                <th>Total</th>
            </tr>
            <tr>
                @php

                    $items = json_decode(json_encode($purchase['items']))
                @endphp

                <td>{{$purchase->id}}</td>
                <td>{{$purchase->created_at}}</td>
                <td>{{$purchase->status}}</td>
                <td>{{$items->subtotal}}for {{$items->count}} </td>
                <td>
                    <a  href="{{route('account-order-details')}}" data-target="orders-details">View</a>
                </td>
            </tr>

        </table>
    @endforeach

</main>