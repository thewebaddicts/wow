@php
    $profile= ecom('profile')->get();
@endphp

<main id="edit-address-first">

    <h3>Addresses</h3>
    <div>


        <form action="account-add-address/add" class="address-form" method="post">
            @csrf
            <div class="form-container">
                <div class="form-title">
                    <h4>Address 1</h4>
                </div>
                <div class="form-content">

                    <input type="text" name="first_name" value="{{$profile->first_name}}" required>
                    <input type="text" name="last_name" value="{{$profile->last_name}}" required>
                    <input type="text" name="email" value="{{$profile->email}}" required style="width: 100%;">
                    {{--                    <input type="text" name="phone" value="{{$profile->phone}}" required>--}}
                    @if(isset($CountryCodes) && sizeof($CountryCodes))
                        <div class="phone-container">
                            <select name="phone_country_code" class="browser-default subject-field form-select control-25" id="phone_country_code" required>
                                @foreach($CountryCodes AS $CountryCode)
                                    <option value="{{ $CountryCode->code }}">+{{ $CountryCode->phone_code }}
                                    </option>
                                @endforeach
                            </select>

                            <input id="phone" type="text" placeholder="Phone Number" class="form-control control-75 " name="phone" value="{{old('phone')}}" required autocomplete="phone" >
                        </div>
                    @endif

                    <input class="text-input" type="text" name="label" id="" placeholder="Address Title" required>



                    @if(isset($Countries) && count($Countries)>0)
                        <select class="text-input" name="country" id="country" required>
                            @foreach($Countries as $country)
                                <option value="{{$country->code}}">{{$country->name_en}}</option>

                            @endforeach
                        </select>
                    @endif

                    <div class="address-country-section" style="display: contents">
                        <input class="text-input half" type="text" name="city" id="" placeholder="City / State" required>
                        <input class="text-input half" type="text" name="street" id="" placeholder="street" required></div>
                    <input class="text-input" type="text" name="building" id="" placeholder="Building" required>
                    <input class="text-input" type="text" name="floor" id="" placeholder="Floor" required>

                    <input class="text-input half" type="text" name="zip_code" id="" placeholder="Zip Code" required>
                    <div>     <button class="address-btn" type="submit">Save</button></div>

                </div>
            </div>
        </form>

    </div>


</main>