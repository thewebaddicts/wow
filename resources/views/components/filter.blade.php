<?php
if(!isset($secondFormSync)){ $secondFormSync = false; }

?>
<section class="filter-section">
<button id="filter-button" class="button">Filter</button>

<form id="filter-button" class="filterForm" style="display: contents" data-action="{{ explode('?',$_SERVER["REQUEST_URI"])[0] }}" data-form="{{$_SERVER["REQUEST_URI"]}}">

    <div id="filter-container" class="filters">
        <ul class="filters__list">
            @if($cust['filters'])

                @foreach($cust['filters']['data'] as $key => $row)
                    <span style="text-transform: uppercase">{{$key}}</span>
                <sep></sep>
                <sep></sep>
                    @foreach($row as $r)

                        <div class="sublist">

                                        <span class="checkbox-cust">
                                        <i class="fa-solid fa-check @if($r['selected'] == true)active @endif"></i>


                                        </span>
                            &nbsp;
                            <input @if($r['selected'] == true) checked @endif
                            type="checkbox" value="{{ $r['id']}}" id="{{ $r['id']}}"
                                   name="filter[]">
                            <label for="1">{{$r['label']}}</label>

                        </div>

                    @endforeach

                @endforeach
            @endif

        </ul>

        <button  class=filter-button id="filter_button">Submit</button>
    </div>
</form>
</section>
