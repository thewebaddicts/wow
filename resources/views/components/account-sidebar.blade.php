<ul class="sidebar">
    <li>  @if(Request::url() === route('account-profile'))
            <picture><img src="/assets/svgs/Ellipse 31.svg"></picture>@endif
        <a href="{{route('account-profile')}}" style="display: contents"> Personal information</a>
    <li>  @if(Request::url() === route('addresses') || Request::url() === route('addresses-create'))
            <picture><img src="/assets/svgs/Ellipse 31.svg"></picture>@endif
        <a href="{{route('addresses')}}" style="display: contents"> addresses</a>
    <li>  @if(Request::url() === route('saved_products'))
            <picture><img src="/assets/svgs/Ellipse 31.svg"></picture>@endif
        <a href="{{route('saved_products')}}" style="display: contents"> Saved products</a>
    <li>  @if(Request::url() === route('orders'))
            <picture><img src="/assets/svgs/Ellipse 31.svg"></picture>@endif
        <a href="{{route('orders')}}" style="display: contents"> orders</a>
    <li>  @if(Request::url() === route('logout'))
            <picture><img src="/assets/svgs/Ellipse 31.svg"></picture>@endif
        <a href="{{route('logout')}}" style="display: contents"> logout</a>


</ul>

<select name="forma" onchange="location = this.value;" class="sidebar-mobile">

    <option value="{{route('account-profile')}}" @if(Request::url() === route('account-profile')) selected @endif>
        Personal information
    </option>


    <option value="{{route('addresses')}}"
            @if(Request::url() === route('addresses') || Request::url() === route('addresses-create')) selected @endif>
        Addresses
    </option>


    <option value="{{route('saved_products')}}" @if(Request::url() === route('saved_products')) selected @endif>Saved
        products
    </option>


    <option value="{{route('orders')}}" @if(Request::url() === route('orders')) selected @endif>Orders</option>

    <option value="{{route('logout')}}">Logout</option>
</select>