<div class="checkout">
    <picture>
        <div>
            <div class="parent">
                <div style="visibility: hidden" class="prog-bar"></div>
                <div style="background: #998484;color: white" class="square">1</div>
                <div style="background: #998484;" class="prog-bar"></div>
            </div>

            <span>Shipping Address</span>

        </div>

        <div>
            <div class="parent">
                <div class="prog-bar"></div>
                <div style="" class="square">2
                </div>
                <div class="prog-bar"></div>
            </div>

            <span>Payment Method</span>
            <div class="progress-bar"></div>
        </div>

        <div>
            <div class="parent">
                <div class="prog-bar"></div>
                <div  style="" class="square">3</div>
                <div style="visibility: hidden" class="prog-bar"></div>
            </div>

            <span>Order Summary</span>
            <div class="progress-bar"></div>
        </div>
    </picture>
    <form style="display: contents;" method="get" action={{route('checkout-payment')}}>
        <main>


            @foreach($Countries as $Country)


                <div id="remove-address-second" class="sub-box">
                    <header>
                        <span>

                            <label class="container">
                <input type="radio"  name="address" value="{{$Country->id}}">
                <span class="checkmark"></span>
            </label>


                            <div></div></span>

                        <div>
{{--                            <span class="edit-feature" data-target="edit-address-second">--}}
{{--                                <img--}}
{{--                                        src="/assets/svgs/Icon feather-edit.svg" alt=""></span>--}}


                            <span class="trash" id="remove-address"><img
                                        src="/assets/svgs/Icon feather-trash-2.svg" alt=""></span></div>
                    </header>

                    <?php $loop->index;?>

                    <p>Address {{$loop->index+1}}</p>
                    <p>{{$Country->country->name}} | {{$Country->city}} | {{$Country->street}} | {{$Country->building}}
                        Building | {{$Country->floor}} Floor</p>
                </div>


            @endforeach

        </main>
        <div class="btns">
            <a style="visibility: hidden">Previous</a>
            <a href="http://toneit.test/lb/en/account-add-address">Add Address</a>

            <input id="submit" type="submit" value="Next">
        </div>
    </form>

</div>
