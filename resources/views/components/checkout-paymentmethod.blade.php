<div class="checkout">


    <?php $PaymentMethods = ecom('stores')->getPayments(); ?>
    <?php $ShippingMethods = ecom('stores')->getShipping(); ?>
    <picture>
        <div>
            <div class="parent">
                <div style="visibility: hidden" class="prog-bar"></div>
                <div style="background: #998484;color: white" class="square">1</div>
                <div style="background: #998484;" class="prog-bar"></div>
            </div>

            <span>Shipping Address</span>

        </div>

        <div>
            <div class="parent">
                <div style="background: #998484;" class="prog-bar"></div>
                <div style="background: #998484;color: white" class="square">2
                </div>
                <div style="background: #998484;" class="prog-bar"></div>
            </div>

            <span>Payment Method</span>
            <div class="progress-bar"></div>
        </div>

        <div>
            <div class="parent">
                <div class="prog-bar"></div>
                <div style="" class="square">3</div>
                <div style="visibility: hidden" class="prog-bar"></div>
            </div>

            <span>Order Summary</span>
            <div class="progress-bar"></div>
        </div>
    </picture>

    <form style="width:100%;" method="get" action={{route('checkout-order')}} >
        <input type="hidden" name="address" value="{{request()->address}}">

        <div class="paymentmethod">
            @foreach($PaymentMethods as $pay)



                    <label class="container">
                    <input type="radio"  name="payment_method" value="{{$pay->id}}">
                    <span class="checkmark"></span>
                </label>



                <div>{{$pay->label}}</div>
            @endforeach



        </div>

        <h3>Billing Address</h3>
        <main>
            <?php if (isset($_GET['address'])) {
                $address = $_GET['address'];
            }
            ?>



    @foreach($Countries as $Country)
                <div id="remove-address-second" class="sub-box">
                    <header>
                        <span>

                            <label class="container">
                <input type="radio" checked="checked" name="billing" value="{{$Country->id}}">
                <span class="checkmark"></span>
            </label>


                            <div></div></span>

                        <div><span class="edit-feature" data-target="edit-address-second"><img
                                        src="/assets/svgs/Icon feather-edit.svg" alt=""></span><span
                                    class="trash" data-target="remove-address-second"><img
                                        src="/assets/svgs/Icon feather-trash-2.svg" alt=""></span></div>
                    </header>

                    <?php $loop->index;?>

                    <p>Address {{$loop->index+1}}</p>
                    <p>{{$Country->country->name}} | {{$Country->city}} | {{$Country->street}} | {{$Country->building}}
                        Building | {{$Country->floor}} Floor</p>
                </div>
            @endforeach
        </main>

        <div class="btns">
            <a href={{route('checkout')}}>Previous</a>
            <a href="http://toneit.test/lb/en/account-add-address">Add Address</a>

            <input id="submit" type="submit" value="Next">
        </div>
    </form>

