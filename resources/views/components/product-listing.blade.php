
<div class="product-container">

    @foreach ($products['data'] as $product)

        <div>
            @component('pages.products.card',[ "product" => $product ]) @endcomponent
        </div>
    @endforeach


@component('components.pagination' , ['products'=>$products ]) @endcomponent

</div>