
<main id="second">

    <header>
        <h3>Addresses</h3>
        <button><a href={{route('account-add-address')}}>Add Adresses</a></button>
    </header>

    @if(isset($addresses) && sizeof($addresses)>0)
        @foreach($addresses as $address)
            <form style="" method="get"
                  action="{{route('delete-address',['id'=>$address->id])}}" id="icon-form">
                <div class="box">


                    <div id="remove-address-{{$loop->index}}" class="sub-box">
                        <header><span>Address 1</span>
                            <div><span class="edit-feature" data-target="edit-address-first">
{{--                                    <img--}}
{{--                                            src="/assets/svgs/Icon feather-edit.svg" alt="">--}}

                                </span>





                                <span class="trash" id="remove-address"><img
                                            src="/assets/svgs/Icon feather-trash-2.svg" alt=""></span>




                            </div>
                        </header>
                        <p>{{$address->city}} |{{$address->street}} | Building : {{$address->building}} | Floor
                            : {{$address->floor}}
                        </p>
                    </div>
                    @endforeach


                </div>
                <input type="hidden"  value="Save">
            </form>
            @else
                <div>No Addresses Found</div>
            @endif

</main>