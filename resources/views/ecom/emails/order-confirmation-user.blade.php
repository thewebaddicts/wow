<?php

    $order = App\Models\Purchase::find($order_id);
    $user = App\Models\User::where('id',$order->users_id)->first();
    $template = App\Models\EmailTemplate::where('location','Client Order')->orWhere('location','client order')->where('cancelled',0)->first();

    $items = json_decode($order->items);
    $shipping_address = json_decode($order->shipping_address_details);
    $billing_address = json_decode($order->billing_address_details);

    $payment_type = $order->payment_type;

    $checkout_details = $order->checkout_details ? json_decode($order->checkout_details) : NULL;

    $payment_data ="";

if($checkout_details){
 $voucher_discount = $checkout_details->voucher_discount ? $checkout_details->voucher_discount : '-';


$payment_data =
"<table style='width:100%; border: 1px solid #E5E5E5 ; border-collapse: collapse;   margin-bottom:50px'>
   <tr>
        <td   style='width: 100px ; padding:20px 30px 5px 30px; font-size:12px ; font-weight:bolder '>  Subtotal</td>
        <td   style='padding:20px 30px 5px 30px; font-size:12px ;  '>  ".$checkout_details->subtotal."</td>
    </tr>
    <tr>
        <td   style='width: 100px ;padding:5px 30px 5px 30px; font-size:12px ; font-weight:bolder '> VAT </td>
        <td   style='padding:5px 30px 5px 30px; font-size:12px ; '>  ".$checkout_details->vat."</td>
    </tr>
    <tr>
        <td   style='width: 100px ;padding:5px 30px 5px 30px; font-size:12px ; font-weight:bolder '> Shipping </td>
        <td   style='padding:5px 30px 5px 30px; font-size:12px ; '>  ".$checkout_details->shipping."</td>
    </tr>

    <tr>
        <td   style='width: 100px ;padding:5px 30px 5px 30px; font-size:12px ; font-weight:bolder '> Voucher Discount </td>
        <td   style='padding:5px 30px 5px 30px; font-size:12px ; '>  ".$voucher_discount."</td>
    </tr>

    <tr>
        <td   style='width: 100px ;padding:5px 30px 20px 30px; font-size:12px ; font-weight:bolder '>  Grand Total  </td>
        <td   style='padding:5px 30px 20px 30px; font-size:12px ;  '>".$checkout_details->grandTotal_formated."</td>
    </tr>
  </table>";
}

    $personal_data =
    "<table style='width:100%; border: 1px solid #E5E5E5 ; border-collapse: collapse;   margin-bottom:50px'>
       <tr>
            <td   style='width: 100px ; padding:20px 30px 5px 30px; font-size:12px ; font-weight:bolder '>  First Name</td>
            <td   style='padding:20px 30px 5px 30px; font-size:12px ;  '>  ".$user->first_name."</td>
        </tr>
        <tr>
            <td   style='width: 100px ;padding:5px 30px 5px 30px; font-size:12px ; font-weight:bolder '> Last Name </td>
            <td   style='padding:5px 30px 5px 30px; font-size:12px ; '>  ".$user->last_name."</td>
        </tr>
        <tr>
            <td   style='width: 100px ;padding:5px 30px 5px 30px; font-size:12px ; font-weight:bolder '> Email </td>
            <td   style='padding:5px 30px 5px 30px; font-size:12px ; '>  ".$user->email."</td>
        </tr>
        <tr>
            <td   style='width: 100px ;padding:5px 30px 20px 30px; font-size:12px ; font-weight:bolder '> Phone </td>
            <td   style='padding:5px 30px 20px 30px; font-size:12px ;  '>".$user->phone."</td>
        </tr>
      </table>";



    $shipping_data =
    "<table style=' width:100%; border: 1px solid #E5E5E5 ; border-collapse: collapse;   margin-bottom:50px'>

    <tr>
        <td   style='width: 100px ; padding:20px 30px 5px 30px; font-size:12px ; font-weight:bolder '>  Full Name</td>
        <td   style='padding:20px 30px 5px 30px; font-size:12px ;  '>  ".$shipping_address->first_name." ". $shipping_address->last_name ."</td>
    </tr>
    <tr>
        <td   style='width: 100px ;padding:5px 30px 5px 30px; font-size:12px ; font-weight:bolder '>  City</td>
        <td   style='padding:5px 30px 5px 30px; font-size:12px ; '>  ".$shipping_address->city."</td>
    </tr>
    <tr>
        <td   style='width: 100px ;padding:5px 30px 5px 30px; font-size:12px ; font-weight:bolder '>  Street</td>
        <td   style='padding:5px 30px 5px 30px; font-size:12px ; '>  ".$shipping_address->street."</td>
    </tr>
    <tr>
        <td   style='width: 100px ;padding:5px 30px 5px 30px; font-size:12px ; font-weight:bolder '>  Building</td>
        <td   style='padding:5px 30px 5px 30px; font-size:12px ; '>  ".$shipping_address->building."</td>
    </tr>
    <tr>
        <td   style='width: 100px ;padding:5px 30px 20px 30px; font-size:12px ; font-weight:bolder '>  Tracking Number</td>
        <td   style='padding:5px 30px 20px 30px; font-size:12px ;  '>  ".$order->tracking_number."</td>
    </tr>
  </table>";


    $billing_data =
    "<table style=' width:100%; border: 1px solid #E5E5E5 ; border-collapse: collapse;   margin-bottom:50px'>
        <tr>
        <td   style='width: 100px ; padding:20px 30px 5px 30px; font-size:12px ; font-weight:bolder '>  Full Name</td>
        <td   style='padding:20px 30px 5px 30px; font-size:12px ;  '>  ".$billing_address->first_name." ". $billing_address->last_name ."</td>
    </tr>
        <tr>
        <td   style='width: 100px ;padding:5px 30px 5px 30px; font-size:12px ; font-weight:bolder '>  City</td>
        <td   style='padding:5px 30px 5px 30px; font-size:12px ; '>  ".$billing_address->city."</td>
    </tr>
        <tr>
            <td   style='width: 100px ;padding:5px 30px 5px 30px; font-size:12px ; font-weight:bolder '>  District</td>
            <td   style='padding:5px 30px 5px 30px; font-size:12px ; '>  ".$billing_address->street."</td>
        </tr>
        <tr>
            <td   style='width: 100px ;padding:5px 30px 5px 30px; font-size:12px ; font-weight:bolder '>  Building</td>
            <td   style='padding:5px 30px 5px 30px; font-size:12px ; '>  ".$billing_address->building."</td>
        </tr>
        <tr>
            <td   style='width: 100px ;padding:5px 30px 20px 30px; font-size:12px ; font-weight:bolder '>  Payment Type</td>
            <td   style='padding:5px 30px 20px 30px; font-size:12px ;  '> ".$payment_type."</td>
        </tr>
      </table>";
$i=0;
$items_data="";
      foreach($items as $item){

          $i++;
         // $marginBottom = "50px";
          $marginBottom = $i == sizeof($items) ? "50px" : "10px";
    $items_data .=
    "<table style='width:100%;border: 1px solid #E5E5E5 ; border-collapse: collapse;  margin-bottom:".$marginBottom."'>
<tr>
    <td colspan='2' style='padding:10px 60px 10px 10px ;font-weight:bolder; margin:0 '>".$item->item_details->label."</td>
    </tr>
    <tr>
        <td style='padding:2px 10px; font-size:12px ; font-weight:bold'> Code</td>
        <td style='padding:2px 10px ;font-size:12px ; '> ".$item->item_details->sku."</td>
    </tr>
    <tr>
       <td style='padding:2px 10px; font-size:12px ; font-weight:bold'>Quantity </td>
        <td style='padding:2px 10px; font-size:12px ; '> ".$item->quantity."</td>
    </tr>
    <tr>
        <td style='padding:2px 10px 10px 10px;font-size:12px ;  font-weight:bold'>Price </td>
        <td style='padding:2px 10px 10px 10px ;font-size:12px ; '> ".$item->total_formated."</td>
    </tr>
</table>";

}




$emaile=$template->email_form;
$dictionary=[
    'full_name'=> $user->first_name . ' '. $user->last_name ,
    'personal_data'=> $personal_data,
    'items_data'=> $items_data ,
    'shipping_data'=> $shipping_data ,
    'billing_data' => $billing_data,
    'payment_data' => $payment_data
];


$Search = [];
$Replace = [];
foreach ($dictionary AS $key=>$value){
    $Search[]   =   "*".$key."*";
    $Replace[]  =   $value;
}

$emaile = str_replace($Search,$Replace,$emaile);

$logo_url = asset('images/sadagroup-holding.png');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

</head>

<body style="font-family: arial,sans-serif; padding:0; margin:0; font-family:'Raleway',sans-serif">
    <div style="background-color:#f2f2f2">
        <div
            style="background-color:#fff; box-shadow:0 0 10px #EEE; width:100%; max-width:600px; padding:40px; margin:auto; overflow:hidden;">



            <table style="border-collapse: collapse; width:100%; margin-bottom:20px">
                <tr>
                    <td style="padding:10px 0px ; font-size:26px ; font-weight:bolder ; text-align:left ">
                        <img src="{{$logo_url}}" alt="LOGO" width="120" />
                    </td>
                    <td
                        style="padding:10px 0px ; font-size:20px ;font-family:'Raleway',sans-serif; font-weight:bolder ; text-align:right ">
                        ORDER #{{$order->id}}
                    </td>
                </tr>
            </table>

            {!! $emaile !!}

        </div>
    </div>
</body>

</html>

