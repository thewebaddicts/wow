<?php


$form_data = "<table>";
foreach($dictionary->form_data as $key=>$value ){
    $form_data .=  "<tr><td  style='width: 100px ; padding:5px 30px 5px 30px; font-size:12px ; font-weight:bolder ; text-transform: uppercase'>".  $key ."</td><td   style='padding:5px 30px 5px 30px; font-size:12px ; '> " . $value ."</td></tr>";
}
$form_data .= "</table>";



$email=$template->email_form;

if(isset($dictionary->title)){
    $dictionary=["title"=>$dictionary->title, "form_data"=>$form_data];
}else{
    $dictionary=["form_data"=>$form_data];
}


//$EmailText = $EmailTemplate['template_en'];
$Search = [];
$Replace = [];
foreach ($dictionary AS $key=>$value){
    $Search[]   =   "*".$key."*";
    $Replace[]  =   $value;
}

//$Search[]   =   "*data*";
//$Replace[]  =   $form_data;


$email = str_replace( $Search, $Replace,$email );


$logo_url = \App\Models\Setting::getSettingImage(29);
?>

<div style=" padding: 30px 0; font-size: 14px; border: 1px solid #eeeeee ">
    <div style="margin:auto; max-width: 500px; padding: 20px 0; overflow: hidden "><img src="{{$logo_url}}" width="120">
    </div>
    <div
        style="margin:auto; border: 1px solid #eeeeee; max-width: 500px; background-color: #FAFAFA; padding: 20px; color:#000;">
        {!! $email !!}</div>
</div>
