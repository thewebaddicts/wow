
<?php
$social_media = App\Models\SocialMediaModel::where('cancelled', 0)->get();


//$shopbycategory = App\Models\ShopbycategoryModel::where('cancelled', 0)->limit(4)->get();
?>
@if(Request::segment(3) == null)
@component('components.socialmedia')@endcomponent
@endif


<section class="footermap">
    <div class="content">
        <div class="subscribe"><span>Subscribe to our newsletter to receive our latest updates</span>
            <sep></sep>
            <sep></sep>
            <form class="newsletter-form" action="{{route('newsletter')}}" method="post">
                @csrf
                <input type="email" required name="newsletter-email" placeholder="Your email...">
                <button type="submit"><img src='/assets/svgs/Icon feather-arrow-right2.svg'></button>
            </form>
            <div>
            <ul>
                <li><a href="mailto:{{($social_media[0]->mail)}}"   class="icon-container"><picture><img src="/assets/svgs/mailbox.svg"></picture><span class="socialmedia-link">{{($social_media[0]->mail)}}</span></a></li>
                <li><a href="tel:{{($social_media[0]->mobile)}}"  class="icon-container"><picture><img src="/assets/svgs/phoneicon.svg"></picture><span class="socialmedia-link">{{($social_media[0]->mobile)}}</span></a></li>
                <li><a href="{{($social_media[0]->location)}}" target="_blank"  class="icon-container"><i class="fa-solid fa-location-pin"></i>&nbsp;&nbsp;&nbsp;<span class="socialmedia-link">DIP 1- Dubai- Zaraa Complex-<br> Warehouse N.3</span></a></li>
            </ul>
                <ul>
                    <li><a href="{{($social_media[0]->facebook_link)}}" target="_blank" class="icon-container"><picture><img src="/assets/svgs/facebook-footer.svg"></picture><span class="socialmedia-link">{{($social_media[0]->facebook_label)}}</span></a></li>
{{--                    <li><a><img src="../assets/svgs/phoneicon.svg">+961 70 538 026</a></li>--}}
                    <li><a href="{{($social_media[0]->instagram_link)}}" target="_blank" class="icon-container"><picture><img src="/assets/svgs/insta.svg"></picture><span class="socialmedia-link">{{($social_media[0]->instagram_label)}}</span></a></li>
                    <li><a href="{{($social_media[0]->twitter_link)}}" target="_blank" class="icon-container"><picture><img src="/assets/svgs/twitter.svg"></picture><span class="socialmedia-link">{{($social_media[0]->twitter_label)}}</span></a></li>

                </ul>
            </div>
        </div>


            {!!(ecom('menus')->get('footerMenu'))!!}

        </ul>
        <sep></sep>
        <div class="copyright">
        <span>
            <a href="https://thewebaddicts.com/" target="_blank">© 2022. All Rights Reserved. Copyrights WOW Surprises UAE. Designed and Developed by The Web Addicts</a>
        </span>

        </div>
    </div>

</section>


