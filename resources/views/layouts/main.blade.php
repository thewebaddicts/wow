<?php use App\Models\SlideshowModel;$cartList = ecom('cart')->getAsObject();
$slideshow = SlideshowModel::where('cancelled', 0)->get();

?>

        <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    {{ seo()->setBladeInheritedTags(View::getSection('title'))->generate() }}
{{--    {{  structuredData()->generate(request()->store, (isset($product)) ? $product : false, (isset($slug)) ? $slug : false,--}}
{{--\App\Http\Controllers\FrontEndMenuController::linkExtractor(request()->menus['footerSocialMenu'])) }}--}}
    <meta name="keywords" content="Gifts, Gift box, customized gift, birthday, event, birthday party, balloons, balloon decoration, bridal, proposals, part decor, gift shop, board games, mugs, frames, father’s day, Christmas, ramadan, mother’s day, valentine, bridesmaid gift, I do, easter, teacher’s day.">
    <meta name="description" content="A gift shop that lets you buy your own items, gadgets & home accessories. You can also customize some for loved ones, buy ready-made gift boxes or create your own box. We also cover Balloon arrangements with same day delivery all over the UAE!">
    <meta name="store-id" content="{{ request()->segment(1) }}">
    <meta name="lang" content="{{ request()->segment(2) }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <x-htmlhelpers-favicon prefix="/assets/favicon"></x-htmlhelpers-favicon>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700&display=swap"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">

    @yield('title')
    <style>
        .common-loader-initial {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            width: 100vw;
            height: 100vh;
            margin: auto;
            background-color: #fff;
            z-index: 9999;
            background-image: url("/assets/svgs/Wow-Loader.gif");
            background-position: center;
            background-repeat: no-repeat;


        }

        body,
        html {
            margin: 0;
            padding: 0;
        }

    </style>


</head>

<body>

<div class="common-loader-initial common-loader-hide"></div>

<nav class="main @if(Request::segment(3) !== null) compact @endif">

    <div class="content">

        <a href="{{ route('home') }}" class="logo"><img src="/assets/svgs/wowlogo.svg" alt="logo"></a>
        <ul class="menu @if($slideshow[0]->theme == 'BLACK')black-theme @else  white-theme @endif">

            {!!(ecom('menus')->get('mainMenu'))!!}

        </ul>

        <ul class="actions @if($slideshow[0]->theme == 'BLACK')black-theme @else  white-theme @endif">
            <li class="search"><a href="#"><img src="/assets/svgs/icon-search.svg"></a>
                <div class="togglesearch" style="">

                    <form style="width: 550px;display: inherit" id="search" method="get" action="{{route('search')}}">

                        <input id="inp" type="text" placeholder="Search" name="term">
                        <button type="submit"><img src="/assets/svgs/icon-search.svg"></button>

                    </form>

                    <i class="fa-solid fa-xmark"></i>
                </div>

            </li>

            <li><a  href="/ae/en/account/login"><img src="/assets/svgs/Icon feather-user.svg"></a></li>
            <li style="position: relative;"><a href="{{route('cart')}}">@if($cartList->count<1)<img
                            src="/assets/svgs/Icon-cart.svg">@else <img src="/assets/svgs/Icon-cart.svg"><span
                            id="cart-count-special" class="cart-cou"><span><?php try {
                                echo request()->user['counts']['cart'];
                            } catch (\Throwable $th) {
                                echo "0";
                            } ?></span></span>  @endisset</a>
            </li>


        </ul>
        @if(session('login'))
            <div class="wrapper">
                <div class="typing-demo">
                    Hello {{request()->user->user->first_name}}&nbsp;!
                </div>
            </div>
        @endif
    </div>


{{--    ////--}}
        <div class="content-responsive">
            <div class="menu-btn">
                <div class="menu-btn__burger">
                </div>
            </div>


            <div class="burger-display">
                <ul class="menu-responsive accordion accordion-slide">
                    {!!(ecom('menus')->get('mainMenu'))!!}

                </ul>
            </div>


            <a href="{{ route('home') }}" class="logo"><img src="/assets/svgs/wowlogo.svg" alt="logo"></a>
            <ul class="actions @if($slideshow[0]->theme == 'BLACK')black-theme @else  white-theme @endif">
                <li class="search"><a href="#"><img src="/assets/svgs/icon-search.svg"></a>
                    <div class="togglesearch" style="">

                        <form style="width: 550px;display: inherit" id="search" method="get" action="{{route('search')}}">

                            <input id="inp" type="text" placeholder="Search" name="term">
                            <button type="submit"><img src="/assets/svgs/icon-search.svg"></button>

                        </form>

                        <i class="fa-solid fa-xmark"></i>
                    </div>

                </li>

                <li><a  href="/ae/en/account/login"><img src="/assets/svgs/Icon feather-user.svg"></a></li>
                <li style="position: relative;"><a href="{{route('cart')}}">@if($cartList->count<1)<img
                                src="/assets/svgs/Icon-cart.svg">@else <img src="/assets/svgs/Icon-cart.svg"><span
                                id="cart-count-special" class="cart-cou"><span><?php try {
                                    echo request()->user['counts']['cart'];
                                } catch (\Throwable $th) {
                                    echo "0";
                                } ?></span></span>  @endisset</a>
                </li>


            </ul>


        </div>
    {{--    ////--}}
</nav>
<nav class="main-second @if(Request::segment(3) == null) compact @endif">

    <div class="content">

        <a href="{{ route('home') }}" class="logo"><img src="/assets/svgs/wowlogo.svg" alt="logo"></a>
        <ul class="menu">

            {!!(ecom('menus')->get('mainMenu'))!!}

        </ul>

        <ul class="actions-second">
            <li class="search"><a href="#"><img src="/assets/svgs/icon-search.svg"></a>
                <div class="togglesearch" style="">
                    <form style="width: 550px;display: inherit" method="get" id="search" action="{{route('search')}}">

                        <input id="inp" type="text" placeholder="Search" name="term">
                        <button type="submit"><img src="/assets/svgs/icon-search.svg"></button>
                    </form>
                    <i class="fa-solid fa-xmark"></i>
                </div>
            </li>

            <li><a href="/ae/en/account/login"><img src="/assets/svgs/Icon feather-user.svg"></a></li>
            <li style="position: relative"><a href="{{route('cart')}}">@if($cartList->count<1)<img
                            src="/assets/svgs/Icon-cart.svg">@else <img src="/assets/svgs/Icon-cart.svg"><span
                            id="cart-count-special" class="cart-cou"><span><?php try {
                                echo request()->user['counts']['cart'];
                            } catch (\Throwable $th) {
                                echo "0";
                            } ?></span></span> @endisset</a></li>


        </ul>

    </div>

    {{--    <li class="searchbar">--}}
    {{--        <i class="fa fa-search" aria-hidden="true"></i>--}}
    {{--        <div class="togglesearch">--}}
    {{--            <input type="text" placeholder=""/>--}}
    {{--            <input type="button" value="Search"/>--}}
    {{--        </div>--}}
    {{--    </li>--}}
    <div class="content-responsive">
        <div class="menu-btn">
            <div class="menu-btn__burger">
            </div>
        </div>


        <div class="burger-display">
            <ul class="menu-responsive accordion accordion-slide">
                {!!(ecom('menus')->get('mainMenu'))!!}

            </ul>
        </div>


        <a style="width: 90px" href="{{ route('home') }}" class="logo"><img src="/assets/svgs/wowlogo.svg"
                                                                            alt="logo"></a>
        <ul class="actions @if($slideshow[0]->theme == 'BLACK')black-theme @else  white-theme @endif">
            <li class="search"><a href="#"><img src="/assets/svgs/icon-search.svg"></a>
                <div class="togglesearch" style="">

                    <form style="width: 550px;display: inherit" id="search" method="get" action="{{route('search')}}">

                        <input id="inp" type="text" placeholder="Search" name="term">
                        <button type="submit"><img src="/assets/svgs/icon-search.svg"></button>

                    </form>

                    <i class="fa-solid fa-xmark"></i>
                </div>

            </li>

            <li><a  href="/ae/en/account/login"><img src="/assets/svgs/Icon feather-user.svg"></a></li>
            <li style="position: relative;"><a href="{{route('cart')}}">@if($cartList->count<1)<img
                            src="/assets/svgs/Icon-cart.svg">@else <img src="/assets/svgs/Icon-cart.svg"><span
                            id="cart-count-special" class="cart-cou"><span><?php try {
                                echo request()->user['counts']['cart'];
                            } catch (\Throwable $th) {
                                echo "0";
                            } ?></span></span>  @endisset</a>
            </li>


        </ul>


    </div>
</nav>

<div id="toast"></div>
<div id="toast2"></div>


@yield('content')





@include('layouts.footer')

@yield('script')


<script type="text/javascript" src="/js/require.js"></script>
<script type="text/javascript" async src="/js/init.js" attr-cache-version="{{ env('CACHE_VERSION') }}"></script>
<script language="javascript">


    function NotificationFunction() {

        @if (request()->input('notification'))

        ShowMessage("", "{{ request()->input('notification') }}");
        removeQueryNotificationString();
        @endif
        @if (request()->input('notification_title') && request()->input('notification_message'))

        ShowMessage("{{ request()->input('notification_title') }}", "{{ request()->input('notification_message') }}");
        removeQueryNotificationString();
        @endif
        @if (request()->input('notification_id'))

        <?php $notification = \App\Models\Notifications::getNotification(request()->input('notification_id'));
        ?>
        ShowMessage("{{ $notification->label }}", "{{ $notification->text }}");
        removeQueryNotificationString();

        @endif


    }

    function getVals() {
        // Get slider values
        var parent = this.parentNode;
        var slides = parent.getElementsByTagName("input");

        var slide1 = parseFloat(slides[0].value);

        var slide2 = parseFloat(slides[1].value);

        if (slide1 > slide2) {
            var tmp = slide2;
            slide2 = slide1;
            slide1 = tmp;
        }

        var displayElement1 = parent.getElementsByClassName("rangeValues1")[0];
        var displayElement2 = parent.getElementsByClassName("rangeValues2")[0];
        displayElement1.value = slide1;
        displayElement2.value = slide2;
        displayElement2.value = slide2;
    }

    window.onload = function () {
        // Initialize Sliders
        var sliderSections = document.getElementsByClassName("range-slider");
        for (var x = 0; x < sliderSections.length; x++) {
            var sliders = sliderSections[x].getElementsByTagName("input");
            for (var y = 0; y < sliders.length; y++) {
                if (sliders[y].type === "range") {
                    sliders[y].oninput = getVals;
                    // Manually trigger event first time to display values
                    sliders[y].oninput();
                }
            }
        }
    }


</script>
{{ gtag()->track() }}
</body>

</html>