
@extends('layouts.main')
@section('content')
    @php  $addresses = ecom('addresses')->list();

    $profile = ecom('profile')->get();

     @endphp
    <section class="addresses">
        <div class="content">
            <div class="path">
                <span>Home</span><img src="/assets/svgs/arrow-right.svg"><span>Account</span>
            </div>


            <h1 class="path-title">
                ACCOUNT
            </h1>


            <main>
                @component('components.account-sidebar')@endcomponent
                <div class="addresses-information">
                    <span class="small-title">Add Addresses</span>
                    <a href="{{route('addresses-create')}}" style="display:contents" title="click here to add address">
                        <img src="/assets/svgs/add-address.svg"></a>
                    <sep></sep>
                    <sep></sep>
                    <sep></sep>

                    <div class="container">

                        @if(isset($addresses) && sizeof($addresses)>0)
                            @foreach($addresses as $address)

                                <form style="" method="get" action="{{route('delete-address',['id'=>$address->id])}}" id="icon-form">
                                    <div>
                                        <span>{{$address->label}}</span>
                                        <sep></sep>

                                        <span>{{$profile->first_name}}&nbsp;&nbsp;{{$profile->last_name}}</span>
                                        <span>{{$address->city}}, {{$address->street}}, Building : {{$address->building}}, Floor
                            : {{$address->floor}}</span>
                                        <span class="settings">
                                             <img src="/assets/svgs/Icon material-delete.svg">

                        <span class="remove-address" id="remove-address">REMOVE</span>
                           </span>
                                    </div>
                                </form>
                            @endforeach
                        @endif
                    </div>

                </div>
            </main>


        </div>
    </section>
@endsection