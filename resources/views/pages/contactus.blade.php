@extends('layouts.main')

@section('content')
    <div class="topbanner">
        <figure>
            <img src="/assets/images/Mask Group 30.png" alt="">
        </figure>
        <figcaption>
            <span>Home / Contact Us</span>
            <h1>Contact Us</h1>
            <h2> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</h2>

        </figcaption>
    </div>

    <section class="contactus">

        <div class="content">
            <header>
                <h2>Contact Form</h2>
            </header>
            <form method="post" class="contact-form" action="{{route('contact-us')}}">
                @csrf
                <main>
                    <?php $profile = ecom('profile')->get();?>
                    <?php  $PhoneCodes = ecom('countries')->getPhoneCodes();
                    ?>

                    <div>
                        <label for="">First Name</label>
                        <input class="text-input half" type="text" name="first_name" id="" placeholder="First Name">
                    </div>

                    <div>
                        <label for="">Last Name</label>
                        <input class="text-input half" type="text" name="last_name" id="" placeholder="Last Name">
                    </div>
                    <div>
                        <label for="">Email</label>
                        <input class="text-input half" type="email" name="email" id="" placeholder="Email Address">
                    </div>
                    <div>
                        <label for="">Phone Number</label>
                        <div style="display: flex;flex-direction:row;width:100%;"><select name="phone_country_code" id="">


                                @foreach($PhoneCodes AS $code)

                                    <option value="{{ $code['code'] }}"
                                            @if( $code['phone_code']=='961') selected @endif>{{ $code['phone_code'] }}</option>

                                @endforeach
                            </select>
                            <input type="tel" placeholder="Your Phone Number" name="phone">


                        </div>
                    </div>

                        <div style="width: 100%;height: 200px">
                            <label for="">Subject</label>
                            <input style="height: 100%;" class="text-input half" type="text" name="subject" id="" placeholder="Subject">
                        </div>
                    <input type="submit" value="Send">
                </main>

            </form>
        </div>


    </section>
@endsection
