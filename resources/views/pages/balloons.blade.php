@extends('layouts.main')
{{--@section('title')<title>{{$balloons['menu']->label}} | {{ __('texts.wow') }}</title>@endsection--}}
@section('content')


    @php
        $balloons_adds_on = getter('adds_on')->condition('location','balloons')->get()->pluck('ecom_keywords_id')->first();
        $wrapping_adds_on = getter('adds_on')->condition('location','wrapping papers')->get()->pluck('ecom_keywords_id')->first();

            if($balloons['filters']>0){

                $price_min = $balloons['filters']['price']['min'];
                $price_max = $balloons['filters']['price']['max'];
                }
    @endphp

    <section class="balloons-products-listing-page" id="product-listing-container">
        <div class="content">
            @component('partials.page-path')@endcomponent
            <div class="balloons-filter-container">
                <div class="filter-container-flat-responsive">
                    <div class="filter-responsive">
                        <picture class="block"><img src="/assets/svgs/filter-icon.svg"></picture>
                        <span>Filters</span>
                    </div>
                    {{--                    <span>Filters</span>--}}


                    <form id="sort-by-id" action="{{ecom('url')->prefix()}}/products/{{$balloons['menu']->slug}}"
                          data-action="">
                        @csrf
                        <div class="select-product-listing">
                            <select id="sort-by-name" onchange="sortByChange($(this))" name="sortBy">

                                <option>Sort By: Featured</option>
                                <option @if(request()->sortBy === 'alphaA') selected @endif value="alphaA">
                                    A to Z
                                </option>
                                <option @if(request()->sortBy === 'alphaD') selected @endif value="alphaD">
                                    Z to A
                                </option>
                                <option @if(request()->sortBy === 'priceA') selected @endif value="priceA">
                                    Price Low to High
                                </option>
                                <option @if(request()->sortBy === 'priceD') selected @endif value="priceD">
                                    Price High to Low
                                </option>
                            </select>
                        </div>
                    </form>
                    <form id="filter-price-form" action="{{ecom('url')->prefix()}}/products/{{$balloons['menu']->slug}}"
                          data-action="/ae/en/boxes/your-box">
                        @if($balloons['filters']>0)

                            @foreach($balloons['filters']['data'] as $key => $row)
                                <span style="text-transform: uppercase">{{$key}}</span>
                                @foreach($row as $r)

                                    <div class="sublist">

                                        <span class="checkbox-cust">
                                        <i class="fa-solid fa-check @if($r['selected'] == true)active @endif"></i>


                                        </span>
                                        &nbsp;
                                        <input @if($r['selected'] == true) checked @endif
                                        type="checkbox" value="{{ $r['id']}}" id="{{ $r['id']}}"
                                               name="filter[]">
                                        <label for="1">{{$r['label']}}</label>

                                    </div>

                                @endforeach
                                <sep></sep>
                                <sep></sep>
                            @endforeach
                        @endif

                        {{--                    </form>--}}

                        <sep></sep>
                        <sep></sep>
                        <sep></sep>

                        @if($balloons['filters']>0)

                            <span id="valBox">PRICE RANGE</span>
                            <div class="range-slider">


                                <input type="range" min="{{$price_min}}" max="{{$price_max}}" class="range"
                                       id="min"
                                       @isset($balloons['request']['filter_price_min']) value="{{$balloons['request']['filter_price_min']}}"
                                       @else value="{{$price_min}}" @endisset>
                                <input type="range" min="{{$price_min}}" max="{{$price_max}}" class="range"
                                       id="max"
                                       @isset($balloons['request']['filter_price_max']) value="{{$balloons['request']['filter_price_max']}}"
                                       @else value="{{$price_max}}" @endisset>

                                <sep></sep>
                                <sep></sep>

                                <div class="range-max-min">
                                    <span style="font-size: 9px;letter-spacing: unset">Minimum</span>
                                    <input style="font-size: 9px;width: 20px;border: unset;text-indent: unset"
                                           class="rangeValues1 range-number" name="filter_price_min">
                                    <span style="margin: unset;font-size: 9px;letter-spacing: unset">AED</span></div>
                                <div class="range-max-min">
                                    <span style="font-size: 9px;letter-spacing: unset;">Maximum</span>
                                    <input style="font-size: 9px;width: 20px;border: unset;text-indent: unset"
                                           class="rangeValues2 range-number" name="filter_price_max">
                                    <span style="margin: unset;
    font-size: 9px;letter-spacing: unset">AED</span></div>

                            </div>
                        @endif
                        <sep></sep>
                        @if($balloons['filters']>0)
                            <div class="submitorreset">
                                <button id="filter_button">Submit</button>
                            </div>
                        @endif
                    </form>


                </div>
                <div class="filter-container-flat expand">
                    <div class="filter">
                        <picture><img src="/assets/svgs/filter-icon.svg"></picture>
                        <span>Filters</span>
                    </div>
                    {{--                    <span>Filters</span>--}}


                    <form id="sort-by-id" style="display: contents"
                          action="{{ecom('url')->prefix()}}/products/{{$balloons['menu']->slug}}" data-action="">
                        @csrf
                        <div class="select-product-listing">
                            <select id="sort-by-name" onchange="sortByChange($(this))" name="sortBy">

                                <option>Sort By: Featured</option>
                                <option @if(request()->sortBy === 'alphaA') selected @endif value="alphaA">
                                    A to Z
                                </option>
                                <option @if(request()->sortBy === 'alphaD') selected @endif value="alphaD">
                                    Z to A
                                </option>
                                <option @if(request()->sortBy === 'priceA') selected @endif value="priceA">
                                    Price Low to High
                                </option>
                                <option @if(request()->sortBy === 'priceD') selected @endif value="priceD">
                                    Price High to Low
                                </option>
                            </select>
                        </div>
                    </form>
                    <form id="filter-price-form" style="display: contents"
                          action="{{ecom('url')->prefix()}}/products/{{$balloons['menu']->slug}}"
                          data-action="/ae/en/boxes/your-box">
                        @if($balloons['filters']>0)

                            @foreach($balloons['filters']['data'] as $key => $row)
                                <span style="text-transform: uppercase">{{$key}}</span>
                                @foreach($row as $r)

                                    <div class="sublist">

                                        <span class="checkbox-cust">
                                        <i class="fa-solid fa-check @if($r['selected'] == true)active @endif"></i>


                                        </span>
                                        &nbsp;
                                        <input @if($r['selected'] == true) checked @endif
                                        type="checkbox" value="{{ $r['id']}}" id="{{ $r['id']}}"
                                               name="filter[]">
                                        <label for="1">{{$r['label']}}</label>

                                    </div>

                                @endforeach
                                <sep></sep>
                                <sep></sep>
                            @endforeach
                        @endif

                        {{--                    </form>--}}

                        <sep></sep>
                        <sep></sep>
                        <sep></sep>

                        @if($balloons['filters']>0)

                            <span id="valBox">PRICE RANGE</span>
                            <div class="range-slider">


                                <input type="range" min="{{$price_min}}" max="{{$price_max}}" class="range"
                                       id="min"
                                       @isset($balloons['request']['filter_price_min']) value="{{$balloons['request']['filter_price_min']}}"
                                       @else value="{{$price_min}}" @endisset>
                                <input type="range" min="{{$price_min}}" max="{{$price_max}}" class="range"
                                       id="max"
                                       @isset($balloons['request']['filter_price_max']) value="{{$balloons['request']['filter_price_max']}}"
                                       @else value="{{$price_max}}" @endisset>
                                <sep></sep>
                                <sep></sep>


                                <span style="font-size: 9px;letter-spacing: unset">Minimum</span> <input
                                        style="font-size: 9px;width: 20px;border: unset;text-indent: unset"
                                        class="rangeValues1" name="filter_price_min"><span style="margin: unset;
    font-size: 9px;letter-spacing: unset">AED</span>
                                <span style="font-size: 9px;letter-spacing: unset;margin-left: 40px;">Maximum</span>
                                <input
                                        style="font-size: 9px;width: 20px;border: unset;text-indent: unset"
                                        class="rangeValues2" name="filter_price_max"><span style="margin: unset;
    font-size: 9px;letter-spacing: unset">AED</span>
                            </div>
                        @endif
                        <sep></sep>
                        @if($balloons['filters']>0)
                            <div class="submitorreset">
                                <button id="filter_button">Submit</button>
                            </div>
                        @endif
                    </form>


                </div>
                {{--        end expand        --}}
                <div class="main-container">

                    @if($balloons['total']>0)

                        @foreach(   $balloons['data'] as $balloon_single)
                            @php
                                $balloon_single = json_decode(json_encode($balloon_single))
                            @endphp

                            <div class="balloons-product">
                                <a href="{{ecom('url')->prefix()}}/product/{{$balloon_single->slug}}/"></a>

                                <picture><img src="{{$balloon_single->thumb}}">
                                </picture>

                                <div>
                                    @php

                                        if(ecom('users')->loginCheck()){

                                                 $favorite_active = ecom('favorites')->check($balloon_single->id);

                                             }else{
                                                 $favorite_active = false;
                                             }

                                    @endphp


                                    @php
                                        $request = (int)$balloons['request']['keywords'];

                                    @endphp

                                    {{--                                    @foreach($balloons_adds_on as $add_on)--}}

                                    @if($balloon_single->stock_quantity!==0)
                                        @if($request == $balloons_adds_on)
                                            <div class="primary-button">


                                                <a class="click-btn btn-style904" data-hover="" href="javascript:;"
                                                   onclick="addToCart('product_id={{ $balloon_single->id }}',1,event); return false;">

                                                    <span class="hover-me">Add to bag</span>
                                                </a>
                                            </div>
                                        @elseif($request == $wrapping_adds_on)

                                            <div class="primary-button">


                                                <a class="click-btn btn-style904" data-hover="" href="javascript:;"
                                                   onclick="addToCart('product_id={{ $balloon_single->id }}',2,event); return false;">

                                                    <span class="hover-me">Add to bag</span>
                                                </a>
                                            </div>

                                        @else
                                            <div class="primary-button">


                                                <a class="click-btn btn-style904" data-hover="" href="javascript:;"
                                                   onclick="addToCart('product_id={{ $balloon_single->id }}',0,event); return false;">

                                                    <span class="hover-me">Add to bag</span>
                                                </a>
                                            </div>
                                        @endif
                                    @endif
                                    {{--                                    @endforeach--}}

                                    <h2>

                                        {{$balloon_single->label}}
                                    </h2>

                                    <span class="price">

      {{$balloon_single->unit_price_formatted}}
                            </span><br>
                                    @if($balloon_single->stock_quantity==0)
                                        <a href="javascript:;" class="outofstock">Out of stock</a>
                                    @endif

                                    {{--                                    <div data-pid="{{$balloon_single->id}}"--}}
                                    {{--                                         class="add-towishlist-container favorite-toggle @if($favorite_active) favorite-toggle-active @endif">--}}
                                    {{--                     <span class="hart favorite-toggle" id="myBtn">--}}
                                    {{--                        <i onclick="wishList(this)"--}}
                                    {{--                           class="fa-heart fa-regular @if($favorite_active) fa-solid @endif"></i>--}}
                                    {{--                        <i class="fas fa-heart" id="myImg"></i>--}}
                                    {{--                    </span>--}}
                                    {{--                                    </div>--}}


                                </div>


                            </div>
                        @endforeach

                    @else
                        <div class="no-product">

                            <span class="small-title">no products found</span>


                        </div>
                    @endif
                </div>
            </div>
        </div>

        @component('components.pagination' , ['products'=>$balloons]) @endcomponent

    </section>
    <script>
        function FooterFunctions() {
            filterCustom();
            onclickClass();

        }
    </script>
@endsection
