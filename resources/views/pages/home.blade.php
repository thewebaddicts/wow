@extends('layouts.main')
{{--@section('title')<title>WOW Surprises UAE - Creators of Happiness</title>@endsection--}}

@section('content')
    <div class="particles"></div>
    <?php


    $collections = ecom('collections')->condition('display_homepage', 1)->condition('cancelled', 0)->condition('collectionID', 1)->condition('ignoreListing', 1)->condition('display', 1)->condition('IncludeProducts', 1)->get();



    ?>
    {{--<span onclick="menuExtra()">yow</span>--}}


    {{--    <div @if($slideshow[0]->theme == 'BLACK') class="slideshow black-theme" @else class="slideshow white-theme" @endif>--}}

    <div class="slideshow black-theme carousel owl-carousel" data-carousel-items="1" data-carousel-dots="false"
         data-carousel-loop="true" data-carousel-nav="false" data-carousel-autoplay="true"
         data-carousel-autowidth="false">


        @foreach ($slideshows as $slideshow)

            <div class="slide {{$slideshow->theme}}-theme" @class="" style=
            "background-image: url('{{ env('DATA_URL') }}/slideshow/{{ $slideshow->id }}.{{ $slideshow->extension_image }}?v={{ $slideshow->version }}')
            ">


            <div class="content">
                <div class="slide-title">
                    <div class="progress"></div>
                    <sep></sep>
                    <h1>{{$slideshow->label}}</h1>
                    <span>{{$slideshow->small_parag}}</span>
                    <sep></sep>
                    <sep></sep>
                    <a href="{{$slideshow->btn_link}}">{{$slideshow->btn_label}}</a>
                </div>

            </div>
    </div>

    @endforeach

    </div>
    <div class="slideshow-mobile black-theme carousel owl-carousel" data-carousel-items="1" data-carousel-dots="false"
         data-carousel-loop="true" data-carousel-nav="false" data-carousel-autoplay="false"
         data-carousel-autowidth="false">


        @foreach ($slideshows as $slideshow)

            <div class="slide {{$slideshow->theme}}-theme" @class="" style=
            "background-image: url('{{ env('DATA_URL') }}/slideshow_mobile_version/{{ $slideshow->id }}.{{ $slideshow->extension_slideshow_mobile }}?v={{ $slideshow->version }}')
            ">


            <div class="content">
                <div class="slide-title">
                    <div class="progress"></div>
                    <sep></sep>
                    <h1>{{$slideshow->label}}</h1>
                    <span>{{$slideshow->small_parag}}</span>
                    <sep></sep>
                    <sep></sep>
                    <a href="{{$slideshow->btn_link}}">{{$slideshow->btn_label}}</a>
                </div>

            </div>
    </div>

    @endforeach

    </div>



    <div class="menu-marker"></div>

    @foreach($collections as $products)

        @if($products->cms_attributes['attributes_position'] == "Top" && $products->cms_attributes['attributes_carousel']==1)
            <section class="products">

                <div>
                    <div class="content">

                        <h1>{{$products['label']}}</h1>


                        <div class="best-sellers-container-carousel owl-carousel carousel single-carousel"
                             data-carousel-nav="false"
                             data-carousel-autoplay="false" data-carousel-items="5" data-carousel-autowidth="true"
                             data-carousel-loop="true">


                            {{--                @php $product = json_decode(json_encode($collections[0])); @endphp--}}





                            @php

                                if(ecom('users')->loginCheck()){

                                         $favorite_active = ecom('favorites')->check($products->id);

                                     }else{
                                         $favorite_active = false;
                                     }

                            @endphp

                            @foreach($products->products->data as $product)
                                @php $product = json_decode(json_encode($product)); @endphp

                                <div class="balloons-product" id="home_product_listing">


                                    <picture>

                                            <a href="en/product/{{$product->slug}}"
                                               style="width: 100%;height:100%;position: absolute;"></a>


                                        <img loading="lazy" src="{{ $product->thumb}}">
                                        <div class="primary-button">
                                            @if($product->stock_quantity!==0)
                                                <a class="click-btn btn-style904" data-hover="" href="javascript;"
                                                   onclick="addToCart('product_id={{ $product->id }}',event); return false;">
                                                    <span class="hover-me">Add to bag</span>
                                                </a>
                                            @endif

                                        </div>
                                    </picture>

                                    {{--                                <button type="button" onclick="addToCart('product_id={{$product->id }}',event); return false;" class="primary-button">Add to Bag</button>--}}

                                    <div class="product-container-details">

                                        <h2>
                                            {{$product->label}}
                                        </h2>
                                        {{--                                    <h3>--}}

                                        {{--                                        {{$product->small_description}}--}}
                                        {{--                                    </h3>--}}
                                        <span>
                               {{$product-> unit_price_formatted}}


                            </span>
                                        <br>

                                        @if($product->stock_quantity==0)
                                            <a href="javascript:;" class="outofstock">Out of stock</a>
                                        @endif

                                        @if($product->stock_quantity!==0)
                                            <div data-pid="{{$product->id}}"
                                                 class="add-towishlist-container favorite-toggle @if($favorite_active) favorite-toggle-active @endif">
                     <span class="hart favorite-toggle" id="myBtn">
                        <i onclick="wishList(this)"
                           class="fa-heart fa-regular @if($favorite_active) fa-solid @endif"></i>
{{--                        <i class="fas fa-heart" id="myImg"></i>--}}


                    </span>
                                            </div>
                                        @endif
                                    </div>


                                </div>
                            @endforeach


                        </div>
                    </div>
                </div>


            </section>
        @endif
    @endforeach
    <sep></sep>
    <sep></sep>
    <section class="create-urbox" data-aos="fade-up">
        <div class="content">
            <div class="create-urbox-container">
                <h1>Customize your gift</h1>
                <a href="{{route('your-box')}}" class="link buzz-out-on-hover">CREATE YOUR OWN </a>
                <div>

                    <img src="/assets/images/createyourbox.png" loading="lazy">
                </div>

            </div>
        </div>
    </section>

    <section class="home-collections" data-aos="fade-up">
        <div class="content">
            <div class="home-collections-container">
                {{--            <div class="home-collections-container">--}}
                @isset($collections)
                    @foreach($collections as $addlink)
                        @if($addlink->display_homepage==1)


                            <div class="link buzz-out-on-hover">
                                <a href="{{$addlink->link}}"></a>
                                <img loading="lazy"
                                     src="{{ env('DATA_URL') }}/ecom_collections/{{ $addlink->id }}.{{ $addlink->extension_image}}?v={{ $addlink->version }}">
                                <span>{{$addlink->label}}</span>
                            </div>
                        @endif

                    @endforeach

                @endisset
            </div>
        </div>
    </section>


    <section class="shop-now">
        <div class="content">
            @foreach($banner as $ban)
                @php
                    $bann = json_decode($ban['cms_attributes']);

                @endphp

                @if($bann->attributes_position == "Top")

                    @component('banner.banner_desktop',compact('ban'))@endcomponent


                @endif
            @endforeach

        </div>

    </section>



    @foreach($collections as $products)
        @if($products->cms_attributes['attributes_position'] == "Bottom" && $products->cms_attributes['attributes_carousel']==1)

            <section class="products" data-aos="fade-up">

                <div>
                    <div class="content">

                        <h1>{{$products['label']}}</h1>


                        <div class="best-sellers-container-carousel owl-carousel carousel single-carousel"
                             data-carousel-nav="false"
                             data-carousel-autoplay="false" data-carousel-items="6" data-carousel-autowidth="true"
                             data-carousel-loop="true">


                            {{--                @php $product = json_decode(json_encode($collections[0])); @endphp--}}



                            @php

                                if(ecom('users')->loginCheck()){

                                         $favorite_active = ecom('favorites')->check($products->id);

                                     }else{
                                         $favorite_active = false;
                                     }

                            @endphp

                            @foreach($products->products->data as $product)
                                @php $product = json_decode(json_encode($product)); @endphp

                                <div class="balloons-product" id="home_product_listing">


                                    <picture>

                                            <a href="en/product/{{$product->slug}}"
                                               style="width: 100%;height:100%;position: absolute;"></a>


                                        <img loading="lazy" src="{{ $product->thumb}}">
                                        <div class="primary-button">
                                            @if($product->stock_quantity!==0)
                                                <a class="click-btn btn-style904" data-hover="" href="javascript;"
                                                   onclick="addToCart('product_id={{ $product->id }}',event); return false;">
                                                    <span class="hover-me">Add to bag</span>
                                                </a>
                                            @endif

                                        </div>
                                    </picture>

                                    {{--                                <button type="button" onclick="addToCart('product_id={{$product->id }}',event); return false;" class="primary-button">Add to Bag</button>--}}

                                    <div class="product-container-details">

                                        <h2>
                                            {{$product->label}}
                                        </h2>
                                        {{--                                    <h3>--}}

                                        {{--                                        {{$product->small_description}}--}}
                                        {{--                                    </h3>--}}
                                        <span>
                               {{$product-> unit_price_formatted}}


                            </span>
                                        <br>
                                        @if($product->stock_quantity==0)
                                            <a href="javascript:;" class="outofstock">Out of stock</a>
                                        @endif

                                        @if($product->stock_quantity!==0)
                                            <div data-pid="{{$product->id}}"
                                                 class="add-towishlist-container favorite-toggle @if($favorite_active) favorite-toggle-active @endif">
                     <span class="hart favorite-toggle" id="myBtn">
                        <i onclick="wishList(this)"
                           class="fa-heart fa-regular @if($favorite_active) fa-solid @endif"></i>
{{--                        <i class="fas fa-heart" id="myImg"></i>--}}


                    </span>
                                            </div>
                                        @endif
                                    </div>


                                </div>

                            @endforeach


                        </div>
                    </div>
                </div>


            </section>
        @endif
    @endforeach
    <section class="shop-now">
        <div class="content">
            @foreach($banner as $ban)
                @php
                    $bann = json_decode($ban['cms_attributes']);

                @endphp

                @if($bann->attributes_position == "Bottom")

                    @component('banner.banner_desktop',compact('ban'))@endcomponent


                @endif
            @endforeach

        </div>

    </section>
    <section class="latest-events" data-aos="fade-up">

        <div>
            <div class="content">

                <h1>Latest events</h1>

            </div>
            <sep></sep>
            <sep></sep>
            <sep></sep>
            <sep></sep>
            <sep></sep>
            <sep></sep>
            <sep></sep>
            <sep></sep>
            <sep></sep>
            <sep></sep>
            <sep></sep>
            <sep></sep>

            <div class="latest-events-container-carousel owl-carousel single-carousel" data-carousel-nav="true"
                 data-carousel-autoplay="true" data-carousel-autowidth="true" data-carousel-loop="true"
                 data-carousel-items="1">

                @foreach($events as $single_events)

                    <div class="single-product" onclick="openPage({{$single_events['id']}})">
                        <picture><img loading="lazy"
                                      src="{{ env('DATA_URL') }}/events/{{ $single_events['id'] }}.{{ $single_events['extension_image']}}">
                        </picture>
                        <div>

                            <span>{{$single_events['title']}}</span>
                            {{--                            <span><img loading="lazy" src="/assets/svgs/arrow-right.svg"></span>--}}

                        </div>


                    </div>
                @endforeach

            </div>

        </div>


    </section>
    <sep></sep>
    <sep></sep>
    <sep></sep>
    <sep></sep>
    <sep></sep>
    <script>
        function FooterFunctions() {

            checkClass();
        }

        function getCookie() {

            var cookies = document.cookie
                .split(';')
                .map(cookie => cookie.split('='))
                .reduce((accumulator, [key, value]) => ({
                    ...accumulator,
                    [key.trim()]: decodeURIComponent(value)
                }), {});

            if (!cookies.email) {
                text = "Join our newsletter And benefit from 10% Off on your first order";
                $.fancybox.open(
                    '<form  class="newsletter-form" action="{{route('newsletter')}}" name="" method="post">@csrf <div  class="message"><div class="x-message-top"></div><h1>' + text + "</h1><input id='b' type='email' name='email' value='' required><button  id='blur' class='join' type='submit'  onclick='setCookie(email);'>JOIN</button></div></form>"
                );

            } else ;
        }

        function setCookie(cvalue) {

            let value = cvalue.value;
            var d = new Date();

            d.setTime(d.getTime() + 2 * 24 * 60 * 60 * 1000);
            var expires = "expires=" + d.toUTCString();
            document.cookie = "email" + "=" + value + ";" + expires + ";path=/";
        }


        function openPage(id) {

            $.fancybox.open(
                '<picture><img src="{{ env('DATA_URL') }}/events/' + id + '.{{ $single_events->extension_image}}"></picture>'
            );
        }

    </script>


@endsection

