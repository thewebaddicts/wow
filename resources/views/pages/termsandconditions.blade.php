@extends('layouts.main')
@section('content')
@php

    $termsandconditions = getter('terms_conditions')->get();

@endphp

    <section class="privacycookiesterms">
        <div class="content">
            @component('partials.page-path')@endcomponent
@foreach($termsandconditions as $termsandcondition)
            <h3>{{$termsandcondition->title}}</h3>
            <p>{!!nl2br($termsandcondition->text)!!}</p>
            @endforeach

        </div>
    </section>
@endsection
