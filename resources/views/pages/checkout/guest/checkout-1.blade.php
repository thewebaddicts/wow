@extends('layouts.main')
@section('content')

    <?php $PaymentMethods = ecom('stores')->getPayments(); ?>

    <?php $ShippingMethods = ecom('stores')->getShipping(); ?>

    <?php     $addresses = ecom('addresses')->list();?>

    <?php     $profile = ecom('profile')->get();?>
    <?php     $cart = ecom('cart')->getAsObject();?>

    @php


        $code = isset($cart->voucher_code) ? $cart->voucher_code : "";

    $code_value = "";
    $valid = false;

    if(isset($code) && !empty($code)){
    $code_value = $code;
    $valid = true;
    }

    @endphp
    <section class="placeorder-section" id="guest-section">
        <div class="content">
            @component('partials.page-path')@endcomponent
            <form style="display: contents" method="post" action={{route('store-guest-checkout')}}>
                @csrf
                <input type="hidden" name="shipping_method" value="2">


                <div class="container">


                    <div class="address-container checkout-1">
                        <span class="small-title">PAYMENT METHOD</span>
                        <main>
                            @foreach($PaymentMethods as $pay)

                                <div class="checkmark-parent">
                                    <span class="checkmark2"></span>
                                    <input type="radio" name="payment_method" value="{{$pay->id}}">
                                    <span>{{$pay->label}}</span>
                                </div>

                            @endforeach
                        </main>
                      <br>
                        <span class="small-title">GENERAL INFORMATION</span>

                        @component('forms.general-information')@endcomponent
                      <br>



                        <span class="small-title">SHIPPING ADDRESS</span>


                        @component('forms.shipping-address-guest')@endcomponent

                       <br>
                        <span class="small-title">BILLING ADDRESS</span>
                        @component('forms.billing-address-guest')@endcomponent
<br>
                        <button>REGISTER</button>

                    </div>



                    <div class="cart">

                        <div class="cart-breakdown" style="display: contents">
                            <div class="cart-sub">
                                <div class="small-title">ORDER SUMMARY</div>
                                <sep></sep>

                                @foreach($cart->items->items as $carts)
                                    @if($carts->quantity>0)

                                        <div class="cart-row">
                                            <picture><img
                                                        src="{{ env('DATA_URL') }}/products/{{ $carts->details->id }}.{{ $carts->details->extension_image }}?v={{ $carts->details->version }}">
                                            </picture>

                                            <div class="info">
                                                <span class="info-title">{{$carts->details->label}}</span>
                                                <span class="info-desc">{{$carts->details->small_description}}</span>

{{--                                                <span class="info-price">{{$carts->details->unit_price_formatted}}</span>--}}
                                            </div>
                                            <div class="quantity">

                                                {{$carts->quantity}}
                                            </div>

                                            <div class="price">{{$carts->final_price_formatted}}</div>
                                        </div>
                                    @endif
                                @endforeach
                                <br>
                                <div class="subtotal"><span>SUBTOTAL</span><span>{{$cart->subtotal}}</span></div>
                                <br>
                                <div class="subtotal"><span>SHIPPING FEES</span><span>{{$cart->shipping}}</span></div>
                               <br>
                                <div class="total"><span>TOTAL</span><span>{{$cart->grandTotal_formated}}</span></div>
                            </div>

                        </div>

                        <sep></sep>
                        <sep></sep>
                        {{--                        <button>CHECKOUT</button>--}}
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection