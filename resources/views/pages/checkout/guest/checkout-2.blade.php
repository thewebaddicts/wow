@extends('layouts.main')
@section('content')
    @php
        $info = session('info');

    @endphp
    <?php $PaymentMethods = ecom('stores')->getPayments(); ?>



    <?php $cart = ecom('cart')->getAsObject();?>

    <section class="placeorder-section">
        <div class="content">
            @component('partials.page-path')@endcomponent
            <form style="display: contents" method="post" action={{route('place-order-guest')}}>


                <input type="hidden" name="billing_address" value="{{request('billing_address')}}">
                <input type="hidden" name="shipping_address" value="{{$info->shipping_address->id}}">
                <input type="hidden" name="shipping_method" value="2">
{{--                <input type="hidden" name="payment_method" value="{{$info->payment_method}}">--}}

                <div class="container">


                    <div class="address-container">
                        <span class="small-title">PAYMENT MENTHOD</span>

                        <select class="billing-address" onchange="getOption(this)" name="payment_method">
                            @foreach($PaymentMethods as $pay)
                                @if($pay->id == request('payment_method'))
                                    <option value="{{$info->payment_method}}">{{$pay->label}}</option>
                                @endif
                                {{--                            <option id="selec" value="">Pick A New Billing address</option>--}}
                            @endforeach
                        </select>
                        <span class="small-title">GENERAL INFORMATION</span>
                        <main id="guest-shipping-billing">
                            <div>

                                <span>First Name: {{$info->user->first_name}}</span>

                                <span>Last Name: {{$info->user->last_name}}</span>

                                <span>Mobile: {{$info->user->phone}}</span>

                                <span>E-mail: {{$info->user->email}}</span>
                            </div>

                        </main>
                        <sep></sep>
                        <sep></sep>
                        <sep></sep>
                        <span class="small-title">SHIPPING ADDRESS</span>
                        <main id="guest-shipping-billing">
                            <div>

                                <span>First Name: {{$info->shipping_address->first_name}}</span>

                                <span>Last Name: {{$info->shipping_address->last_name}}</span>

                                <span>Phone: {{$info->shipping_address->phone}}</span>
                                <span>Destination: {{$info->shipping_address->label}}</span>
                                <span>State: {{$info->shipping_address->state}}</span>
                                <span>City: {{$info->shipping_address->city}}</span>
                                <span>Street: {{$info->shipping_address->street}}</span>
                                <span>Floor: {{$info->shipping_address->floor}}</span>
                                <span>Building: {{$info->shipping_address->building}}</span>

                            </div>

                        </main>
                        <sep></sep>
                        <sep></sep>
                        <sep></sep>
                        <span class="small-title">BILLING ADDRESS</span>
                        <main id="guest-shipping-billing">
                            <div>
                                <span>First Name: {{$info->billing_address->first_name}}</span>
                                <span>Last Name: {{$info->billing_address->last_name}}</span>
                                <span>Phone: {{$info->billing_address->phone}}</span>
                                <span>Destination: {{$info->billing_address->label}}</span>
                                <span>State: {{$info->billing_address->state}}</span>
                                <span>City: {{$info->billing_address->city}}</span>
                                <span>Street: {{$info->billing_address->street}}</span>
                                <span>Floor: {{$info->billing_address->floor}}</span>
                                <span>Building: {{$info->billing_address->building}}</span>
                            </div>
                        </main>


                    </div>


                    <div class="cart">

                        <div class="cart-breakdown" style="display: contents">
                            @component('components.cart-breakdown',['cart'=>$cart])@endcomponent
                        </div>

                        <sep></sep>

                        <sep></sep>
                        <button>CHECKOUT</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection