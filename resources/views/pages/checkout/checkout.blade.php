@extends('layouts.main')
@section('content')

    <?php $addresses = ecom('addresses')->list();?>

    <?php     $profile = ecom('profile')->get();?>
    <?php     $cart = ecom('cart')->getAsObject();?>
    <section class="checkout-section">


        <div class="content">
            @component('partials.page-path')@endcomponent
            <div class="container">


                <div class="address-container">
                    <span class="small-title">SHIPPING ADDRESS</span>
                    <small>Please choose your shipping address</small>
<sep></sep>
<sep></sep>
                    <main>
                        <form style="display: contents;" method="get" action="{{route('billing')}}" id="mainform">
                            @csrf
                            {{--                            <input type="hidden" name="shipping_address" value="{{$address->id}}">--}}
                            <input type="hidden" name="shipping_method" value="2">
                            @foreach($addresses as $address)
                                <div class="checkmark-parent2">
                                    <span class="checkmark"></span>

                                    <input type="radio" name="shipping_address" value="{{$address->id}}">

                                    <span>{{$address->label}}</span>
                                    <sep></sep>

                                    <span>{{$profile->first_name}}&nbsp;&nbsp;{{$profile->last_name}}</span>
                                    <span>{{$address->city}},&nbsp;&nbsp;{{$address->street}},&nbsp;&nbsp;{{$address->building}}&shy;Building,&nbsp;&nbsp;{{$address->floor}}&nbsp;&nbsp;Floor
                        </span>
                                    <span class="settings">
{{--                           <span> <img src="/assets/svgs/Icon material-edit.svg">EDIT</span>--}}
                                        {{--                        <span id="remove-address">    <img src="/assets/svgs/Icon material-delete.svg">REMOVE</span>--}}
                           </span>

                                </div>
                            @endforeach

                        </form>

                    </main>

                    <a href="{{route('addresses-create')}}" style="position: absolute;
    right: 0;margin: 20px;">
                        <img src="/assets/svgs/add-address.svg"></a>
                    <button disabled onclick="submitform($('#mainform').serialize()); return false;">CONTINUE TO
                        PAYMENT
                    </button>
                </div>
                <div class="cart">
                    <div class="cart-sub">
                        <div class="small-title">Order Summary</div>
                        <sep></sep>

                        @foreach($cart->items->items as $carts)
                            @if($carts->quantity>0)
                                <div class="cart-row">
                                    <picture><img
                                                src="{{ env('DATA_URL') }}/products/{{ $carts->details->id }}.{{ $carts->details->extension_image }}?v={{ $carts->details->version }}">
                                    </picture>

                                    <div class="info">
                                        <span class="info-title">{{$carts->details->label}}</span>
                                        <span class="info-desc">{{$carts->details->small_description}}</span>
                                        <span class="info-price">{{$carts->details->unit_price_formatted}}</span>
                                    </div>
                                    <div class="quantity">

                                        {{$carts->quantity}}
                                    </div>
                                    <div class="price">{{$carts->details->unit_price_formatted}}</div>
                                </div>
                            @endif
                        @endforeach

                        <sep></sep>
                        <sep></sep>
                        <div class="subtotal"><span>SUBTOTAL</span>


                            <span class="price">{{$cart->breakdown->Subtotal}}</span>


                        </div>
                        <sep></sep>
                        <sep></sep>
                        @isset($cart->breakdown->Discount)
                        <div class="subtotal"><span>DISCOUNT</span>


                                <span class="price">{{$cart->breakdown->Discount}}</span>


                        </div>
                        @endisset
                        <sep></sep>
                        <sep></sep>
                        <div class="subtotal"><span>SHIPPING FEES</span><span>{{$cart->shipping}}</span></div>
                        <sep></sep>
                        <sep></sep>
                        <div class="total"><span>TOTAL</span>

                                <span class="price">{{$cart->grandTotal_formated}}</span>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection