@extends('layouts.main')

@section('content')
    <?php $PaymentMethods = ecom('stores')->getPayments(); ?>

    <?php $ShippingMethods = ecom('stores')->getShipping(); ?>
    <?php $addresses = ecom('addresses')->list();?>

    <?php     $profile = ecom('profile')->get();?>
    <?php     $cart = ecom('cart')->getAsObject();?>

    <section class="placeorder-section">
        <div class="content">
            @component('partials.page-path')@endcomponent
            <form style="display: contents" method="post"  action={{route('checkout-placeorder')}}>
               @csrf


                <input type="hidden" name="shipping_address" value="{{request('shipping_address')}}">
                <input type="hidden" name="shipping_method" value="2">

                <div class="container">


                    <div class="address-container">
                        <span class="small-title">BILLING ADDRESS</span>

                        <select class="billing-address"  onchange="getOption(this)" name="billing_address">
                            <option  value="{{request()->shipping_address}}">Same As Shipping Address</option>
                            <option id="selec" value="">Pick A New Billing address</option>
                        </select>
                        <main  id="billing">
{{--                            <form style="display: contents;" method="get" action="{{route('billing')}}" id="mainform">--}}
{{--                                @csrf--}}

                                @foreach($addresses as $address)

                                    <div class="checkmark-parent2">
                                        <span class="checkmark"></span>

                                        <input type="radio" name="billing_address" value="{{$address->id}}">

                                        <span>{{$address->label}}</span>
                                        <sep></sep>

                                        <span>{{$profile->first_name}}&nbsp;&nbsp;{{$profile->last_name}}</span>
                                        <span>{{$address->city}},{{$address->street}}, {{$address->building}}Building,{{$address->floor}}Floor,
                        </span>
                                        <span class="settings">
{{--                           <span> <img src="/assets/svgs/Icon material-edit.svg">EDIT</span>--}}
{{--                        <span id="remove-address" class="remove-card">    <img src="/assets/svgs/Icon material-delete.svg">REMOVE</span>--}}
                           </span>

                                    </div>
                                @endforeach

{{--                            </form>--}}

                        </main>

                        <small>Please choose your payment method</small>
                        <sep></sep>
                        <sep></sep>
                        <span class="small-title">PAYMENT METHOD</span>


                        <main>

                            @foreach($PaymentMethods as $pay)
                                <div class="checkmark-parent">
                                    <span class="checkmark2"></span>
                                    <input type="radio" name="payment_method" value="{{$pay->id}}">
                                    <span>{{$pay->label}}</span>
                                </div>

                            @endforeach
                        </main>


                    </div>
                    <?php if (isset($_GET['address'])) {
                        $address = $_GET['address'];
                    }
                    ?>

                    <div class="cart">

                    <div class="cart-breakdown" style="display: contents">
                            @component('components.cart-breakdown',['cart'=>$cart])@endcomponent
                    </div>

                        <sep></sep>
                        @foreach($addresses as $address)
                            @if($address->id == request()->address)

                                <div class="cart-shipping">
                                    <span>Shipping Address</span>
                                    <sep></sep>
                                    <span>{{$address->label}}</span>
                                    <div class="cart-shipping-info">
                                        <span>{{$address->first_name}}&nbsp;{{$address->last_name}}</span>
                                        <br>
                                        <span>{{$address->city}},{{$address->street}},{{$address->state}}</span>
                                        <span>{{$address->building}}Building, {{$address->floor}}Floor</span>
                                    </div>
                                    <span id="remove-address">    <img
                                                src="/assets/svgs/Icon material-delete.svg"><span>REMOVE</span></span>

                                </div>
                            @endif
                        @endforeach
                        <sep></sep>
                        <button>CHECKOUT</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection