@extends('layouts.main')
@section('content')
    @php

        $privacypolicy = getter('privacy_policy')->get();

    @endphp

    <section class="privacycookiesterms">
        <div class="content">
            @component('partials.page-path')@endcomponent
            @foreach($privacypolicy as $privacypolicies)
                <h3>{{$privacypolicies->title}}</h3>
                    <p>{!!nl2br($privacypolicies->text)!!}
                    </p>
            @endforeach

        </div>
    </section>
@endsection
