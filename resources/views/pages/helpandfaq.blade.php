@extends('layouts.main')
@section('content')

    <div class="topbanner">
        <figure>
            <img src="/assets/images/Mask Group 30.png" alt="">
        </figure>
        <figcaption>
            <span>Home / Help & FAQS</span>
            <h1> Help & FAQS</h1>
            <h2> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</h2>

        </figcaption>
    </div>
    <section class="account-info">
        <div class="content">

            <div>
                @php $helpfaq = getter('help_faq')->condition('cancelled',0)->get(); @endphp
                <form style="display: contents">


                    <select id="sort-by-name" onchange="window.location.href=this.value;">
                        <option>Pick a page</option>
                        @foreach($helpfaq as $help)

                            <option value="{{$help->id}}">{{$help->side_bar}}</option>
                        @endforeach
                    </select>


                </form>
                <ul>


                    @foreach($helpfaq as $help)

                        <li id="helpfaq"><a
                                    href="{{ecom('url')->prefix()}}/helpandfaq/{{$help->id}}">{{$help->side_bar}}</a>
                            @if(request()->id == $help->id)

                                <img id="arrow-right" src="/assets/svgs/Icon ionic-ios-arrow-back.svg">
                            @endif
                        </li>


                    @endforeach
                </ul>

                <main>


                    <h3>{{$helpandfaq[0]->side_bar}}</h3>
                    @php
                        $helpjson = json_decode($helpandfaq[0]->contents)
                    @endphp
                    @foreach($helpjson as $hell)



                        <ul style="display:contents;line-height: 2em;">
                            <li class="accordion" style="position: relative">
                                <a><span>{{$hell->title}}</span>
                                    <span style="display: inline-block;width: 20px;height: 20px;    position: absolute;right: 0;top: 3px;">
                                        <img class="expand" src="/assets/svgs/Group 76.svg" alt=""
                                             style="width: 100%;height: 100%;object-fit: contain">
                                    </span></a>
                                <ul class="expandable sublist" style="line-height: 2em;height: 325px;font-family:'restora';color: #00000078;
                                list-style: none;
                                padding-left: 0;
                                font-size: 14px;width: unset">
                                    <li>
                                        {{$hell->text}}
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    @endforeach
                </main>


            </div>
        </div>
    </section>
@endsection