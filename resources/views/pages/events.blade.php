@extends('layouts.main')
{{--@section('title')<title>Events | {{ __('texts.wow') }}</title>@endsection--}}

@section('content')
    @php    $all = App\Models\Events::where('cancelled', 0)->get(); @endphp
    @php    $corporate = App\Models\Events::where('cancelled', 0)->where('type','Corporate Event')->get(); @endphp
    @php    $private = App\Models\Events::where('cancelled', 0)->where('type','Private Event')->get(); @endphp
    <section class="events-page">
        <div class="content">
            @component('partials.page-path')@endcomponent

            <main>
                <div class="event-header-nav">
                    <div>
                        <select name="forma" onchange="events(this,this.value)" class="sidebar-mobile">
                            <option value="1">ALL</option>
                            <option value="2">CORPORATE EVENTS</option>
                            <option value="3">PRIVATE EVENTS</option>

                        </select>
                        <div class="event-links">
                            <a class="selects link buzz-out-on-hover" onclick="events(this,1)">ALL</a>
                            <a class="link buzz-out-on-hover" onclick="events(this,2)">CORPORATE EVENTS</a>
                            <a class="link buzz-out-on-hover" onclick="events(this,3)">PRIVATE EVENTS</a>
                        </div>
                        <a href="{{route('events')}}" class="more-events">
                            REACH OUT FOR A WOW EVENT
                        </a>
                    </div>
                </div>

                <sep></sep>


                <div class="container-events active" data-id="1">
                    @foreach($all as $single_events)

                        <div>
                            <a href="#"></a><img
                                    src="{{ env('DATA_URL') }}/events/{{  $single_events->id }}.{{  $single_events->extension_image }}?v={{  $single_events->version }}">
                            <div class="event-onhover">
                                <span class="title">{{$single_events->title}}</span>
                                <sep></sep>
                                <span class="small-desc">{{$single_events->small_description}}</span>
                                <sep></sep>
                                <span class="location">{{$single_events->location}}</span>
                            </div>
                        </div>

                    @endforeach
                </div>
                <div class="container-events" data-id="2">
                    @foreach($corporate as $single_events)

                        <div><a href="#"></a> <img
                                    src="{{ env('DATA_URL') }}/events/{{  $single_events->id }}.{{  $single_events->extension_image }}?v={{  $single_events->version }}">
                            <div class="event-onhover">
                                <span class="title">{{$single_events->title}}</span>
                                <sep></sep>
                                <span class="small-desc">{{$single_events->small_description}}</span>
                                <sep></sep>
                                <span class="location">{{$single_events->location}}</span>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="container-events" data-id="3">
                    @foreach($private as $single_events)

                        <div><a href="#"></a> <img
                                    src="{{ env('DATA_URL') }}/events/{{  $single_events->id }}.{{  $single_events->extension_image }}?v={{  $single_events->version }}">
                            <div class="event-onhover">
                                <span class="title">{{$single_events->title}}</span>
                                <sep></sep>
                                <span class="small-desc">{{$single_events->small_description}}</span>
                                <sep></sep>
                                <span class="location">{{$single_events->location}}</span>
                            </div>
                        </div>
                    @endforeach
                </div>


            </main>


        </div>


    </section>

@endsection