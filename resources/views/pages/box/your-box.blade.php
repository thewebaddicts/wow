@extends('layouts.main')
@section('title')<title>Create your box | {{ __('texts.wow') }}</title>@endsection

@section('content')
    <?php

    $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $uri_segments = explode('/', $uri_path);
    $series = str_replace('-', ' ', $uri_segments);
    //    $boxes = ecom('products')->condition('cancelled', 0)->condition('keywords', 11)->condition('display', "full")->condition('display', 1)->condition('includeVariationDetails',1)->get()->toArray();
    $boxes = ecom('products')->condition('display', "full")->condition('includeVariationDetails', 1)->condition('numberOfProducts', 100)->condition('cancelled', 0)->condition('keywords', getter('create_your_box_keywords')->condition('location', 'boxes')->get()->pluck('ecom_keywords_id'))->get()->toArray();

    ?>

    <section class="create-your-box">
        <div class="content">
            <div class="path">
                <a href="/"><span>Home</span></a><img src="/assets/svgs/arrow-right.svg"><span>{{$series[3]}}<img
                            src="/assets/svgs/arrow-right.svg">{{$series[4]}}</span>
            </div>


            <h1 class="path-title">
                {{$series[4]}}
            </h1>

            {{--            --}}
            <div class="path" style="color: black">Step 1 — Choose Your Box</div>
            <sep></sep>
            <sep></sep>
            <sep></sep>
            <div class="create-box-container">


                @foreach($boxes['data'] as $boxes)


                    {{--                    @php--}}
                    {{--                        $product_fam_id = $boxes['family_group_id'];--}}

                    {{--                            $provar = getter('ecom_products')->condition('family_group_id' , $product_fam_id)->get();--}}

                    {{--                                $primary_variation = getter('ecom_products_variation_primaries')->get();--}}

                    {{--                    @endphp--}}
                    <div>

                        <picture>
                            <img src="{{$boxes['thumb']}}">
                        </picture>
                        <span class="small-title">{{$boxes['label_en']}}</span>
                        <span class="price">{{$boxes['unit_price_formatted']}}</span>
                        <div class="create-your-box-variation">


                                @isset($boxes['variation']['primary_variations'])
                                    @foreach($boxes['variation']['primary_variations'] as $variation)

                                        <div>
                                            <span class="small-title">{{$variation['label']}}</span>
                                            <span class="italic-small-title">{{$variation['code']}}&nbsp;items</span>
                                            <input type="hidden" name="small">

                                            <a href="{{ ecom("url")->prefix() }}/boxes/your-box/{{ Str::slug($boxes['slug'])}}/{{ Str::slug($variation['label']) }}/{{$variation['id']}}"
                                               data-link="{{ ecom("url")->prefix() }}/boxes/your-box/{{ Str::slug($boxes['slug'])}}/{{ Str::slug($variation['label']) }}/{{$variation['id']}}"
                                               class=""></a>

                                        </div>

                                    @endforeach

                                @endisset


                        </div>

                    </div>
                @endforeach

            </div>
        </div>

    </section>
@endsection