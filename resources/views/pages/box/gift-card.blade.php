@extends('layouts.main')
@section('content')

    <?php


    try {
        $items = $cartList->items->items;
    } catch (\Throwable $th) {
        $items = [];
    }
    $available_quantities = \App\Http\Controllers\CartController::getAvailableQuantities($items);


    $store_id = ecom('stores')->getCurrent()->id;
    $category = ecom('products')->condition('display', 'card')->condition('ignoreListing', 0)->condition('includeVariationDetails', 0)->condition('cancelled', 0)->addField('weight')->get()->toArray();

    $category = ucwords(str_replace('-', ' ', $category));
    $category_label = $category;
    //    $category = getter('ecom_products')->condition('family_group_id', "LIKE", $category_label)->condition('family_group_id', "LIKE", $category_label)->get()->first();

    if (!$category) {

        $category = getter('ecom_products')->condition('label_' . app()->getLocale(), "LIKE", $category_label)->get()->first();
        if (!$category) {

            echo '<script language="javascript">window.location="' . ecom("url")->prefix(true) . '/box/create/classic%20box/";</script>';
            exit();
        }
    }

    // $boxes = ecom('products')->condition('store', $store_id)->condition('ignoreListing', 1)->get()->toArray();
    $store = ecom('stores')->getCurrent();

    //    if (isset($id)) {
    //        $current = ecom('products')->condition('store', $store_id)->condition('display', 'full')->condition('subcategories', $category->id)->condition('productID', $id)->condition('ignoreListing', 1)->get()->toArray();
    //    } else {
    //        echo '<script language="javascript">window.location="' . url()->current() . '/' . $boxes['data'][0]['id'] . '/' . Str::slug($boxes['data'][0]['label']) . '"</script>';
    //        exit();
    //    }
    //    if ($current) {
    //        try {
    //            $current = $current["data"][0];
    //        } catch (\Throwable $e) {
    //            $current = false;
    //        }
    //    }
    $filters = [];







    $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $uri_segments = explode('/', $uri_path);
    $series = str_replace('-', ' ', $uri_segments);
    $box = ecom('products')->condition('id', request()->segment(6))->condition('cancelled', 0)->condition('display', "card")->get()->toArray();

    $cartList = ecom('cart')->getAsObject();
    $cart = $cartList->items->items;
    $product = ecom('products')->condition('display', "full")->condition('cancelled', 0)->get()->toArray();
    $cust = ecom('products')->condition('display', "full")->condition('cancelled', 0)->condition('keywords', "13")->condition('includeVariationDetails', 1)->get()->toArray();
    $ball = ecom('products')->condition('display', "full")->condition('cancelled', 0)->condition('keywords', "14")->condition('includeVariationDetails', 1)->get()->toArray();

    //    echo '<script language="javascript">window.location="'.url()->current().'/'.$box['data'][0]['id'].'/'.Str::slug($box['data'][0]['label']).'"</script>'; exit();
    $boxes = ecom('products')->condition('keywords', '11')->condition('cancelled', 0)->condition('display', "full")->condition('includeVariationDetails', 1)->get()->toArray();
    $wrap = ecom('products')->condition('keywords', '18')->condition('cancelled', 0)->condition('display', "full")->condition('includeVariationDetails', 1)->get()->toArray();

    ?>



    <form id="BYBX_form" method="post" action="{{route('postbox')}}">
        {{--    <form id="BYBX_form" method="get" action="{{route('choose-balloon')}}" name="formdata">--}}
        @csrf
        <section class="create-your-box-id">
            <div class="content">
                <div class="path">
                    <span>Home</span><img src="/assets/svgs/arrow-right.svg"><span
                            style="text-transform: capitalize">{{$series[3]}}<img src="/assets/svgs/arrow-right.svg">{{$series[4]}}</span>
                </div>


                <h1>
                    {{$series[4]}}
                </h1>

                {{--            --}}
                <div class="small-title">Step 3 — Choose your wrap</div>
                <sep></sep>
                <sep></sep>
                <sep></sep>
                <main>

                    <div class="inside-box-content">

                        <div style="display:contents;" class="card-row card-row-{{$box['data'][0]['id']}}"
                             data-unit-price="{{$box['data'][0]['price_ttc']}}"></div>

                        <input type="hidden" name="product_id[]" value="{{$box['data'][0]['id']}}" class='users'>
                        <input type="hidden" name="quantity{{$box['data'][0]['id']}}" value="1" class='users'>
                        {{--                        <picture><img src="{{$box['data'][0]['image']}}">--}}
                        {{--                        </picture>--}}
                        {{--                        <div>--}}
                        {{--                            <span class="small-title">Blue box</span>--}}
                        {{--                            <span class="price">{{$box['data'][0]['unit_price_formatted']}}</span>--}}
                        {{--                        </div>--}}
                        {{--                    --}}
                        <div class="variation">
                            @foreach($boxes['data'] as $bo)

                                @isset($bo['variation'])
                                    @foreach($bo['variation'] as $var)
                                        @foreach($var as $variation)

                                            {{--                                            <a href="{{ ecom("url")->prefix() }}/boxes/your-box/{{$bo['family_group_id']}}/{{ $variation['id'] }}/{{ Str::slug($variation['label']) }}/{{$variation['id']}}"--}}
                                            {{--                                               data-link="{{ ecom("url")->prefix() }}/boxes/your-box/{{$bo['family_group_id']}}/{{ $variation['id'] }}/{{ Str::slug($variation['label']) }}/{{$variation['id']}}"--}}
                                            {{--                                               class=""></a>--}}
                                            <a href="{{ ecom("url")->prefix() }}/boxes/your-box/{{ $bo['family_group_id']}}/{{ $bo['id'] }}/{{ Str::slug($variation['label']) }}/{{$variation['id']}}"
                                               @if(request()->segment(8)==$variation['id']) class="detect" @endif>{{$variation['label']}}</a>


                                        @endforeach
                                    @endforeach
                                @endisset
                            @endforeach
                        </div>
                        <sep></sep>
                        <sep></sep>


                        <main class="variation-card">

                            @foreach($boxes['data'] as $bo)
                                @if(isset($bo['variation']))
                                    @foreach($bo['variation'] as $var)
                                        @foreach($var as $variation)
                                            @if($variation['id']==request()->segment(8))
                                                <div class="small-title limit" id="{{$variation['code']}}">Up
                                                    to {{$variation['code']}} items from the store
                                                </div>
                                            @else

                                            @endif
                                        @endforeach
                                    @endforeach
                                @endif
                            @endforeach

                            @foreach($product['data'] as $basicitem)

                                @php
                                    $qty = request()->has('quantity'.$basicitem['id']) ? request()->input('quantity'.$basicitem['id']) : '0'
                                @endphp


                                    <div class="card-row card-row-{{$basicitem['id']}} @if($qty > 0 ) active @endif"
                                         data-unit-price="{{$basicitem['price_ttc']}}">
                                        <picture><img src="{{ $basicitem['image']}}"></picture>

                                        <div class="lol">
                                            <input type="hidden" name="quantity[]" value="{{$qty}}">

                                            <span>{{$basicitem['label']}}</span>
                                            <div style="display: block;font-style: italic">
                                                <span>( </span> <span
                                                        class="demo card-row-{{$basicitem['id']}}"> {{$qty}} </span>
                                                <span> piece) </span>
                                            </div>

                                            <div class="price">{{$basicitem['unit_price_formatted']}}</div>
                                        </div>
                                        <div class="remove" onclick="removeFromBox('{{$basicitem['id']}}')"><img
                                                    src="/assets/svgs/Icon material-delete.svg"><span>REMOVE</span>
                                        </div>
                                    </div>

                            @endforeach

                            @foreach($cust['data'] as $basicitem)

                                @php
                                    $qty = request()->has('quantity'.$basicitem['id']) ? request()->input('quantity'.$basicitem['id']) : 0;

                                @endphp

                                <div class="card-row card-row-{{$basicitem['id']}}"
                                     data-unit-price="{{$basicitem['price_ttc']}}">
                                    <picture><img src="{{ $basicitem['image']}}"></picture>

                                    <div class="lol">


                                        <span>{{$basicitem['label']}}</span>
                                        <div style="display: block;font-style: italic;">
                                            <span>( </span> <span
                                                    class="demo card-row-{{$basicitem['id']}}"> {{$qty}} </span> <span> piece) </span>
                                        </div>

                                        <div class="price">{{$basicitem['unit_price_formatted']}}</div>
                                    </div>
                                    <div class="remove" onclick="removeFromBox('{{$basicitem['id']}}')"><img
                                                src="/assets/svgs/Icon material-delete.svg"><span>REMOVE</span></div>
                                </div>

                            @endforeach
                            @foreach($ball['data'] as $basicitem)

                                @php
                                    $qty = request()->has('quantity'.$basicitem['id']) ? request()->input('quantity'.$basicitem['id']) : 0
                                @endphp

                                <div class="card-row card-row-{{$basicitem['id']}}"
                                     data-unit-price="{{$basicitem['price_ttc']}}">
                                    <picture><img src="{{ $basicitem['image']}}"></picture>

                                    <div class="lol">


                                        <span>{{$basicitem['label']}}</span>
                                        <div style="display: block;font-style: italic;">
                                            <span>( </span> <span
                                                    class="demo card-row-{{$basicitem['id']}}"> {{$qty}} </span> <span> piece) </span>
                                        </div>

                                        <div class="price">{{$basicitem['unit_price_formatted']}}</div>
                                    </div>
                                    <div class="remove" onclick="removeFromBox('{{$basicitem['id']}}')"><img
                                                src="/assets/svgs/Icon material-delete.svg"><span>REMOVE</span></div>
                                </div>

                            @endforeach


                            <div id="subtotal" class="subtotal">
                                <div class="small-title">SUBTOTAL</div>

                                <span data-currency-symbol="AED" data-currency-rate="10" class="value">0 AED</span>

                            </div>
                        </main>
                        <div class="add-to-cart">
                            {{--                            <a onclick="document.getElementById('BYBX_form').submit();" class="nextclass">NEXT</a>--}}
                            <main class="addtocartinvisiblewrap">
                                <a onclick="box(this,2)">CHOOSE A WRAPPING
                                    PAPER</a>
                                <sep></sep>
                                <a onclick="box(this,3)">CHOOSE A BALLOON</a>
                                <sep></sep>
                                <span>(Optional)</span>
                                <sep></sep>
                                <button id="postBox">
                                    Add to Cart
                                </button>
                            </main>
                        </div>
                        <sep></sep>
                        <sep></sep>
                        <sep></sep>
                    </div>
                    <div class="items-inside-box-content">
                        {{--                        <a class="true" onclick="box(this,1)">BASIC ITEMS</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--}}
                        {{--                        <a onclick="box(this,2)">CUSTOMIZABLE ITEMS</a>--}}
                        <sep></sep>
                        <sep></sep>


                        <div class="container active" data-id="1">

                            @foreach($wrap['data'] as $basicitem)

                                <div class="lol">

                                    <picture><img src="{{ $basicitem['image']}}"></picture>
                                    <span class="small-title">{{$basicitem['label']}}</span>

                                    <span class="price">{{$basicitem['unit_price_formatted']}}</span>
                                    <input data="{{$basicitem['id']}}" type="text" max=""
                                           name="quantity{{ $basicitem['id'] }}"
                                           value="@if(request()->has('quantity'.$basicitem['id'])){{request()->input('quantity'.$basicitem['id'])}}@else{{0}}@endif">
                                    <a onclick="EcomQuantityIncrease(this , '{{$basicitem['id']}}')">Choose</a>
                                    <input type="hidden" name="product_id[]" value="{{$basicitem['id']}}" class='users'>

                                </div>

                            @endforeach


                        </div>
                                                <div class="container" data-id="2">

                                                    @foreach($wrap['data'] as $basicitem)

                                                        <div class="lol">

                                                            <picture><img src="{{ $basicitem['image']}}"></picture>
                                                            <span class="small-title">{{$basicitem['label']}}</span>

                                                            <span class="price">{{$basicitem['unit_price_formatted']}}</span>
                                                            <input data="{{$basicitem['id']}}" type="text" max=""
                                                                   name="quantity{{ $basicitem['id'] }}"
                                                                   value="@if(request()->has('quantity'.$basicitem['id'])){{request()->input('quantity'.$basicitem['id'])}}@else{{0}}@endif">
                                                            <a onclick="EcomQuantityIncrease(this , '{{$basicitem['id']}}')">Add to box</a>
                                                            <input type="hidden" name="product_id[]" value="{{$basicitem['id']}}" class='users'>
                                                        </div>

                                                    @endforeach


                                                </div>
                                                <div class="container" data-id="3">

                                                    @foreach($ball['data'] as $basicitem)

                                                        <div class="choose-a-balloon">

                                                            <picture><img src="{{ $basicitem['image']}}"></picture>
                                                            <span class="small-title">{{$basicitem['label']}}</span>

                                                            <span class="price">{{$basicitem['unit_price_formatted']}}</span>
                                                            <input data="{{$basicitem['id']}}" type="text" max=""
                                                                   name="quantity{{ $basicitem['id'] }}"
                                                                   value="@if(request()->has('quantity'.$basicitem['id'])){{request()->input('quantity'.$basicitem['id'])}}@else{{0}}@endif">
                                                            <a onclick="EcomQuantityIncrease(this , '{{$basicitem['id']}}')">Add to box</a>
                                                            <input type="hidden" name="product_id[]" value="{{$basicitem['id']}}" class='users'>
                                                        </div>

                                                    @endforeach


                                                </div>

                    </div>

                </main>
            </div>


        </section>
    </form>
    <sep></sep>
    <sep></sep>
    <sep></sep>
    <sep></sep>
    <sep></sep>



    <script language="javascript">

        var no_popup = {{ request()->input('no_popup','false') }};
        var current_id = false;
        {{--        @if($current) current_id = "{{ $current['id'] }}";  @endif--}}
        var formSubmit = false;

        function FooterFunctions() {

            // initBYBX()
            updateSidebbar();
        }
    </script>

@endsection
