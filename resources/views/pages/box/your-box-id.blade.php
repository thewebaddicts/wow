@extends('layouts.main')


@section('content')


    <?php


    use twa\ecomproducts\models\EcomProductModel;
    $cartList = ecom('cart')->getAsObject();

    try {
        $items = $cartList->items->items;
    } catch (\Throwable $th) {
        $items = [];
    }
    $available_quantities = \App\Http\Controllers\CartController::getAvailableQuantities($items);

    $store_id = ecom('stores')->getCurrent()->id;

    $category = ucwords(str_replace('-', ' ', $category));
    $category_label = $category;

    $category = getter('ecom_products')->condition('family_group_id', "LIKE", $category_label)->get()->first();

    if (!$category) {

        $category = getter('ecom_products')->condition('label_' . app()->getLocale(), "LIKE", $category_label)->get()->first();
        if (!$category) {

            echo '<script language="javascript">window.location="' . ecom("url")->prefix(true) . '/box/create/classic%20box/";</script>';
            exit();
        }
    }

    // $boxes = ecom('products')->condition('store', $store_id)->condition('ignoreListing', 1)->get()->toArray();
    $store = ecom('stores')->getCurrent();

    if (isset($id)) {
        $current = ecom('products')->condition('store', $store_id)->condition('display', 'full')->condition('subcategories', $category->id)->condition('productID', $id)->condition('ignoreListing', 1)->get()->toArray();
    } else {
        echo '<script language="javascript">window.location="' . url()->current() . '/' . $boxes['data'][0]['id'] . '/' . Str::slug($boxes['data'][0]['label']) . '"</script>';
        exit();
    }
    if ($current) {
        try {
            $current = $current["data"][0];
        } catch (\Throwable $e) {
            $current = false;
        }
    }
    $filters = [];







    $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $uri_segments = explode('/', $uri_path);
    $series = str_replace('-', ' ', $uri_segments);
    //  $box = ecom('products')->condition('id', request()->segment(6))->condition('cancelled', 0)->condition('display', "card")->get()->toArray();

    $cartList = ecom('cart')->getAsObject();
    $cart = $cartList->items->items;



    $product = ecom('products')->condition('display', "card")->condition('numberOfProducts', 80)->condition('cancelled', 0)->condition('keywords', getter('create_your_box_keywords')->condition('location', 'basic items')->get()->pluck('ecom_keywords_id'))->includeFilters()->addFilters(request()->input('filter', []))->get()->toArray();

    $cust = ecom('products')->condition('display', "full")->condition('numberOfProducts', 80)->condition('cancelled', 0)->condition('keywords', getter('create_your_box_keywords')->condition('location', 'customisable items')->get()->pluck('ecom_keywords_id'))->includeFilters()->addFilters(request()->input('filter', []))->get()->toArray();


    //   $ball = ecom('products')->condition('display', "card")->condition('cancelled', 0)->condition('keywords', "14")->condition('includeVariationDetails', 1)->get()->toArray();

    $boxes = ecom('products')->condition('cancelled', 0)->condition('slug', request()->segment(5))->condition('forceVariationSorting', 1)->condition('display', 'full')->condition('display', 1)->condition('includeVariationDetails', 1)->get()->toArray();
    //    $l = getter('ecom_products')->condition('slug', request()->segment(5))->condition('ecom_products_variation_primary_id', (int)request()->segment(7))->get();

    $l = EcomProductModel::where('slug', request()->segment(5))->where('ecom_products_variation_primary_id', (int)request()->segment(7))->get();
    $keywords_special = collect(getter('create_your_box_keywords')->get());

    ?>



    <form id="BYBX_form" method="post" action="{{route('postbox')}}">

        @csrf
        <section class="create-your-box-id">
            <div class="content">
                <div class="path">
                    <span>Home</span><img src="/assets/svgs/arrow-right.svg"><span>{{$series[3]}}<img
                                src="/assets/svgs/arrow-right.svg">{{$series[4]}}</span>
                </div>


                <h1 class="path-title">
                    {{$series[4]}}
                </h1>

                {{--            --}}
                <div class="path" style="color: black">Step 2 — Fill Your Box</div>
                <sep></sep>
                <sep></sep>
                <sep></sep>
                <main>

                    <div class="inside-box-content">

                        <div style="display:contents;" class="card-row card-row-{{$l[0]->id}}"
                             data-unit-price="{{$l[0]->price_ttc}}"></div>

                        <input type="hidden" name="product_ids" value="{{$l[0]->id}}" class='users'>
                        <input type="hidden" name="quantity" value="1" class='users'>
                        <picture><img src="{{$boxes['data'][0]['thumb']}}">
                        </picture>
                        <div>

                            <span class="small-title">{{$boxes['data'][0]['label_en']}}</span>
                            <span class="price">{{$l[0]->price_ttc}}&nbsp;AED</span>
                            @if($boxes['data'][0]['stock_quantity']==0)
                                <small>Out of stock</small>

                            @endif
                        </div>
                        {{--                    --}}
                        <div class="variation">
                            @foreach($boxes['data'] as $bo)

                                @isset($bo['variation'])

                                    @foreach($bo['variation'] as $var)
                                        @foreach($var as $variation)


                                            <a href="{{ ecom("url")->prefix() }}/boxes/your-box/{{Str::slug($bo['slug'])}}/{{ Str::slug($variation['label']) }}/{{$variation['id']}}"
                                               @if(request()->segment(7)==$variation['id']) class="detect" @endif>{{$variation['label']}}</a>


                                        @endforeach
                                    @endforeach
                                @endisset
                            @endforeach
                        </div>
                        <sep></sep>
                        <sep></sep>


                        <main class="variation-card">

                            @foreach($boxes['data'] as $bo)
                                @if(isset($bo['variation']))
                                    @foreach($bo['variation'] as $var)

                                        @foreach($var as $variation)
                                            @if($variation['id']==request()->segment(7))

                                                <div class="small-title limit" id="{{$variation['code']}}">Up
                                                    to {{$variation['code']}} items from the store
                                                </div>
                                            @else

                                            @endif
                                        @endforeach
                                    @endforeach
                                @endif
                            @endforeach

                            @foreach($product['data'] as $basicitem)

                                @php
                                    $qty = request()->has('quantity'.$basicitem['id']) ? request()->input('quantity'.$basicitem['id']) : 0
                                @endphp

                                <div class="card-row card-row-{{$basicitem['id']}}"
                                     data-unit-price="{{$basicitem['price_ttc']}}">

                                    <picture><img src="{{ $basicitem['thumb']}}"></picture>

                                    <div class="lol">


                                        <span>{{$basicitem['label']}}</span>
                                        <div style="display: block;font-style: italic;">
                                            <span>( </span> <span
                                                    class="demo card-row-{{$basicitem['id']}}"> {{$qty}} </span>
                                            <span> piece) </span>
                                        </div>

                                        <div class="price">{{$basicitem['unit_price_formatted']}}</div>

                                    </div>
                                    <div class="remove" onclick="removeFromBox('{{$basicitem['id']}}')"><img
                                                src="/assets/svgs/Icon material-delete.svg"><span>REMOVE</span>
                                    </div>
                                    @isset($basicitem['customization'])

                                        @foreach($basicitem['customization'] AS $key => $customization)

                                            <input type="checkbox" name="customization[]"
                                                   value="{{$customization['id']}}" class="input-customize">
                                            <i class="fa-sharp fa-solid fa-pen-to-square customzie-item"></i>
                                            <textarea placeholder="Customized message ..." class="customzie-text"
                                                      name="customization_message_{{$customization['id']}}">{{request()->input('gift_message')}}</textarea>

                                        @endforeach
                                    @endisset
                                </div>

                            @endforeach
                            @foreach($cust['data'] as $basicitem)

                                @foreach($basicitem['customization'] as $modifier)

                                    <input type="hidden" name="modifierid" value="{{$modifier['id']}}">
                                @endforeach
                                @php
                                    $qty = request()->has('quantity'.$basicitem['id']) ? request()->input('quantity'.$basicitem['id']) : 0;

                                @endphp

                                <div class="card-row card-row-{{$basicitem['id']}}"

                                     data-unit-price="{{$basicitem['price_ttc']}}">

                                    <picture><img src="{{$basicitem['thumb']}}"></picture>

                                    <div class="lol">


                                        <span>{{$basicitem['label']}}</span>
                                        <div style="display: block;font-style: italic;">
                                            <span>( </span> <span
                                                    class="demo card-row-{{$basicitem['id']}}"> {{$qty}} </span>
                                            <span> piece) </span>
                                        </div>

                                        <div class="price">{{$basicitem['unit_price_formatted']}}</div>
                                    </div>
                                    <div class="remove" onclick="removeFromBox('{{$basicitem['id']}}')"><img
                                                src="/assets/svgs/Icon material-delete.svg"><span>REMOVE</span>
                                    </div>

                                    <input type="checkbox" name="customization[]"
                                           value="{{$basicitem['id']}}" class="input-customize">


                                    <i class="fa-sharp fa-solid fa-pen-to-square customzie-item"></i>


                                    <textarea placeholder="Customized message ..." class="customzie-text"
                                              name="customization_message_{{$basicitem['id']}}"></textarea>


                                </div>

                            @endforeach

                            {{--                            @isset($cust['customization'])--}}

                            {{--                                @foreach($cust['customization'] AS $customization)--}}



                            {{--                                        <div id="customize" class="sublist">--}}
                            {{--                                                                    <span class="checkbox-cust">--}}
                            {{--                                                                        <i class="fa-solid fa-check"></i>--}}
                            {{--                                                                    </span>--}}
                            {{--                                        <input type="checkbox" name="customization[]"--}}
                            {{--                                               value="{{$customization['id']}}">--}}
                            {{--                                        <label>{{$customization['label'] }}</label>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div>--}}

                            {{--                                        <label class="uk-form-label text uk-padding-small uk-padding-remove-bottom">{{ $customization['note_available_title'] }}</label>--}}
                            {{--                                        <textarea--}}
                            {{--                                                placeholder="Customization / personalization requests (Optional)"--}}
                            {{--                                                name="customization_message_{{$customization['id']}}">{{ request()->input('gift_message') }}</textarea>--}}
                            {{--                                    </div>--}}
                            {{--                                @endforeach--}}
                            {{--                            @endisset--}}
                            <div id="subtotal" class="subtotal">
                                <div class="small-title">SUBTOTAL</div>

                                <span data-currency-symbol="AED" data-currency-rate="1" class="value">0 AED</span>

                            </div>
                        </main>
                        <div class="add-to-cart">
                            {{--                            <a onclick="document.getElementById('BYBX_form').submit();" class="nextclass">NEXT</a>--}}
                            {{--                            <a class="nextclass">NEXT</a>--}}
                            <main class="addtocartinvisible active">
                                <a class="add-wrap-link" data-id="1"
                                   href="{{ ecom('url')->prefix() }}/boxes/your-box/wrapping-pappers"
                                   onclick="window.location=$(this).attr('href')+'?'+$('#BYBX_form').serialize(); return false;">CHOOSE
                                    A WRAPPING PAPER</a>

                                {{--                                <a href="{{route('choose-balloon',[Request()->parameter])}}">CHOOSE A WRAPPING--}}
                                {{--                                    PAPER</a>--}}
                                <sep></sep>
                                {{--                                <a data-id="2" href="{{ ecom('url')->prefix() }}/boxes/your-box/wrap"--}}
                                {{--                                   onclick="window.location=$(this).attr('href')+'?'+$('#BYBX_form').serialize(); return false;">CHOOSE--}}
                                {{--                                    A CARD</a>--}}
                                {{--                                <sep></sep>--}}
                                {{--                                <a data-id="3" href="{{ ecom('url')->prefix() }}/boxes/your-box/wrap"--}}
                                {{--                                   onclick="window.location=$(this).attr('href')+'?'+$('#BYBX_form').serialize(); return false;">CHOOSE--}}
                                {{--                                    A BALLOON</a>--}}
                                {{--                                <sep></sep>--}}
                                <span class="add-wrap-link">(Optional)</span>
                                <sep></sep>
                                <button id="postBox">
                                    Add to Cart
                                </button>
                            </main>
                        </div>

                    </div>

                    <div class="items-inside-box-content @if($boxes['data'][0]['stock_quantity']==0) disabled @endif">


                        <div class="parent-select">
                            <select id="mySelect" onchange="box(this)">
                                <option value="15">Basic Items</option>
                                <option value="16">Customizable Items</option>
                            </select>

                        </div>

                        <sep></sep>
                        <sep></sep>


                        <div class="container active" data-id="15">
                            @foreach($product['data'] as $basicitem)

                                <div class="lol">

                                    <picture><img src="{{ $basicitem['thumb']}}"></picture>
                                    <span class="small-title">{{$basicitem['label']}}</span>

                                    <span class="price">{{$basicitem['unit_price_formatted']}}</span>
                                    <input data="{{$basicitem['id']}}" type="text" max=""
                                           name="cms_attributes_quantity{{$basicitem['id']}}"
                                           value="@if(request()->has('cms_attributes_quantity'.$basicitem['id'])){{request()->input('cms_attributes_quantity'.$basicitem['id'])}}@else{{0}}@endif">
                                    @if($boxes['data'][0]['stock_quantity']!==0)
                                        @if($basicitem['stock_quantity']==0)

                                            <a class="wrapclick outofstock" href="javascript:;">Out of Stock</a>
                                        @else
                                            <a class="wrapclick"
                                               onclick="EcomQuantityIncrease(this , '{{$basicitem['id']}}')">Choose</a>
                                        @endif
                                        <input type="hidden" name="cms_attributes_id[]" value="{{$basicitem['id']}}"
                                               class='users'>
                                    @endif
                                </div>

                            @endforeach


                        </div>
                        <div class="container" data-id="16">

                            @foreach($cust['data'] as $basicitem)

                                <div class="lol">

                                    <picture><img src="{{ $basicitem['thumb']}}"></picture>
                                    <span class="small-title">{{$basicitem['label']}}</span>

                                    <span class="price">{{$basicitem['unit_price_formatted']}}</span>
                                    <input data="{{$basicitem['id']}}" type="text" max=""
                                           name="cms_attributes_quantity{{$basicitem['id']}}"
                                           value="@if(request()->has('cms_attributes_quantity'.$basicitem['id'])){{request()->input('cms_attributes_quantity'.$basicitem['id'])}}@else{{0}}@endif">
                                    @if($basicitem['stock_quantity']==0)

                                        <a class="wrapclick outofstock" href="javascript:;">Out of Stock</a>
                                    @else
                                        <a class="wrapclick"
                                           onclick="EcomQuantityIncrease(this , '{{$basicitem['id']}}')">Choose</a>
                                    @endif
                                    <input type="hidden" name="cms_attributes_id[]" value="{{$basicitem['id']}}"
                                           class='users'>

                                </div>

                            @endforeach

                        </div>

                </main>
            </div>


        </section>
    </form>

    {{--    <div id="formo">--}}
    {{--<span>--}}
    {{--    <label for="">one </label>--}}
    {{--    <input name="filter[]" type="checkbox" value="35">--}}
    {{--</span>--}}
    {{--        --}}{{--        <span>--}}
    {{--        --}}{{--    <label for="">two</label>--}}
    {{--        --}}{{--    <input name="filter[]" type="checkbox">--}}
    {{--        --}}{{--</span>--}}
    {{--        <span onclick="bybxFilter(this)">submit</span>--}}
    {{--    </div>--}}

    {{--    @component('components.pagination' , ['products'=>$product ?? '']) @endcomponent--}}

@endsection

@section('script')
    <script language="javascript">

        var no_popup = {{ request()->input('no_popup','false') }};
        var current_id = false;
        @if($current) current_id = "{{ $current['id'] }}";  @endif
        var formSubmit = false;

        function FooterFunctions() {
            appendElement();

            EcomQuantityIncrease();
            // initBYBX()
            updateSidebbar();
        }
    </script>

@endsection