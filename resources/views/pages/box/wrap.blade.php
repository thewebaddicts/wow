@extends('layouts.main')
@section('content')

    <?php


    use Illuminate\Support\Facades\DB;try {
        $items = $cartList->items->items;
    } catch (\Throwable $th) {
        $items = [];
    }
    $available_quantities = \App\Http\Controllers\CartController::getAvailableQuantities($items);


    $store_id = ecom('stores')->getCurrent()->id;
    $category = ecom('products')->condition('display', 'card')->condition('ignoreListing', 0)->condition('includeVariationDetails', 0)->condition('cancelled', 0)->addField('weight')->get()->toArray();

    $category = ucwords(str_replace('-', ' ', $category));
    $category_label = $category;
    //    $category = getter('ecom_products')->condition('family_group_id', "LIKE", $category_label)->condition('family_group_id', "LIKE", $category_label)->get()->first();

    if (!$category) {

        $category = getter('ecom_products')->condition('label_' . app()->getLocale(), "LIKE", $category_label)->get()->first();
        if (!$category) {

            echo '<script language="javascript">window.location="' . ecom("url")->prefix(true) . '/box/create/classic%20box/";</script>';
            exit();
        }
    }

    $store = ecom('stores')->getCurrent();


    $filters = [];






    $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $uri_segments = explode('/', $uri_path);
    $series = str_replace('-', ' ', $uri_segments);

    $box = ecom('products')->condition('id', request('product_ids'))->condition('cancelled', 0)->condition('includeVariationDetails', 1)->condition('display', "card")->get()->toArray();

    $cartList = ecom('cart')->getAsObject();
    $cart = $cartList->items->items;

    foreach (request()->input('cms_attributes_id') as $key => $test) {
        if (request()->input('cms_attributes_quantity' . $test) !== '0') {
            $products[] = json_decode(json_encode(request()->all('cms_attributes_quantity' . $test)));
        }
    }
    foreach ($products as $j) {

        $string = key($j);
        $outputString[] = preg_replace('/[^0-9]/', '', $string);

    }

    foreach ($outputString as $output) {


        $productlisting[] = ecom('products')->condition('display', "card")->condition('id', (int)$output)->condition('cancelled', 0)->get()->toArray();

    }


    $ball = ecom('products')->condition('display', "card")->condition('numberOfProducts', 100)->condition('cancelled', 0)->condition('keywords', getter('create_your_box_keywords')->condition('location', 'balloons')->get()->pluck('ecom_keywords_id'))->get()->toArray();

    $wrap = ecom('products')->condition('display', "card")->condition('numberOfProducts', 100)->condition('cancelled', 0)->condition('keywords', getter('create_your_box_keywords')->condition('location', 'wrapping papers')->get()->pluck('ecom_keywords_id'))->get()->toArray();

    ?>



    <form id="BYBX_form" method="post" action="{{route('postbox')}}">

        {{--    <form id="BYBX_form" method="get" action="{{route('choose-balloon')}}" name="formdata">--}}
        @csrf
        <section class="create-your-box-id">
            <div class="content">
                <div class="path">
                    <span>Home</span><img src="/assets/svgs/arrow-right.svg"><span
                    >{{$series[3]}}<img src="/assets/svgs/arrow-right.svg">{{$series[4]}}</span>
                </div>


                <h1 class="path-title">
                    {{$series[4]}}
                </h1>

                {{--            --}}
                <div class="path" style="color: black">Step 3 — Choose your wrap</div>
                <sep></sep>
                <sep></sep>
                <sep></sep>
                <main>

                    <div class="inside-box-content">

                        <div style="display:contents;" class="card-row card-row-{{$box['data'][0]['id']}}"
                             data-unit-price="{{$box['data'][0]['price_ttc']}}"></div>
                        <input type="hidden" name="product_ids" value="{{request('product_ids')}}" class='users'>
                        <input type="hidden" name="quantity" value="1" class='users'>
                        <picture><img src="{{$box['data'][0]['thumb']}}">
                        </picture>
                        <div>
                            <span class="small-title">{{$box['data'][0]['label']}}</span>
                            <span class="price">{{$box['data'][0]['unit_price_formatted']}}</span>
                        </div>


                        <sep></sep>
                        <sep></sep>


                        <sep></sep>
                        <sep></sep>
                        <sep></sep>
                        <main class="variation-card">

                            @foreach($productlisting as $product)
                                @foreach($product['data'] as $basicitem)

                                    @php
                                        $qty = request('product_ids') ? intval(request('cms_attributes_quantity'.$basicitem['id'])) : 0
                                    @endphp

                                    <div class="card-row card-row-{{$basicitem['id']}}"
                                         data-unit-price="{{$basicitem['price_ttc']}}">
                                        <picture><img src="{{ $basicitem['thumb']}}"></picture>

                                        <div class="lol">
                                            <input type="hidden" name="cms_attributes_quantity{{$basicitem['id']}}"
                                                   value="{{intval(request('cms_attributes_quantity'.$basicitem['id']))}}">
                                            <input type="hidden" name="cms_attributes_id[]"
                                                   value="{{$basicitem['id']}}">


                                            <span>{{$basicitem['label']}}</span>
                                            <div style="display: block;font-style: italic">
                                                <span>( </span> <span
                                                        class="demo card-row-{{$basicitem['id']}}"> {{$qty}} </span>
                                                <span> piece) </span>
                                            </div>

                                            <div class="price">{{$basicitem['unit_price_formatted']}}</div>
                                        </div>
                                        <div class="remove" onclick="removeFromBox('{{$basicitem['id']}}')"><img
                                                    src="/assets/svgs/Icon material-delete.svg"><span>REMOVE</span>
                                        </div>
                                        @if(request('customization')>0)
                                        @foreach(request('customization') as $request)
                                            @if($request==$basicitem['id'])
                                                <input type="checkbox" name="customization"
                                                       value="{{$basicitem['id']}}" class="input-customize">






                                                <textarea placeholder="Customized message ..."
                                                          class="customzie-text active" disabled=""
                                                          name="customization_message_{{$basicitem['id']}}">
                                            {{request('customization_message_'.$basicitem['id'])}}
                                        </textarea>
                                            @endif
                                        @endforeach
                                        @endif
                                    </div>

                                @endforeach
                            @endforeach
                            @foreach($wrap['data'] as $basicitem)

                                @php
                                    $qty = request('product_ids') ? intval(request('cms_attributes_quantity'.$basicitem['id'])) : 0
                                @endphp

                                <div class="card-row card-row-{{$basicitem['id']}}"
                                     data-unit-price="{{$basicitem['price_ttc']}}">
                                    <picture><img src="{{ $basicitem['thumb']}}"></picture>

                                    <div class="lol">
                                        <input type="hidden" name="cms_attributes_quantity{{$basicitem['id']}}"
                                               value="{{intval(request('cms_attributes_quantity'.$basicitem['id']))}}">
                                        <input type="hidden" name="cms_attributes_id[]"
                                               value="{{$basicitem['id']}}">


                                        <span>{{$basicitem['label']}}</span>
                                        <div style="display: block;font-style: italic">
                                            <span>( </span> <span
                                                    class="demo card-row-{{$basicitem['id']}}"> {{$qty}} </span>
                                            <span> piece) </span>
                                        </div>

                                        <div class="price">{{$basicitem['unit_price_formatted']}}</div>
                                    </div>
                                    <div class="remove" onclick="removeFromBox('{{$basicitem['id']}}')"><img
                                                src="/assets/svgs/Icon material-delete.svg"><span>REMOVE</span>
                                    </div>
                                </div>

                            @endforeach
                            @foreach($ball['data'] as $basicitem)

                                @php
                                    $qty = request('product_ids') ? intval(request('cms_attributes_quantity'.$basicitem['id'])) : 0
                                @endphp

                                <div class="card-row card-row-{{$basicitem['id']}}"
                                     data-unit-price="{{$basicitem['price_ttc']}}">
                                    <picture><img src="{{ $basicitem['thumb']}}"></picture>

                                    <div class="lol">
                                        <input type="hidden" name="cms_attributes_quantity{{$basicitem['id']}}"
                                               value="{{intval(request('cms_attributes_quantity'.$basicitem['id']))}}">
                                        <input type="hidden" name="cms_attributes_id[]"
                                               value="{{$basicitem['id']}}">


                                        <span>{{$basicitem['label']}}</span>
                                        <div style="display: block;font-style: italic">
                                            <span>( </span> <span
                                                    class="demo card-row-{{$basicitem['id']}}"> {{$qty}} </span>
                                            <span> piece) </span>
                                        </div>

                                        <div class="price">{{$basicitem['unit_price_formatted']}}</div>
                                    </div>
                                    <div class="remove" onclick="removeFromBox('{{$basicitem['id']}}')"><img
                                                src="/assets/svgs/Icon material-delete.svg"><span>REMOVE</span>
                                    </div>
                                </div>

                            @endforeach


                            <div id="subtotal" class="subtotal">
                                <div class="small-title">SUBTOTAL</div>

                                <span data-currency-symbol="AED" data-currency-rate="1" class="value">0 AED</span>

                            </div>
                        </main>
                        <div class="add-to-cart">
                            {{--                            <a onclick="document.getElementById('BYBX_form').submit();" class="nextclass">NEXT</a>--}}
                            <main class="addtocartinvisiblewrap">
                                <a onclick="addActive(this,1)" class="active">CHOOSE A WRAPPING PAPER</a>
                                <sep></sep>
                                {{--                                <a onclick="addActive(this,2)">CHOOSE A CARD</a>--}}
                                {{--                                <sep></sep>--}}

                                <a onclick="addActive(this,2)" >CHOOSE A BALLOON</a>
                                <sep></sep>
                                <span>(Optional)</span>
                                <sep></sep>
                                <button id="postBox">
                                    Add to Cart
                                </button>
                            </main>
                        </div>
                        <sep></sep>
                        <sep></sep>
                        <sep></sep>
                    </div>
                    <div class="items-inside-box-content">
                        {{--                        <a class="true" onclick="box(this,1)">BASIC ITEMS</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--}}
                        {{--                        <a onclick="box(this,2)">CUSTOMIZABLE ITEMS</a>--}}
                        <sep></sep>
                        <sep></sep>
                        <div class="container active" data-id="1">

                            @foreach($wrap['data'] as $basicitem)


                                <div class="lol">


                                    <picture><img src="{{ $basicitem['thumb']}}"></picture>
                                    <span class="small-title">{{$basicitem['label']}}</span>

                                    <span class="price">{{$basicitem['unit_price_formatted']}}</span>
                                    <input data="{{$basicitem['id']}}" type="text" max=""
                                           name="cms_attributes_quantity{{$basicitem['id']}}"
                                           value="@if(request()->has('cms_attributes_quantity'.$basicitem['id'])){{request('cms_attributes_quantity'.$basicitem['id'])}}@else{{0}}@endif">

                                    @if($basicitem['stock_quantity']==0)

                                        <a class="wrapclick outofstock" href="javascript:;">Out of Stock</a>
                                    @else
                                        <a class="wrapclick"
                                           onclick="EcomQuantityIncrease(this , '{{$basicitem['id']}}')">Choose</a>
                                    @endif
                                    <input type="hidden" name="cms_attributes_ids[]" value="{{$basicitem['id']}}"
                                           class='users'>

                                </div>

                            @endforeach


                        </div>
                        <div class="container" data-id="2">

                            @foreach($ball['data'] as $basicitem)


                                <div class="lol">


                                    <picture><img src="{{ $basicitem['thumb']}}"></picture>
                                    <span class="small-title">{{$basicitem['label']}}</span>

                                    <span class="price">{{$basicitem['unit_price_formatted']}}</span>
                                    <input data="{{$basicitem['id']}}" type="text" max=""
                                           name="cms_attributes_quantity{{$basicitem['id']}}"
                                           value="@if(request()->has('cms_attributes_quantity'.$basicitem['id'])){{request('cms_attributes_quantity'.$basicitem['id'])}}@else{{0}}@endif">
                                    @if($basicitem['stock_quantity']==0)

                                        <a class="wrapclick outofstock" href="javascript:;">Out of Stock</a>
                                    @else
                                        <a class="wrapclick"
                                           onclick="EcomQuantityIncrease(this , '{{$basicitem['id']}}')">Choose</a>
                                    @endif
                                    <input type="hidden" name="cms_attributes_ids[]" value="{{$basicitem['id']}}"
                                           class='users'>

                                </div>

                            @endforeach


                        </div>

                    </div>

                {{--                        <div class="container" data-id="1">--}}

                {{--                            @foreach($wrap['data'] as $basicitem)--}}


                {{--                                <div class="lol">--}}


                {{--                                    <picture><img src="{{ $basicitem['thumb']}}"></picture>--}}
                {{--                                    <span class="small-title">{{$basicitem['label']}}</span>--}}

                {{--                                    <span class="price">{{$basicitem['unit_price_formatted']}}</span>--}}
                {{--                                    <input data="{{$basicitem['id']}}" type="text" max=""--}}
                {{--                                           name="cms_attributes_quantity{{$basicitem['id']}}"--}}
                {{--                                           value="@if(request()->has('cms_attributes_quantity'.$basicitem['id'])){{request('cms_attributes_quantity'.$basicitem['id'])}}@else{{0}}@endif">--}}
                {{--                                    <a class="wrapclick" onclick="EcomQuantityIncrease(this , '{{$basicitem['id']}}')">Choose</a>--}}
                {{--                                    <input type="hidden" name="cms_attributes_ids[]" value="{{$basicitem['id']}}"--}}
                {{--                                           class='users'>--}}

                {{--                                </div>--}}

                {{--                            @endforeach--}}


                {{--                        </div>--}}


            </div>

            </main>
            </div>


        </section>
    </form>
    <sep></sep>
    <sep></sep>
    <sep></sep>
    <sep></sep>
    <sep></sep>



    <script language="javascript">
        function FooterFunctions() {

            EcomQuantityIncrease();
            // initBYBX()
            updateSidebbar();
        }

        var no_popup = {{ request()->input('no_popup','false') }};
        var current_id = false;
        {{--        @if($current) current_id = "{{ $current['id'] }}";  @endif--}}
        var formSubmit = false;


    </script>

@endsection
