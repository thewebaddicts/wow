@extends('layouts.main')
@section('content')

    <?php

    $cartList = ecom('cart')->getAsObject();


    ?>

    <section class="cart-section">
        <div class="content">
            @component('partials.page-path')@endcomponent

            <div class="container-cart">
                @component('components.responsive-cart',compact('cartList'))
                @endcomponent
                    @component('components.responsive-cart-mobile',compact('cartList'))
                    @endcomponent
                <div class="cart">
                    <div class="cart-sub">
                        <div class="small-title" id="order-summary">Order Summary</div>
                        <sep></sep>
                        <sep></sep>
                        <sep></sep>
                        <sep></sep>
                        <div>
                            @isset($cartList->breakdown->Discount)
                                <span class="small-title" id="subtotal">DISCOUNT</span>
                                <span class="small-title">{{$cartList->breakdown->Discount}}</span>
                                <br>
                                <br>
                            @endisset
                            <span class="small-title" id="subtotal">SUBTOTAL</span>


                            <span class="small-title">{{$cartList->grandTotal_formated}}</span>


                        </div>
                    </div>
                    <sep></sep>
                    <sep></sep>
                    <sep></sep>
                    <sep></sep>
                    <a href="/">CONTINUE SHOPPING</a>
                    <a class="button" href="{{route('checkout')}}"
                       @if(count($cartList->items->items) == 0) style="display: none" @endif>PROCEED TO CHECKOUT</a>
                    <sep></sep>
                    @if(session('login') == null)
                        <a class="button" href="{{route('store-guest-checkout')}}"
                           @if(count($cartList->items->items) == 0) style="display: none" @endif>GUEST CHECKOUT</a>
                    @endif
                </div>
            </div>

        </div>
    </section>

@endsection

<script>
    function FooterFunctions() {
        // checkIfExist();
        // removeItemFromCart();
    }
</script>