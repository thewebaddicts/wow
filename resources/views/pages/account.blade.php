@extends('layouts.main')
@section('content')

    <?php $profile = ecom('profile')->get();?>

    <section class="account">
        <div class="content">
            <div class="path">
                <span>Home</span><img src="/assets/svgs/arrow-right.svg"><span>Account</span>
            </div>



            <h1 class="path-title">
                ACCOUNT
            </h1>

            <main>
                @component('components.account-sidebar')@endcomponent
                <div class="general-information">
                    <span class="small-title">General information</span>
                    <sep></sep>
                    <sep></sep>
                    <form style="display: contents" method="POST" action="{{route('profile-edit')}}">
                        @csrf
                    <div>
                        <label>FIRST NAME</label>

                        <input type="text" name="first_name" value="{{$profile->first_name}}">
                    </div>
                    <div>
                        <label>LAST NAME</label>
                        <input type="text" name="last_name" value="{{$profile->last_name}}">
                    </div>
                    <div>
                        <label>EMAIL ADDRESS</label>
                        <input type="email" disabled value="{{$profile->email}}">
                    </div>

                    <div>

                        <label>PHONE NUMBER</label>
                        <input name="phone" type="tel" value="{{$profile->phone}}">

                    </div>

                    <sep></sep>
                    <sep></sep>
                    <button>SAVE CHANGES</button>
                    </form>
                    <sep></sep>
                    <sep></sep>
                    <sep></sep>

                    <span style="font-size: 18px;" class="small-title">Change Password</span>
                    <sep></sep>
                    <sep></sep>
                    <form method="post" action={{route('changePassword')}}>
                        @csrf
                    <div>
                        <label >CURRENT PASSWORD</label>

                        <input type="password" name="current_password">
                    </div>

                    <div style="visibility: hidden">
                        <label></label>

                        <input type="hidden">
                    </div>
                    <div>
                        <label>NEW PASSWORD</label>

                        <input   type="password" name="password">
                    </div>
                    <div>
                        <label>CONFIRM PASSWORD</label>

                        <input type="password" name="confirm_password">
                    </div>

                    <sep></sep>
                    <sep></sep>
                    <button>SAVE CHANGES</button>
                    </form>

                </div>
            </main>


        </div>
    </section>
@endsection