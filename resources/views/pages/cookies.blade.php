@extends('layouts.main')
@section('content')
    @php

        $cookies = getter('cookies')->get();

    @endphp

    <section class="privacycookiesterms">
        <div class="content">
            @component('partials.page-path')@endcomponent
            @foreach($cookies as $cooki)
                <h3>{{$cooki->title}}</h3>
                <p>{!!nl2br($cooki->text)!!}
                </p>
            @endforeach

        </div>
    </section>
@endsection
