@extends('layouts.main')

@section('content')


    <section class="forget-password">

        <div class="content">
            <div class="path">
                <span>Home</span><img src="/assets/svgs/arrow-right.svg"><span
                        style="text-transform: capitalize">Forgot your password</span>
            </div>


            <h1>
                Account
            </h1>

            <form style="display: contents;" method="post" action={{route('forgot-password')}}>
                @csrf
                <main>

                    <div>
                        <label for="">Email</label>
                        <input type="email" placeholder="Your Email ..." name="email">
                    </div>

                    <button>Submit</button>
                </main>

            </form>
        </div>


    </section>

@endsection