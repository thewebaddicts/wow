

@extends('layouts.main')
@section('title')<title>{{  __('texts.address_create')}} | {{ __('texts.wow') }}</title>@endsection
@section('content')
    <?php $profile = ecom('profile')->get();?>
    <?php  $CountryCodes = ecom('countries')->getPhoneCodes(); ?>
    <?php  $Countries = ecom('countries')->getCountries(); ?>






    <section class="addresses-create">
        <div class="content">
            <div class="path">
                <span>Home</span><img src="/assets/svgs/arrow-right.svg"><span>Account</span>
            </div>


            <h1 class="path-title">
                ACCOUNT
            </h1>


            <main>
                @component('components.account-sidebar')@endcomponent


                <div class="general-information">
                    <span class="small-title">Add Address</span>
                    <sep></sep>
                    <sep></sep>
                    <form action="{{ecom('url')->prefix()}}/account/addresses/add" class="address-form" method="post">

                        @csrf

                        <div>
                            <label>Address Title</label>
                            <sep></sep>
                            <input type="text" name="label" required>
                        </div>
                        <div>
                            <label>FIRST Title</label>
                            <sep></sep>
                            <input type="text" name="first_name" value="{{$profile->first_name}}" required>

                        </div>

                        <div>
                            <label>LAST NAME</label>
                            <sep></sep>
                            <input type="text" name="last_name" value="{{$profile->last_name}}" required>
                        </div>

                            @if(isset($CountryCodes) && sizeof($CountryCodes))
                                <div style="position: relative">
                                    <label>PHONE NUMBER</label>
                                    <sep></sep>
                                    <select name="phone_country_code" id="">
                                        {{--                @foreach($codes AS $CountryCode)--}}
                                        {{--                    <option @if($CountryCode['code'] === 'LB') selected--}}
                                        {{--                            @endif value="{{ $CountryCode['code'] }}">--}}
                                        {{--                        +{{ $CountryCode['phone_code'] }}--}}
                                        {{--                    </option>--}}
                                        {{--                @endforeach--}}
                                        @foreach($CountryCodes AS $code)

                                            <option value="{{ $code['code'] }}"
                                                    @if( $code['phone_code']=='971') selected @endif>{{ $code['phone_code'] }}</option>

                                        @endforeach
                                    </select>
                                    <input type="tel" placeholder="Your Phone Number" name="phone"  value="{{$profile->phone_original}}" style="text-indent: 65px;" required>
                                </div>
                            @endif



                        <div>
                            <label>COUNTRY</label>
                            <sep></sep>
{{--                            <input id="" type="text" placeholder="Country" class="form-control control-75 "--}}
{{--                                   name="country" required>--}}
                            @if(isset($Countries) && count($Countries)>0)
                                <select class="text-input" name="country" id="country" required>
                                    @foreach($Countries as $country)
                                        <option value="{{$country->code}}">{{$country->name_en}}</option>

                                    @endforeach
                                </select>
                            @endif
                        </div>




                        <div>
                            <label>CITY</label>
                            <sep></sep>
                            <input class="text-input half" type="text" name="city" id="" placeholder="City / State"
                                   required>

                        </div>
                        <div>
                            <label>STREET ADDRESS</label>
                            <sep></sep>
                            <input class="text-input half" type="text" name="street" id="" placeholder="City / State"
                                   required>
                        </div>
                        <div>
                            <label>BUILDING</label>
                            <sep></sep>
                            <input class="text-input" type="text" name="building" id="" placeholder="Building" required>

                        </div>
                        <div>
                            <label>FLOOR</label>
                            <sep></sep>
                            <input class="text-input" type="text" name="floor" id="" placeholder="Floor" required>

                        </div>
                        <div>
                            <label>POSTAL CODE</label>
                            <sep></sep>
                            <input class="text-input half" type="text" name="zip_code" id="" placeholder="Zip Code"
                                   required>

                        </div>
                        <sep></sep>
                        <sep></sep>
                        <button>ADD ADDRESS</button>
                    </form>
                </div>

            </main>


        </div>
    </section>

@endsection
