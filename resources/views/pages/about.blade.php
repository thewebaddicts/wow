@extends('layouts.main')
{{--@section('title')<title>{{  __('texts.about_us')}} | {{ __('texts.wow') }}</title>@endsection--}}

@section('content')
    <section class="about-section">
        <div class="content">
      @component('partials.page-path')@endcomponent

            <div class="owl-carousel carousel single-carousel" data-carousel-dots="false"
                 data-carousel-nav="false"
                 data-carousel-autoplay="true" data-carousel-items="1" data-carousel-autowidth="false"
                 data-carousel-loop="true">

                @php $image = json_decode($about[0]['image'])@endphp

               @foreach($image as $img)

                <div style="background-image: url('{{env('DATA_URL')}}/about_image/{{$img}}');background-repeat: no-repeat;aspect-ratio: 4/1;background-size: cover;">


                </div>
                @endforeach
{{--                <div style="background-image: url('/assets/images/Component 62 – 1.jpg');background-repeat: no-repeat;aspect-ratio: 4/1;background-size: cover;">--}}


{{--                </div>--}}
            </div>
        </div>
        <div class="information">
            <div class="content-second">
                <div>
                    <h2>{{$about[0]->label_left}}</h2>
                    <p>{{$about[0]->text_left}}</p>
                </div>
                <div>
                    <h2>{{$about[0]->label_right}}</h2>
                    <p>{{$about[0]->text_right}}</p>
                </div>
            </div>
        </div>

    </section>


@endsection