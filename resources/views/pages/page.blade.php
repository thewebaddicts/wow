@extends('layouts.main')
@section('content')


{{--@php--}}


{{--isset($collections)--}}

{{--$products=$collections[0]->products;--}}


{{--@endphp--}}

    @component('partials.topbanner')@endcomponent

    <section class="categories">
        <div class="content">
{{--            @component('components.sortby')@endcomponent--}}

            <div class="parent-container">

                <div id="sidebar" class="sidebar">

                    <h4>Categories</h4>


                    <ul class="sublist">
                        <form method="GET" id="filter-products-form">

                            @if($products['filters'])
                                @foreach($products['filters']['data'] as $key => $row)
                                    @if($key !== "Brand")
                                        <li class="accordion">
                                            <a><span><img src="/assets/svgs/Icon ionic-ios-arrow-back.svg"
                                                          alt=""></span>
                                                <p>{{$key}}</p>
                                            </a>
                                            <ul class="expandable sublist filter-sublist">
                                                @foreach($row as $rows)

                                                    {{--                                                    <li><input type="checkbox" name="filter[]"--}}
                                                    {{--                                                               value="{{ $rows['id']}}"> {{$rows->label}}</li>--}}
                                                    {{--                                                    <li>--}}
                                                    <li class="filter-custom">
                                                        <input class="" name="filter[]" value="{{ $rows['id']}}"
                                                               id="{{ $rows['id']}}" type="checkbox"
                                                               value="{{ $rows['id']}}">
                                                        <label for="{{ $rows['id']}}">
                                                            <div></div>{{$rows->label}}</label>
                                                    </li>

                                                @endforeach


                                            </ul>
                                        </li>
                                    @endif

                                @endforeach
                            @endif

                        </form>

                    </ul>
                    <h4>Brands</h4>
                    <ul class="sublist">
                        <form method="GET" id="filter-brands-form">
                            @if($products['filters'])
                                @foreach($products['filters']['data'] as $key => $row)

                                    <ul class="expandable sublist brand-sublist">
                                        @foreach($row as $rows)

                                            @if($rows->group=="Brand")
                                                <li class="filter-custom"><input class="" name="filter[]" value="{{ $rows['id']}}"
                                                                                 id="{{ $rows['id']}}" type="checkbox"
                                                                                 value="{{ $rows['id']}}">


                                                    <label for="{{ $rows['id']}}">
                                                        <div></div>{{$rows->label}}</label>

                                                </li>

                                            @endif
                                        @endforeach

                                    </ul>
                                    </li>


                                @endforeach
                            @endif


                        </form>
                    </ul>
                </div>
                <main>


                    <section class="product-list">
                        <div class="content">
                            <div class="container" id="product-listing-container">
                                @include('components.product-listing')
                            </div>
                        </div>
                    </section>


                    {{-- @foreach ($productspage as $productpage)
                                <figure><a href={{$productpage->btn_link}}>{{$productpage->btn_label}}</a><img src="{{ env('DATA_URL') }}/products_page/{{ $productpage->id }}.{{  $productpage->extension_image }}?v={{ $productpage->version }}' alt=">
                                    <figcaption>
                                        <h3>{{ $productpage->product_slogan}}</h3>
                                        <h3>{{ $productpage->product_slogan}}</h3>
                                        <h4>{{ $productpage->brand_name}}</h4><span>{{ $productpage->product_price}}</span>
                                    </figcaption>
                                </figure>
                                @endforeach --}}


                </main>
            </div>

        </div>


    </section>

@endsection













