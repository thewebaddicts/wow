@extends('layouts.main')

@section('content')
    <?php
    $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $uri_segments = explode('/', $uri_path);
    $series = str_replace('-', ' ', $uri_segments);

    ?>

    <?php $profile = ecom('profile')->get();?>
    <?php  $PhoneCodes = ecom('countries')->getPhoneCodes();
    ?>
    <section class="login">
        <div class="content">
            <div class="path">
                <span>Home</span><img src="/assets/svgs/arrow-right.svg"><span>{{$series[3]}}<img src="/assets/svgs/arrow-right.svg">{{$series[4]}}</span>
            </div>
            <main>
                @component('forms.login-account')@endcomponent
          @component('forms.register-account')@endcomponent

            </main>
        </div>

    </section>
@endsection
@section('script')
    <script>
        function FooterFunctions(){
            showPassword();
        }
    </script>
@endsection