@extends('layouts.main')
@section('content')
<section class="saved-products">
    <div class="content">
        <div class="path">
            <span>Home</span><img src="/assets/svgs/arrow-right.svg"><span
                    style="">Account</span>
        </div>


        <h1 class="path-title">
            ACCOUNT
        </h1>
        <main>
            @component('components.account-sidebar')@endcomponent
            <div class="saved-products-container">
                <span class="small-title">Saved Products</span>
                <a href="{{route('addresses-create')}}" style="display:contents">

                <sep></sep>
                <sep></sep>
                <sep></sep>

                    <div class="main-container">


                        @component('components.product')@endcomponent






        </main>


    </div>
</section>
@endsection