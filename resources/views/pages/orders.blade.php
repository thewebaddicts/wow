@extends('layouts.main')
@section('content')
    <?php $purchases = ecom('purchases')->list();


    ;?>

    <section class="orders">
        <div class="content">
            <div class="path">
                <span>Home</span><img src="/assets/svgs/arrow-right.svg"><span>Account</span>
            </div>


            <h1 class="path-title">
                ACCOUNT
            </h1>
            <main>
                @component('components.account-sidebar')@endcomponent
                <div class="orders-container">
                    <span class="small-title">Orders history</span>


                    <sep></sep>


                    @foreach($purchases as $key => $purchase)

                        @php
                            $itemo = json_encode($purchase['items']);

                                $items = json_decode(json_encode($purchase['items']));

                        @endphp
                        @isset($items)
                            <div class="container">
                                <table>
                                    <tr>
                                        <th>Order number</th>
                                        <th>Date</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    <tbody>
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>


                                            {{$items->items->created_at}}

                                        </td>
                                        <td>{{$items->subtotal}}</td>


                                        <td>{{$purchase->status}}</td>
                                        <td><a href="javascript:;" class="outofstock" onclick="expandDiv(this)">view
                                                more</a></td>

                                    </tr>

                                    </tbody>
                                </table>
                                <article>
                                    @foreach($purchase->items['items']['items'] as $details)

                                        <div>
                                            <picture><img src="{{$details['details']['thumb']}}" alt=""></picture>
                                            <div class="info-parent">
                                            <h4>{{$details['details']['label']}}</h4>
                                            <span>{{$details['details']['unit_price_formatted']}}</span>
                                            </div>
                                            <div class="info-parent">

                                                <h4>Quantity</h4>
                                                <span>{{$details['quantity']}}</span>
                                            </div>
                                        </div>
                                    @endforeach

                                </article>
                            </div>
                        @endisset
                    @endforeach


                </div>


        </div>
        </main>


        </div>
    </section>
@endsection