
<a href="{{route('product',[ "id" => $product->id , "slug" => Str::slug($product->label) ])}}" class="productCard">
    <img src="{{ env('DATA_URL') }}{{ $product->image}}" loading="lazy" alt="">
    @if($product->display_badge==1)
        <span style="display: block;display: block;
    position: absolute;
    top: 0;
    left: 0;"><span style="position: absolute;
    left: 50%;
    color: white;
    top: 40%;
    font-family: 'Restora';
    transform: translate(-50%,-50%);
    font-size: 15px;">New</span>
            <img src="{{ env('BASE_URL') }}/assets/svgs/Mask Group 45.svg" loading="lazy" alt=""></span>
    @endif
    <a class="link" href="{{route('product',[ "id" => $product->id , "slug" => Str::slug($product->label)])}}">View More</a>
    <h3>{{ $product->label}}</h3>
    <h3>{{ $product->small_description}}</h3>


    @isset($product->cms_attributes['attributes_product_brand'])
    <span>{{$product->cms_attributes['attributes_product_brand'] }}</span>
    @endisset

    <span class="product-price">{{ $product->unit_price_formatted}}</span>

</a>
