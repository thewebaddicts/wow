@extends('layouts.main',[ "product" => $product, "slug" => $slug ])
{{--@section('title')<title>{{ $product['label'] }} | {{ __('texts.wow') }}</title>@endsection--}}

@section('content')
    @php

        // $balloons =ecom('products')->condition('cancelled',0)->condition('display', "full")->condition('includeVariationDetails' , 1)->condition('forceVariationSorting',1)->condition('includeRecommendations' , '1')->get();
 //
  //$balloons = ecom('products')->condition('cancelled',0)->condition('display',"full")->get();


    @endphp
    @php

        if(ecom('users')->loginCheck()){

                 $favorite_active = ecom('favorites')->check($product['id']);

             }else{
                 $favorite_active = false;
             }

    @endphp

    <section class="balloons-single-product">
        <div class="content">
            @component('partials.page-path')@endcomponent
            <sep></sep>
            <sep></sep>
            <main>
                @component('components.gallery',compact('product'))@endcomponent

                <div>
                    @if($product['display_badge']==1)
                        <span>&#8226; NEW ITEM &#8226;</span>
                    @endif
                    <h1>{{$product['label']}}


                        <span data-pid="{{$product['id']}}"
                              class="favorite-toggle @if($favorite_active) favorite-toggle-active @endif">
                     <span class="hart favorite-toggle">
                        <i onclick="wishList(this)"
                           class="fa-heart fa-regular @if($favorite_active) fa-solid @endif"></i>

{{--                        <i class="fas fa-heart" id="myImg"></i>--}}
                    </span>
                        </span>


                    </h1>
                    <p>{{$product['small_description']}}</p>
                    <br>
                    @if($product['stock_quantity']==0)
                        <a href="javascript:;" class="outofstock">Out of stock</a>
                    @endif
                        @if($product['stock_quantity']!==0)
                    <sep></sep>
                    <sep></sep>
                    <span class="price">{{$product['unit_price_formatted']}}</span>
                    <sep></sep>
                    <sep></sep>
                    <sep></sep>
                        @endif
                    @if($product['stock_quantity']!==0)
                        @if(count($product['variation']['primary_variations']) > 0)
                            {{--                        <span class="variation-style">BALLOON SIZE</span>--}}
                            <sep></sep>
                            <sep></sep>

                            @foreach($product['variation']['primary_variations'] as $variation)

                                <a href="{{$variation['link']}}"
                                   class="customize-links    @if($variation['selected']==1) selected     @endif">{{$variation['label']}}</a>

                            @endforeach
                        @endif
                    @endif
                    <sep></sep>
                    <sep></sep>
                    <sep></sep>
                    <sep></sep>
                    <sep></sep>
                    @if($product['stock_quantity']!==0)
                        <form id="mainform" style="display: contents" method="post">

                            @isset($product['customization'])
                                @foreach($product['customization'] AS $customization)

                                    <span class="small-title">Customize your item</span>

                                    <div id="customize" class="sublist">
                                    <span class="checkbox-cust">
                                        <i class="fa-solid fa-check"></i>
                                    </span>
                                        <input type="checkbox" name="customization[]"
                                               value="{{$customization['id']}}">
                                        <label>{{$customization['label'] }}</label>
                                    </div>


                                    <div class="note">
                                        <form method="post" id="mainform"
                                              onsubmit="event.preventDefault() ; addToCart($('#mainform').serialize()); return false;">
                                            <label class="uk-form-label text uk-padding-small uk-padding-remove-bottom">{{ $customization['note_available_title'] }}</label>
                                            <textarea placeholder="Customization / personalization requests (Optional)"
                                                      name="customization_message_{{$customization['id']}}">{{ request()->input('gift_message') }}</textarea>
                                    </div>
                                    <sep></sep>
                                    <sep></sep>

                                @endforeach
                            @endisset


                            <div class="balloons-addtocart">


                                <div><span onmousedown="mouseDown_d()" onmouseup="mouseUp()"
                                           onmouseleave="mouseLeave()"><i class="fa fa-minus"></i></span>

                                    <input type="hidden" name="product_id" value={{$product['id']}}>
                                    <span class="balloons-addtocart-number"> <input id="xxxx" name="quantity"
                                                                                    readonly
                                                                                    type="text" min="1"
                                                                                    max="{{$product['stock_quantity']}}"
                                                                                    value="1"></span>

                                    <span class="addornotadd" onmousedown="mouseDown_i()" onmouseup="mouseUp()"
                                          onmouseleave="mouseLeave()"><i class="fa fa-plus"></i></span>
                                </div>


                                <button
                                        onclick="addToCart($('#mainform').serialize()); return false;">Add to Cart
                                </button>


                            </div>

                        </form>
                    @endif
                    <sep></sep>
                    <sep></sep>
                    <sep></sep>
                    @if($product['tab_1_display']>0)
                        <div class="accordion">

                            <a><span class="small-title">{{$product['tab_1_title']}}</span><span><img
                                            src="/assets/svgs/Icon material-arrow-drop-down.svg"
                                            alt=""></span></a>
                            <div class="expandable sublist">
                                <p>
                                    {!!nl2br($product['tab_1_content'])!!}
                                </p>
                            </div>

                        </div>
                    @endif
                    <sep></sep>
                    @if($product['tab_2_display']>0)

                        <div class="accordion">

                            <a><span class="small-title">{{$product['tab_2_title']}}</span><span><img
                                            src="/assets/svgs/Icon material-arrow-drop-down.svg"
                                            alt=""></span></a>
                            <sep></sep>
                            @php $jsoncontent = json_decode($product['tab_2_content']) @endphp
                            @foreach($jsoncontent as $arr)

                                <div class="expandable sublist">

                                    <p style="font-weight: 600;
    color: black;margin: unset">
                                        {{($arr->label)}}
                                    </p>
                                    <p style="margin: unset">
                                        {{($arr->value)}}
                                    </p>

                                </div>
                            @endforeach

                        </div>
                    @endif
                    <sep></sep>
                    @if($product['tab_3_display']>0)
                        <div class="accordion">

                            <a><span class="small-title">{{$product['tab_3_title']}}</span><span><img
                                            src="/assets/svgs/Icon material-arrow-drop-down.svg"
                                            alt=""></span></a>
                            <div class="expandable sublist">
                                <p>
                                    {!!nl2br($product['tab_3_content'])!!}
                                </p>
                            </div>

                        </div>
                    @endif
                    <sep></sep>
                    @if($product['tab_4_display']>0)
                        <div class="accordion">

                            <a><span class="small-title">{{$product['tab_4_title']}}</span><span><img
                                            src="/assets/svgs/Icon material-arrow-drop-down.svg"
                                            alt=""></span></a>
                            <div class="expandable sublist">
                                <p>
                                    {!!nl2br($product['tab_4_content'])!!}
                                </p>
                            </div>

                        </div>
                    @endif
                </div>


            </main>
            <sep></sep>
            <sep></sep>
            <sep></sep>
            <sep></sep>

            @isset($product['recommendations'])

                @if($product['recommendations']['recommended'])
                    <div class="products">

                        <div>

                            <h1>Recommended items</h1>
                            <div class="best-sellers-container-carousel owl-carousel carousel single-carousel"
                                 data-carousel-nav="false" data-carousel-autoplay="true" data-carousel-items="6"
                                 data-carousel-autowidth="true" data-carousel-loop="true">

                                @foreach($product['recommendations']['recommended'] as $product)

                                    @php $product = json_decode(json_encode($product)) @endphp


                                    @php

                                        if(ecom('users')->loginCheck()){

                                                 $favorite_active = ecom('favorites')->check($product->id);

                                             }else{
                                                 $favorite_active = false;
                                             }

                                    @endphp

                                    <div class="balloons-product" id="home_product_listing">


                                        <picture>

                                            <a href="{{ecom('url')->prefix()}}/product/{{$product->slug}}"
                                               style="width: 100%;height:100%;position: absolute;"></a>


                                            <img loading="lazy" src="{{ $product->thumb}}">
                                            <div class="primary-button">
                                                @if($product->stock_quantity!==0)
                                                    <a class="click-btn btn-style904" data-hover="" href="javascript;"
                                                       onclick="addToCart('product_id={{ $product->id }}',event); return false;">
                                                        <span class="hover-me">Add to bag</span>
                                                    </a>
                                                @endif

                                            </div>
                                        </picture>

                                        {{--                                <button type="button" onclick="addToCart('product_id={{$product->id }}',event); return false;" class="primary-button">Add to Bag</button>--}}

                                        <div class="product-container-details">

                                            <h2>
                                                {{$product->label}}
                                            </h2>
                                            {{--                                    <h3>--}}

                                            {{--                                        {{$product->small_description}}--}}
                                            {{--                                    </h3>--}}
                                            <span>
                               {{$product-> unit_price_formatted}}


                            </span>
                                            <br>
                                            @if($product->stock_quantity==0)
                                                <a href="javascript:;" class="outofstock">Out of stock</a>
                                            @endif

                                            @if($product->stock_quantity!==0)
                                                <div data-pid="{{$product->id}}"
                                                     class="add-towishlist-container favorite-toggle @if($favorite_active) favorite-toggle-active @endif">
                     <span class="hart favorite-toggle" id="myBtn">
                        <i onclick="wishList(this)"
                           class="fa-heart fa-regular @if($favorite_active) fa-solid @endif"></i>
{{--                        <i class="fas fa-heart" id="myImg"></i>--}}


                    </span>
                                                </div>
                                            @endif
                                        </div>


                                    </div>

                                @endforeach

                            </div>
                        </div>
                    </div>
                @endif
            @endisset

        </div>
    </section>
@endsection
<script>
    function zoom(e) {
        var zoomer = e.currentTarget;
        e.offsetX ? offsetX = e.offsetX : offsetX = e.touches[0].pageX
        e.offsetY ? offsetY = e.offsetY : offsetX = e.touches[0].pageX
        x = offsetX / zoomer.offsetWidth * 100
        y = offsetY / zoomer.offsetHeight * 100
        zoomer.style.backgroundPosition = x + '% ' + y + '%';
    }

    function FooterFunctions() {
        try {
            modifiers();
        } catch (e) {

        }
        try {
            filterCustom();
        } catch (e) {

        }

    }

</script>