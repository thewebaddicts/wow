@extends('layouts.main')
@section('content')
    @php $request = request()->input();
$pagination = $request['term'];


    @endphp

    <section class="balloons-products-listing-page" id="product-listing-container">
        <div class="content">
            <div class="path">
                <a href="/"> <span>Home</span></a><img src="/assets/svgs/arrow-right.svg"><span>{{request()->segment(3)}}</span>
            </div>



            <h1>
                Your search for {{request()->input('term')}} ...
            </h1>

            <div class="main-container">


                @if($balloons->total)



                        @foreach(   $balloons['data'] as $balloon)
@php
    $balloon_single = json_decode(json_encode($balloon))

        @endphp
                        @php

                            if(ecom('users')->loginCheck()){

                                     $favorite_active = ecom('favorites')->check($balloon_single->id);

                                 }else{
                                     $favorite_active = false;
                                 }

                        @endphp
                        <div class="balloons-product">


                            <picture>
                                <a href="product/{{$balloon_single->slug}}"
                                   style="width: 100%;height:100%;position: absolute;"></a>
                                <img loading="lazy" src="{{$balloon_single->thumb}}">
                                <div class="primary-button">
                                    <a class="click-btn btn-style904" data-hover="" href="javascript;"
                                       onclick="addToCart('product_id={{$balloon_single->id }}',event); return false;">
                                        <span class="hover-me">Add to bag</span>
                                    </a>
                                </div>
                            </picture>

                            {{--                                <button type="button" onclick="addToCart('product_id={{$product->id }}',event); return false;" class="primary-button">Add to Bag</button>--}}

                            <div class="product-container-details">

                                <h2>
                                    {{$balloon_single->label}}
                                </h2>
                                {{--                                    <h3>--}}

                                {{--                                        {{$product->small_description}}--}}
                                {{--                                    </h3>--}}
                                <span>
                               {{$balloon_single-> unit_price_formatted}}


                            </span>

                                <div data-pid="{{$balloon_single->id}}" class="add-towishlist-container favorite-toggle @if($favorite_active) favorite-toggle-active @endif">
                     <span class="hart favorite-toggle" id="myBtn">
                        <i onclick="wishList(this)"
                           class="fa-heart fa-regular @if($favorite_active) fa-solid @endif"></i>
{{--                        <i class="fas fa-heart" id="myImg"></i>--}}


                    </span>
                                </div>
                            </div>


                        </div>

                        @endforeach

                @else
                    <div class="error">

                        <span>The Product has not found</span>


                    </div>
                @endif
            </div>
        </div>

        <span class="pagination">
        <ul class="pagination-2">

            @foreach($balloons['links'] as $link)

                <li class="page-number {{$link['active'] === true ? 'active' : 'not'}}"><a
                            href="{{$link['url']}}&term={{$pagination}}">{!!$link['label'] !!}</a></li>
            @endforeach

        </ul>

</span>



    </section>
@endsection