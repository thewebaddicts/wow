@extends('layouts.main')
@section('content')
    <?php
    $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $uri_segments = explode('/', $uri_path);
    $series = str_replace('-', ' ', $uri_segments);
 $PhoneCodes = ecom('countries')->getPhoneCodes();
    ?>
    <section class="getaquote">
        <form method="post" class="contact-form" action="{{route('contact-us')}}">
            @csrf
        <div class="content">
            <div class="path">
              <a href="/"> <span>Home</span></a> <img src="/assets/svgs/arrow-right.svg"><span>{{$series[3]}}<img src="/assets/svgs/arrow-right.svg">{{$series[4]}}</span>
            </div>
            <h1 class="path-title">
                {{$series[4]}}
            </h1>
            <span class="small-title">Fill the form to have your own wow event</span>
            <sep></sep>
            <sep></sep>
            <sep></sep>
            <div class="part-1">
                <span class="pink">01</span>&nbsp;&nbsp;&nbsp;<span class="black">Personal information</span>
                <sep></sep>
                <sep></sep>
                <sep></sep>
                <div class="content-second">

                    <div>
                        <label>FIRST NAME</label>

                        <input class="text-input half" type="text" name="name" id="" placeholder="First Name">
                    </div>

                    <div>
                        <label>LAST NAME</label>

                        <input class="text-input half" type="text" name="last_name" id="" placeholder="Last Name">

                    </div>

                    <div>
                        <label>EMAIL ADDRESS</label>

                        <input class="text-input half" type="email" name="email" id="" placeholder="Email Address">
                    </div>
                    <div style="position: relative">
                        <label>PHONE NUMBER</label>

                        <select name="phone_country_code" id="">
                            {{--                @foreach($codes AS $CountryCode)--}}
                            {{--                    <option @if($CountryCode['code'] === 'LB') selected--}}
                            {{--                            @endif value="{{ $CountryCode['code'] }}">--}}
                            {{--                        +{{ $CountryCode['phone_code'] }}--}}
                            {{--                    </option>--}}
                            {{--                @endforeach--}}
                            @foreach($PhoneCodes AS $code)

                                <option value="{{ $code['code'] }}"
                                        @if( $code['phone_code']=='971') selected @endif>{{ $code['phone_code'] }}</option>

                            @endforeach
                        </select>
                        <input type="tel" placeholder="Your Phone Number" name="phone" style="text-indent: 65px;" required>
                    </div>
                    <div>
                        <label>CITY</label>

                        <input type="name" name="city">
                    </div>
                    <div>
                        <label>STATE</label>

                        <input type="name" name="state">
                    </div>
                </div>
            </div>
            <sep></sep>
            <sep></sep>
            <sep></sep>
            <div class="part-2">

                <span class="pink">02</span>&nbsp;&nbsp;&nbsp;<span class="black">Event Type</span>
                <sep></sep>
                <sep></sep>
                <sep></sep>
                <div class="content-second event-type">

                    <picture><input style="display: contents;opacity: 0;" type="radio" name="type-event" value="corporate-event">
                        <img src="/assets/images/Mask Group 78.jpg"><span>Corporate event</span></picture>

                    <picture><input  style="display: contents;opacity: 0;" type="radio" name="type-event" value="private-event">
                        <img src="/assets/images/Mask Group 78.jpg"><span>Private event</span></picture>

                </div>
            </div>
            <sep></sep>
            <sep></sep>
            <sep></sep>
            <div class="part-3">
                <span class="pink">03</span>&nbsp;&nbsp;&nbsp;<span class="black">Date, time and place</span>
                <sep></sep>
                <sep></sep>
                <sep></sep>
                <div class="content-second">
                    <div>
                        <label>DATE</label>

                        <input type="date" name="date" required>
                    </div>

                    <div>
                        <label>TIME</label>

                        <input type="time" name="time" required>
                    </div>

                    <div style="width: 100%">
                        <label>ADDRESS</label>

                        <textarea name="address" required>



                        </textarea>
                    </div>
                </div>
            </div>
            <div class="part-4">
                <span class="pink">04</span>&nbsp;&nbsp;&nbsp;<span class="black">Details about the event</span>
                <sep></sep>
                <sep></sep>
                <sep></sep>
                <div class="content-second">
                    <div style="width: 100%;height: 200px">
                        <label>DETAILS</label>

                        <textarea name="subject"></textarea>
                    </div>

                </div>
            </div>
            <sep></sep>
            <sep></sep>
            <button>SUBMIT</button>
        </div>
        </form>
    </section>

@endsection