<div class="banner-desktop" style="background-image: url('{{ env('DATA_URL') }}/banner/{{ $ban->id }}.{{ $ban->extension_image }}?v={{ $ban->version }}')">


    <div class="shop-now-container">

        <h1>{!!$ban->label!!}</h1>
        <span>{{$ban->small_parag}}</span>

        <a href="{{$ban->btn_link}}">{{$ban->btn_label}}</a>
    </div>
</div>

@include('banner.banner_mobile')