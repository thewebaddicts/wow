<div class="login-section">
    <h1 class="path-title">login</h1>
    <form method="POST" style="display: contents" action="{{route('loginpost')}}" name="loginform" id="loginform"
          class="section-form form">
        @csrf
        <div>
            <label>EMAIL ADDRESS</label>

            <input type="email" name="email" placeholder="Your E-mail">
        </div>
        <sep></sep>
        <div>
            <label>PASSWORD</label>
            <input id="password-field" type="password" class="form-control" name="password" value="secret"
                   placeholder="Your Password">

            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>

        </div>
        <sep></sep>
        <a href="{{route('forgot-password')}}">Forgot Password?</a>
        <sep></sep>
        <sep></sep>
        <sep></sep>
        <button>LOGIN</button>
    </form>
        <sep></sep>
        <sep></sep>
        <span>Or Login With</span>
        <sep></sep>
        <sep></sep>
        <div class="social-login">

            <picture> <a href="{{route('login.facebook')}}"><img src="/assets/svgs/facebook.svg"></a></picture>
            <picture> <a href="{{route('login.google')}}"><img src="/assets/svgs/Ellipse 77.svg"></a></picture>

        </div>

</div>