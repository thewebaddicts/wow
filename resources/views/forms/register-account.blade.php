<?php
$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri_segments = explode('/', $uri_path);
$series = str_replace('-', ' ', $uri_segments);

?>

<?php $profile = ecom('profile')->get();?>
<?php  $PhoneCodes = ecom('countries')->getPhoneCodes();

?>

<div class="register-section">
    <h1 class="path-title">Register</h1>
    <form method="POST" action="{{route('register')}}" name="loginform" id="loginform" class="section-form form">
        @csrf
        <div>

            <label>FIRST NAME</label>

            <input type="text" placeholder="Your First Name" value="{{old('first_name')}}"  name="first_name" required>
        </div>
        <div>

            <label>LAST NAME</label>

            <input type="text" placeholder="Your Last Name" value="{{old('last_name')}}" name="last_name" required>
        </div>   <div>
            <label>EMAIL ADDRESS</label>

            <input type="email" name="email" placeholder="Your E-mail" required value="{{old('email')}}" >
        </div>

        <div style="position: relative">
            <label>PHONE NUMBER</label>

            <select name="phone_country_code" id="">
                                @foreach($PhoneCodes AS $code)
                                    <option @if($code['code'] === 'AE') selected
                                            @endif value="{{ $code['code'] }}">
                                        +{{ $code['phone_code'] }}
                                    </option>
                                @endforeach

{{--                @foreach($PhoneCodes AS $code)--}}

{{--                    <option value="{{ $code['code'] }}"--}}
{{--                            @if( $code['phone_code']=='971') selected @endif>{{ $code['phone_code'] }}</option>--}}

{{--                @endforeach--}}
            </select>
            <input type="tel" placeholder="Your Phone Number" name="phone" style="text-indent: 60px;" required>
        </div>

        <div>
            <label>PASSWORD</label>

            <input type="password" minlength="6" name="password" placeholder="Your Password" required id="pass1">
        </div>   <div>
            <label>CONFIRM PASSWORD</label>

            <input placeholder="Your Password" minlength="6" name="confirm_password" type="password" id="pass2" onkeyup="checkPass(); return false;">

            <div id="error-nwl"></div>
        </div>
        <sep></sep>
        <sep></sep>
        <button>REGISTER</button>
        @if ($errors -> any())
            <ul>
                @foreach ($errors->all() as $item)
                    <li>{{$item}}</li>
                @endforeach


            </ul>

        @endif
    </form>
</div>