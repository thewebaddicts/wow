<?php
$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri_segments = explode('/', $uri_path);
$series = str_replace('-', ' ', $uri_segments);

?>

<?php $profile = ecom('profile')->get();?>
<?php  $PhoneCodes = ecom('countries')->getPhoneCodes();

?>

<div class="register-section">


        <div class="guest-container-auth">
        <div>
            <label>FIRST NAME</label>

            <input type="text" placeholder="Your First Name"  name="first_name" required>
        </div>
        <div>
            <label>LAST NAME</label>

            <input type="text" placeholder="Your Last Name" name="last_name" required>
        </div>   <div>
            <label>EMAIL ADDRESS</label>
            <sep></sep>
            <input type="email" name="email" placeholder="Your E-mail" required>
        </div>

        <div style="position: relative">
            <label>PHONE NUMBER</label>
            <sep></sep>
            <select name="phone_country_code" id="">
                {{--                @foreach($codes AS $CountryCode)--}}
                {{--                    <option @if($CountryCode['code'] === 'LB') selected--}}
                {{--                            @endif value="{{ $CountryCode['code'] }}">--}}
                {{--                        +{{ $CountryCode['phone_code'] }}--}}
                {{--                    </option>--}}
                {{--                @endforeach--}}

                @foreach($PhoneCodes AS $code)
                    <option value="{{ $code['code'] }}"
                            @if( $code['phone_code']=='971') selected @endif>{{ $code['phone_code'] }}</option>


                @endforeach

            </select>
            <input type="tel" placeholder="Your Phone Number" name="phone" style="text-indent: 50px;" required>
        </div>
        </div>
        <sep></sep>
        <sep></sep>

        @if ($errors -> any())
            <ul>
                @foreach ($errors->all() as $item)
                    <li>{{$item}}</li>
                @endforeach


            </ul>

        @endif

</div>