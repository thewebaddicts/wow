<?php
$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri_segments = explode('/', $uri_path);
$series = str_replace('-', ' ', $uri_segments);
$prefix = "billing_"

?>

<?php $profile = ecom('profile')->get();?>
<?php  $PhoneCodes = ecom('countries')->getPhoneCodes(); ?>
<select class="billing-address" onchange="guest(this)" name="{{$prefix}}address_type">
    <option   value="same">Same As Shipping Address</option>
    <option id="selec" value="new">Pick A New Billing address</option>
</select>
<sep></sep>
<sep></sep>

<div class="register-section" id="billing">


    <div class="guest-container-auth">
        <div>
            <label>FIRST NAME</label>
            <sep></sep>
            <input type="text" placeholder="Your First Name" name="{{$prefix}}first_name">
        </div>
        <div>
            <label>LAST NAME</label>
            <sep></sep>
            <input type="text" placeholder="Your Last Name" name="{{$prefix}}last_name">
        </div>


        <div style="position: relative">
            <label>PHONE NUMBER</label>
            <sep></sep>
            <select name="{{$prefix}}phone_country_code" id="">
                @foreach($PhoneCodes AS $CountryCode)
                    <option @if($CountryCode['code'] === 'AE') selected
                            @endif value="{{ $CountryCode['code'] }}">
                        +{{ $CountryCode['phone_code'] }}
                    </option>
                @endforeach
                @foreach($PhoneCodes AS $code)

                    <option value="{{ $code['code'] }}"
                            @if( $code['phone_code']=='971') selected @endif>{{ $code['phone_code'] }}</option>

                @endforeach
            </select>
            <input type="tel" placeholder="Your Phone Number" name="{{$prefix}}phone"
                   style="text-indent: 50px;">
        </div>
        <div>
            <label>DESTINATION</label>
            <sep></sep>
            <select name="{{$prefix}}label" class="destination">
                <option value="work">Work</option>
                <option value="home">Home</option>
                <option value="others">Others</option>

            </select>
        </div>
        <div>
            <label>COUNTRY</label>
            <sep></sep>
            <input type="text" placeholder="Country" name="{{$prefix}}country">
        </div>
        <div>
            <label>STATE</label>
            <sep></sep>
            <input type="text" placeholder="State" name="{{$prefix}}state">
        </div>
        <div>
            <label>CITY</label>
            <sep></sep>
            <input type="text" placeholder="City" name="{{$prefix}}city">
        </div>
        <div>
            <label>STREET</label>
            <sep></sep>
            <input type="text" placeholder="Street" name="{{$prefix}}street">
        </div>
        <div>
            <label>FLOOR</label>
            <sep></sep>
            <input type="text" placeholder="Floor" name="{{$prefix}}floor">
        </div>
        <div>
            <label>BUILDING</label>
            <sep></sep>
            <input type="text" placeholder="Building" name="{{$prefix}}building">
        </div>
        <div>
            <label>ZIP CODE</label>
            <sep></sep>
            <input type="text" placeholder="Zip Code" name="{{$prefix}}zip_code">
        </div>
    </div>

    <sep></sep>
    <sep></sep>

    @if ($errors -> any())
        <ul>
            @foreach ($errors->all() as $item)
                <li>{{$item}}</li>
            @endforeach


        </ul>

    @endif


</div>
