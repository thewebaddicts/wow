// const sideElement = require("lodash");
function checkClass() {
    if ($('.slideshow .slide').hasClass('BLACK-theme')) {
        $('.slideshow').addClass('black-theme')
    } else if ($('.slideshow .slide').hasClass('WHITE-theme')) {
        $('.slideshow').addClass('white-theme')
    }
}

function removeItemFromCart(cartItemID) {

    var local = $("meta[name=csrf-token]").attr("content");
    var store_id = $("meta[name=store-id]").attr("content");
    var lang = $("meta[name=lang]").attr("content");

    $.fancyConfirm({

        title: "Are you sure?",
        message: "Are you sure you want to remove this item from cart?",
        okButton: "Agree",
        noButton: "Disagree",

        callback: function (value) {
            if (value) {
                $("#cart-info").addClass("loading-box");
                $("#cart-details").addClass("loading-box");

                $.ajax({
                    type: "POST",
                    url: "/" + store_id + "/" + lang + "/cart/remove",
                    data: {
                        cartItemID: cartItemID,
                        store: store_id,
                        lang: lang,
                        _token: local,
                    },
                    success: function (data) {
                        if (data.status || data.status == "true") {

                            location.reload();

                            // $("#cart-details").html(data.breakdown);
                            // $('.cart-count').text(data.total);
                            //
                            // $("#cart-info").removeClass("loading-box");
                            // $("#cart-details").removeClass("loading-box");

                        } else {
                        }
                    },
                });
            }
        },
    });
}

function checkPass() {
    var pass1 = document.getElementById('pass1');
    var pass2 = document.getElementById('pass2');
    var message = document.getElementById('error-nwl');
    var goodColor = "#9ED6E5";
    var badColor = "#E7306A";

    if (pass1.value.length > 5 && pass2.value.length > 5) {
        pass1.style.backgroundColor = "#9ED6E5";
        pass2.style.backgroundColor = "white";
        message.style.color = goodColor;
        message.innerHTML = "character number ok!"

        if (pass1.value == pass2.value) {
            pass2.style.backgroundColor = "#9ED6E5s";
            message.style.color = goodColor;
            message.innerHTML = "ok!"
        } else {
            pass2.style.backgroundColor = "#E7306A";
            message.style.color = badColor;
            message.innerHTML = " These passwords don't match"
        }
    } else {
        pass1.style.backgroundColor = "white";
        pass2.style.backgroundColor = "white";
        message.style.color = badColor;
        message.innerHTML = " you have to enter at least 6 digit!"
    }

    if (pass1.value != pass2.value) {
        pass2.setCustomValidity("Passwords Don't Match");
    } else {
        pass2.setCustomValidity('');
    }
}



var isMobile = false;

if (
    /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
        navigator.userAgent
    ) ||
    /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
        navigator.userAgent.substr(0, 4)
    )
) {
    isMobile = true;
}

var d = document, $d = $(d),
    w = window, $w = $(w),
    wWidth = $w.width(), wHeight = $w.height(),
    credit = $('.credit > a'),
    particles = $('.particles'),
    particleCount = 0,
    sizes = [
        15, 20, 25, 35, 45
    ],
    colors = [
        '#E7306A', '#000', '#9ED6E5', '#fab400'
    ],

    mouseX = $w.width() / 2, mouseY = $w.height() / 2;

function updateParticleCount() {

    $('.particle-count > .number').text(particleCount);
}

$w
    .on('resize', function () {
        wWidth = $w.width();
        wHeight = $w.height();
    });

$d
    .on('mousemove touchmove', function (event) {
        // event.preventDefault();
        event.stopPropagation();
        mouseX = event.clientX;
        mouseY = event.clientY;
        if (!!event.originalEvent.touches) {
            mouseX = event.originalEvent.touches[0].clientX;
            mouseY = event.originalEvent.touches[0].clientY;
        }
    })
    .on('mousedown touchstart', function (event) {
        if (event.target === credit.get(0)) {
            return;
        }
        mouseX = event.clientX;
        mouseY = event.clientY;
        if (!!event.originalEvent.touches) {
            mouseX = event.originalEvent.touches[0].clientX;
            mouseY = event.originalEvent.touches[0].clientY;
        }
        var timer = setInterval(function () {
            $d
                .one('mouseup mouseleave touchend touchcancel touchleave', function () {
                    clearInterval(timer);
                })
            createParticle(event);
        }, 1000 / 60)

    });


function createParticle(event) {

    var particle = $('<div class="particle"/>'),
        size = sizes[Math.floor(Math.random() * sizes.length)],
        color = colors[Math.floor(Math.random() * colors.length)],
        negative = size / 2,
        speedHorz = Math.random() * 10,
        speedUp = Math.random() * 25,
        spinVal = 360 * Math.random(),
        spinSpeed = ((36 * Math.random())) * (Math.random() <= .5 ? -1 : 1),
        otime,
        time = otime = (1 + (.5 * Math.random())) * 1000,
        top = (mouseY - negative),
        left = (mouseX - negative),
        direction = Math.random() <= .5 ? -1 : 1,
        life = 10;

    particle
        .css({
            height: size + 'px',
            width: size + 'px',
            top: top + 'px',
            left: left + 'px',
            background: color,
            transform: 'rotate(' + spinVal + 'deg)',
            webkitTransform: 'rotate(' + spinVal + 'deg)'
        })
        .appendTo(particles);
    particleCount++;
    updateParticleCount();

    var particleTimer = setInterval(function () {
        time = time - life;
        left = left - (speedHorz * direction);
        top = top - speedUp;
        speedUp = Math.min(size, speedUp - 1);
        spinVal = spinVal + spinSpeed;


        particle
            .css({
                height: size + 'px',
                width: size + 'px',
                top: top + 'px',
                left: left + 'px',
                opacity: ((time / otime) / 2) + .25,
                transform: 'rotate(' + spinVal + 'deg)',
                webkitTransform: 'rotate(' + spinVal + 'deg)'
            });

        if (time <= 0 || left <= -size || left >= wWidth + size || top >= wHeight + size) {
            particle.remove();
            particleCount--;
            updateParticleCount();
            clearInterval(particleTimer);
        }
    }, 1000 / 60);
}

function menuExtra() {

    var menu = document.getElementsByClassName("item");
    let htmlo = document.getElementsByClassName("level-2");


    for (let i = 0; i < menu.length; i++) {

        menu.classList.toggle('mystyle');

    }
}

function accordion() {
    var Accordion = function (el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;

        // Variables privadas
        var links = this.el.find('li a');
        // Evento
        links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
    }

    Accordion.prototype.dropdown = function (e) {
        var $el = e.data.el;
        $this = $(this),
            $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
        }
        ;
    }

    var accordion = new Accordion($('.accordion-slide'), false);
}

function removeQueryNotificationString() {

    urlObject = new URL(window.location.href);

    urlObject.searchParams.delete('notification');
    urlObject.searchParams.delete('notification_message');
    urlObject.searchParams.delete('notification_title');
    urlObject.searchParams.delete('notification_id');

    url = urlObject.href;

    history.pushState(null, null, url);

}

function modifiers() {

    $('.note').slideUp();
    $('#customize').click(function () {

        if ($(this).find('input').is(':checked')) {
            $('.note').removeClass('active');
            $(this).find('input').prop('checked', false);
        } else {
            $('.note').addClass('active');
            $(this).find('input').prop('checked', true);
        }

    })

}


function filterCustom() {
    $('.sublist').click(function () {
        if ($(this).find('input').is(':checked')) {
            $(this).find('input').attr('checked', false);
        } else {
            $(this).find('input').attr('checked', true);
        }
        $(this).find('.checkbox-cust i').toggleClass('active');
    })
}

function wisthlistHeart() {
    $('.product-details .info header div span').on("click", function (e) {
        $('#myImg').toggle('slow');
    });
}

function box(elem) {

    var x = $(elem).val();

    // $('.items-inside-box-content option').removeClass('true');
    // $(elem).addClass('true');

    $('.container').removeClass('active');

    $('body').find("[data-id='" + x + "']").addClass('active');

}

function addActive(elem, id) {
    $(elem).parent().children().removeClass('active');
    $(elem).addClass('active');

    $('.container').removeClass('active');

    $('body').find("[data-id='" + id + "']").addClass('active');

}

function events(elem, id) {

    $('.container-events').removeClass('active');
    $('.event-header-nav div div a').removeClass('selects');
    $(elem).addClass('selects');

    $(elem).parents('main').find("[data-id='" + id + "']").addClass('active');


}


function getOption(elem) {

    $(elem).find('option #select');
    if ($(elem).find('option #ko')) {
        $('main').toggleClass('slide');
    }
}

function guest(elem) {
    $(elem).find('option #selec');
    if ($(elem).find('option #ko')) {
        $('#billing').toggleClass('slide');
    }
}

function clickCheckmark() {
    $('.checkmark-parent2').click(function () {

        $('.checkmark').removeClass('active');
        $('.checkmark').parent('div').removeClass('active');
        $(this).find('input[type="radio"]').prop('checked', true);
        $(this).find('.checkmark').addClass('active');
        $(this).addClass('active')

    });
}

function clickCheckmarkPay() {
    $('.checkmark-parent').click(function () {
        $('.checkmark2').removeClass('active');
        $('.checkmark2').parent('div').removeClass('active');
        $(this).find('input[type="radio"]').prop('checked', true);
        $(this).find('.checkmark2').addClass('active');
        $(this).addClass('active')

    });
}

// function clickChecked() {
//     $('.checkmark').click(function () {
//     });
// }

function disableButton() {
    $('form #submit').prop("disabled", true);
    $('input:radio').click(function () {
        if ($(this).is(':checked')) {
            $('#submit').prop("disabled", false);
        } else {
            if ($('.checks').filter(':checked').length < 1) {
                $('#submit').attr('disabled', true);
            }
        }
    });
}


function hartToggle() {

    $('.hart').click(function (e) {

        $(this).find('i').toggleClass('fa-solid');
    });

}

function ToggleFavoritesInit() {

    $('.favorite-toggle').click(function (e) {
        $(this).find('.fa-heart').toggleClass('fa-solid');

        var elem = $(this);
        var productID = elem.attr('data-pid');

        var url = elem.hasClass('favorite-toggle-active') ? "favorites/remove" : "favorites/add";

        var token = $("meta[name=csrf-token]").attr("content");
        var store_id = $("meta[name=store-id]").attr("content");
        var lang = $("meta[name=lang]").attr("content");

        $.ajax({
            type: "POST",
            url: "/" + store_id + "/" + lang + "/" + url,
            data: {
                productID: productID,
                store: store_id,
                lang: lang,
                _token: token
            },
            success: function (data) {
                elem.toggleClass('favorite-toggle-active');

            }
        });
    });


    $('.favorite-remove').click(function () {
        var elem = $(this);

        var item = elem.parents('.wishlist-item');

        var productID = elem.attr('data-pid');
        var url = "favorites/remove";

        var token = $("meta[name=csrf-token]").attr("content");
        var store_id = $("meta[name=store-id]").attr("content");
        var lang = $("meta[name=lang]").attr("content");

        $.fancyConfirm({
            title: "Are you sure?",
            message: "Are you sure you want to remove this item from wishlist?",
            okButton: "Agree",
            noButton: "Disagree",
            callback: function (value) {
                if (value) {
                    $.ajax({
                        type: "POST",
                        url: "/" + store_id + "/" + lang + "/" + url,
                        data: {
                            productID: productID,
                            store: store_id,
                            lang: lang,
                            _token: token
                        },
                        success: function (data) {
                            item.remove();
                        }
                    });
                }
            },
        });

    });


}

function submitform() {
    $('#mainform').submit();
}

function disablebeforecheck() {
    $('#mainform div').click(function () {
        $('button').prop("disabled", false);
        $(this).parents('.address-container').find('button').addClass('ko');

    })
}


function iconFormSubmit() {

    $('.container .remove-address').click(function () {
        $(this).parents('#icon-form').submit();
    })
}


function updateCartQuantity(cartItemID, direction) {

    var local = $("meta[name=csrf-token]").attr("content");
    var store_id = $("meta[name=store-id]").attr("content");
    var lang = $("meta[name=lang]").attr("content");

    $.ajax({
        type: "POST",
        url: "/" + store_id + "/" + lang + "/cart/update-quantity",
        data: {
            direction: direction,
            cartItemID: cartItemID,
            store: store_id,
            lang: lang,
            _token: local
        },
        success: function (data) {

            location.reload();
            // $('.cart-count').html(data.total);

        }
    });

}

// function EcomQuantityIncrease(elem, id) {
//
//     var input = $(elem).parent(".lol").find("input");
//     var val = parseInt(input.val());
//     if (isNaN(val)) {
//         val = 0;
//     }
//     var newval = val + 1;
//
//     if (newval > parseInt(input.attr("max"))) {
//         newval = val;
//     }
//     input.val(newval).serialize();
//
//     let sideElement = $('.card-row-' + id);
//
//     if (newval > 0) {
//         sideElement.slideDown();
//     }
//
//     sideElement.find('.demo').text(newval);
//     sideElement.attr('data-quantity', newval);
//     // document.getElementById("demo").innerHTML= input.val(newval).serialize();
//
//     initBYBX();
// }


function redirectWithCheckedAddress(url, arr) {
    var items = [];
    arr.forEach((item, index) => {
        items.push({
            key: item,
            value:
                $("input[name=" + item + "]:checked").length > 0
                    ? $("input[name=" + item + "]:checked").val()
                    : $("#" + item).val()
        });
    });
    var str = "";
    var obj = getParams();
    if (!jQuery.isEmptyObject(obj)) {
        var added = true;
        Object.keys(obj).forEach((key, index) => {
            if (!arr.includes(key)) {
                str +=
                    index == 0
                        ? "?" + key + "=" + obj[key]
                        : "&" + key + "=" + obj[key];
                added = added && false;
            }
        });
        items.forEach((item, index) => {
            if (index == 0) {
                str += added
                    ? "?" + item.key + "=" + item.value
                    : "&" + item.key + "=" + item.value;
            } else {
                str += "&" + item.key + "=" + item.value;
            }
        });
    } else {
        items.forEach((item, index) => {
            if (index == 0) {
                str += "?" + item.key + "=" + item.value;
            } else {
                str += "&" + item.key + "=" + item.value;
            }
        });
    }
    return (window.location.href = url + str);
}

function getParams() {
    var url = window.location.href;
    if (url.indexOf("?") == -1) {
        return {};
    }
    var params = {};
    var parser = document.createElement("a");
    parser.href = url;
    var query = parser.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        params[pair[0]] = decodeURIComponent(pair[1]);
    }
    return params;
}

function toggleWishlist(event, elem, item_id, csrf, type) {
    event.preventDefault();
    let rqst_url = "";
    if (type == "add") {
        rqst_url = "/wishlist/add";
    } else if (type == "remove") {
        rqst_url = "/wishlist/remove";
    }

    $.ajax({
        type: "POST",
        url: rqst_url,
        data: {_token: csrf, item_id: item_id},
        success: function (data) {
            if (type == "add") {
                elem.parent()
                    .find(".favorite-add")
                    .removeClass("d-block")
                    .addClass("d-none");
                elem.parent()
                    .find(".favorite-remove")
                    .removeClass("d-none")
                    .addClass("d-block");
            } else {
                elem.parent()
                    .find(".favorite-remove")
                    .removeClass("d-block")
                    .addClass("d-none");
                elem.parent()
                    .find(".favorite-add")
                    .removeClass("d-none")
                    .addClass("d-block");
            }
        },
        error: function (e) {
            console.log(e)
        }
    });
}

function removeFromWishlist(event, item_id, csrf) {
    event.preventDefault();
    $.fancyConfirm({
        title: "Are you sure?",
        message: "Are you sure you want to remove this item from your wishlist?",
        okButton: "Agree",
        noButton: "Disagree",
        callback: function (value) {
            if (value) {
                $.ajax({
                    type: "POST",
                    url: "/wishlist/remove",
                    data: {
                        _token: csrf,
                        item_id: item_id,
                        return_view: true
                    },
                    success: function (data) {
                        // console.log(data.view);
                        $("#wishlist-body").html(data.view);
                    }
                });
            } else {
            }
        }
    });
}


function mouseDown_i() {
    value = isNaN(parseInt(document.getElementById('xxxx').value)) ? 0 : parseInt(document.getElementById('xxxx').value);
    document.getElementById('xxxx').value = value + 1;
    timeout_ = setTimeout(function () {
        mouseDown_i();
    }, 150);
}

function mouseDown_d() {
    value = isNaN(parseInt(document.getElementById('xxxx').value)) ? 0 : parseInt(document.getElementById('xxxx').value);
    value - 1 <= 0 ? document.getElementById('xxxx').value = '1' : document.getElementById('xxxx').value = value - 1;
    timeout_ = setTimeout(function () {
        mouseDown_d();
    }, 150);
}

function mouseUp() {
    clearTimeout(timeout_);
}

function mouseLeave() {
    clearTimeout(timeout_);
}


function ShowLoader() {

    $('body').append('<div class="common-loader-initial common-loader-noanim"></div>');
}

function HideLoader() {
    $('.common-loader-initial').remove();
}

function ShowMessage(Title, Text) {
    $.fancybox.open(
        '<div class="message"><h1>' + Title + "</h1><p>" + Text + "</p></div>"
    );
}

function ShowIframe(link, maxwidth) {
    $.fancybox.open({
        src: link,
        type: "iframe",
        opts: {
            iframe: {
                css: {
                    width: maxwidth
                }
            }
        }
    });
}

function submit() {

    $('#myFormSubmit').submit();

}

function closeFancyBox() {
    parent.jQuery.fancybox.getInstance().close();
}


function search() {
    $(document).ready(function () {

        $(".search a img").click(function () {
            $(".togglesearch").toggleClass('active');
            $("#search #inp").focus();
        });
        $('.fa-xmark').click(function () {
            $(this).parent('.togglesearch').removeClass('active');

        });

    });

}

function closeSearch() {
    $('body').on('click', function (event) {

        if ($(event.target).parents('.search').find('.togglesearch').hasClass("active")) {
            $('.togglesearch').addClass('active');
        } else {
            $('.togglesearch').removeClass('active');
        }
    })
}

function applyVoucher(elem) {

    var input = elem.parent().find("input[name=promo_code]");
    var code = input.val();

    var token = $("meta[name=csrf-token]").attr("content");
    var store_id = $("meta[name=store-id]").attr("content");
    var lang = $("meta[name=lang]").attr("content");

    elem.prop("disabled", true);
    elem.addClass("spin");

    if (elem.attr("data-type") == "remove") {
        var url = "/" + store_id + "/" + lang + "/voucher/remove";
        var opp_type = "apply";
        var input_disabled = false;
    } else {
        var url = "/" + store_id + "/" + lang + "/voucher/apply";
        var opp_type = "remove";
        var input_disabled = true;
    }

    var params = getParams();

    $.ajax({
        type: "POST",
        url: url,
        data: {
            code: code,
            store: store_id,
            lang: lang,
            _token: token,
            ...params,
        },
        success: function (data) {
            // console.log(data);
            if (data.status || data.status == "true") {
                elem.attr("data-type", opp_type);

                elem.find(".btn-text").text(
                    elem.attr("data-" + opp_type + "-text")
                );

                if (!input_disabled) {
                    input.val("");
                }

                input.prop("disabled", input_disabled);

            } else {
                ShowMessage('Invalid coupon code', 'Please enter the correct Coupon code');

            }


            $(".cart-breakdown").html(data.breakdown);


            elem.prop("disabled", false);
            elem.removeClass("spin");

        },
    });
}

function wishList(elem) {


    var list = document.getElementById("toast");
    list.classList.add("show");
    if ($(elem).hasClass('fa-solid')) {
        list.innerHTML = '<i class="far fa-heart remove-wish">&nbsp;&nbsp;</i>Removed from WishList';
    } else {
        list.innerHTML = '<i class="far fa-heart wish">&nbsp;&nbsp;</i> Added to WishList';

    }

    setTimeout(function () {
        list.classList.remove("show");
    }, 3000);
}

function postbox() {

    $('#postBox').click(function () {

        $('.items-inside-box-content #BYBX_form').submit();
    });
}

function addToCart(form, id, event = false) {

    if (event) {
        event.stopPropagation();
        event.preventDefault();
    }
    var store_id = $("meta[name=store-id]").attr("content");
    var lang = $("meta[name=lang]").attr("content");


    var local = $("meta[name=locale]").attr("content");


    $.ajax({
        type: "POST",
        url: "/" + store_id + "/" + lang + "/cart/add",
        data: form,

        success: function (data) {

            if (data.success || data.success == "true") {
                // $.fancybox.open(data.view);

                $("body").append(data.view);
                // $('.cart-count').text(data.count);
                setTimeout(function () {

                    $("body").find(".item-added").remove();
                }, 3000);


            } else {

            }
        },


    });
    if (id == 1) {
        $.fancybox.open(
            '<div class="pop-up-add-balloon"><picture><img src="https://wowsurprises.ae/storage/data/products/102.png?v=204"></picture><sep></sep><h1>Do you want to add a balloon?</h1><a href="https://wowsurprises.ae/ae/en/products/balloons">Click here</a></div>'
        );
    } else if (id == 2) {
        // $.fancybox.open(
        //     '<div class="pop-up-add-balloon"><picture><img src="https://wow.twalab.com/storage/data/products/423.jpeg?v=204"></picture><sep></sep><h1>Do you want to add a wrapping paper?</h1><a href="https://wow.twalab.com/ae/en/products/wrapping-p">Click here</a></div>'
        // );

        $.fancybox.open(
            '<div class="pop-up-add-balloon"><picture><img src="https://wowsurprises.ae/storage/data/products/423.jpeg?v=204"></picture><sep></sep><h1>Do you want to add a wrapping paper?</h1><a href="https://wowsurprises.ae/ae/en/products/wrapping-papers">Click here</a></div>'
        );


    } else {
        return false;
    }
}


function sortByChange() {
    $('#sort-by-id').submit();
}

// function filter() {
//     $(".filter-button-ho").event(function () {
//
//         // $('.sublist .checkbox-cust').addClass('active');
//         var inputs = $('#filter-products-form').serializeArray();
//         var url = window.location.pathname;
//
//         $.ajaxSetup({
//             headers: {
//                 "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
//             },
//         });
//
//
//         $.ajax({
//             type: "POST",
//             url: url,
//             data: inputs,
//             success: function (data) {
//                 $('#product-listing-container').html(data);
//             },
//             error: function (err) {
//                 // console.log(err);
//             },
//         });
//
//         // window.history.pushState('', 'New Page Title', 'http://toneit.test/lb/en/products/list/10/make_up?filter=');
//
//     });
// }


// function pricerange() {
//
//     $("#filter_button").click(function () {
//
//         var inputs = $('#filter-price-form').serializeArray();
//         var url = window.location.pathname;
//
//         $.ajaxSetup({
//             headers: {
//                 "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
//             },
//         });
//
//
//         $.ajax({
//             type: "GET",
//             url: url,
//             data: inputs,
//             success: function (data) {
//                 $('#product-listing-container').html(data);
//             },
//             error: function (err) {
//                 console.log(err);
//             },
//         });
//
//         // window.history.pushState('', 'New Page Title', 'http://toneit.test/lb/en/products/list/10/make_up?filter=');
//
//     });
// }


function insertUrlParam(key, value) {
    if (history.pushState) {
        let searchParams = new URLSearchParams(window.location.search);
        searchParams.set(key, value);
        let newurl =
            window.location.protocol +
            "//" +
            window.location.host +
            window.location.pathname +
            "?" +
            searchParams.toString();
        window.history.pushState({path: newurl}, "", newurl);
    }
}


function myFunction(imgs) {

    var expandImg = document.getElementById("expandedImg");
    var expandImgBackground = document.getElementById("expandedImgBackground");
    expandImg.src = imgs.src;
    expandImgBackground.style.backgroundImage = "url(" + imgs.src + ")";


    expandImg.parentElement.style.display = "block";
    expandImgBackground.style.display = "block";
}

function doItem() {
    $('.menu-responsive').find('.level-m').css('display', 'none');

}

function hover() {
    $(document).ready(function () {
        $(".level-1 li a").mouseover(function () {
            $(".level-1 li .level-2 li").removeClass('Display');
            $(this).parent('li').find('.level-2 li a').first().addClass('Display');
            $(this).parent('li').find('.level-2 li ul').first().addClass('Display');

        });

    });
}


// function doSubmenu() {
//
//     $('.level-2 li a').first().toggleClass('Display');
//     $('.level-2 li ul').first().toggleClass('Display');
//
// }

function removeDisplay() {
    $('.level-2 li a').on("click", function () {
        $('.level-2 li a').removeClass('Display');
        $('.level-2 li ul').removeClass('Display');
        $(this).parent('li').find('ul').addClass('Display');
        $(this).parent('li').find('a').toggleClass('Display');
    })
}


function editFeature() {

    $('#second form .box .sub-box div .edit-feature').on("click", function () {

        $('.account-info .content div #second').removeClass('active');
        $('#' + $(this).data('target')).addClass('active');
    });

}

function removeFeature() {

    $('#second form .box .sub-box div .trash').on("click", function () {
        $('#' + $(this).data('target')).css('visibility', 'hidden');
    });

}

function addAddress() {

    $('#second header button').on("click", function () {
        if ($('#remove-address-first').css('visibility', 'visible')) {
            $('#remove-address-second').css('visibility', 'visible');
        } else {
            $('#remove-address-first').css('visibility', 'visible');
        }
    });


}

function doSub() {

    $('.account-info .content div ul li').on("click", function () {

        $('.account-info .content div main').removeClass('active');
        $('#' + $(this).data('target')).addClass('active');
    });

}

function doOrders() {

    $('#orders table tr td button').on("click", function () {

        $('.account-info .content div main').removeClass('active');
        $('#' + $(this).data('target')).addClass('active');
    });

}

function clickChange() {
    $('div picture').on("click", function () {

        $('.images div picture').removeClass('active');
        $(this).addClass('active');
    }, function () {

        $('.images div picture').removeClass('block');
        $(this).addClass('block');
    });
}

function doAccordion() {
    $('.accordion .expandable').slideUp(0);
    $('.accordion a').on('click', function () {
        $(this).parents('.accordion').find('span img').toggleClass('expanded');
        $(this).parents('.accordion').find('.expandable').slideToggle(300);

    });

}

function addclassOnClick() {
    $('.create-your-box-variation div').on('click', function () {

        $('.create-your-box-variation div').removeClass('selected');
        $(this).addClass('selected');
    });
}

function addclassOnClick2() {
    $('.event-type picture').on('click', function () {
        $('.event-type picture input').attr('checked', false);
        $(this).find('input').attr('checked', true);
        $('.event-type').find('picture').removeClass('selected');
        $(this).addClass('selected');
    });
}

function filterClick() {
    $('.filter').on('click', function () {
        // $('.filter').parent('.filter-container').toggleClass('none');
        $('.filter').parent('.filter-container-flat').toggleClass('expand');
        $(this).parent('.balloons-filter-container').find('.main-container').toggleClass('expand');
    });

}

function burger() {
    let menuBtn = $('.menu-btn');
    let menuDiv = $('.burger-display');
    let menuOpen = false;
    menuBtn.click(function () {

        if (!menuOpen) {
            menuBtn.addClass('open');
            menuDiv.addClass('open');
            menuOpen = true;


        } else {
            menuBtn.removeClass('open');
            menuDiv.removeClass('open');
            menuOpen = false;
        }

    });
}


function doParallax() {
    $(".parallax, .parallaxText").css({
        transition: "0s !important",
        "transition-delay": "0s !important",
        "transition-timing-function": "linear"
    });
    if (isMobile != true) {
        $(".parallax").each(function () {
            var offset = $(this).offset();
            var positionY = (window.pageYOffset - offset.top) / 2;
            if (positionY < 0) {
                positionY = 0;
            }
            if (positionY < 100) {
                $(this)
                    .find(".parallaxText")
                    .css("opacity", "" + (100 - positionY) / 100);
            }
            $(this).css("background-position", "center +" + positionY + "px");
        });
    }
}

function InitializeMenuScroll() {
    var scroll_start = 0;
    var startchange = $(".menu-marker");
    var offset = startchange.offset();
    //  var slideshowOffset = $("#slideshow").offset();
    if (startchange.length >= 0 && $(document).scrollTop() - offset.top >= 0) {
        $("nav.main").addClass("compact");
        $("nav.main-second").removeClass("compact");
        $("ul.main-menu.level-2").css("top", "70px");
        $(".togglesearch").addClass("compact");
    } else {
        $("nav.main").removeClass("compact");
        $("nav.main-second").addClass("compact");
        $("ul.main-menu.level-2").css("top", "100px");
        $(".togglesearch").removeClass("compact");
    }
}

function MenuScroll() {
    var scroll_start = 0;
    var startchange = $(".menu-marker");
    var offset = startchange.offset();
    //  var slideshowOffset = $("#slideshow").offset();
    if (startchange.length >= 0 || $(document).scrollTop() - offset.top >= 0) {
        $("div.drop-down-menu").addClass("compact");
    } else {
        $("div.drop-down-menu").removeClass("compact");
    }
}

// function custom(){
//     $('.main-menu.level-4').append('<a id="innerLink">View More</a>');
// }


// animation balloons


function initBYBX() {
    //
    // $(window).click(function() {
    //     $('.filters-opened-container').addClass('hidden');
    //     $('.filters-closed-container').removeClass('hidden');
    // });
    // $('.filters-opened-container, .filters-opened-container *').click(function(event){
    //     event.stopPropagation();
    // });
    // $('.ecom_quantity a').click(function() {
    //     refreshBYBX();
    // });
    //
    // $('.ecom_quantity input').on('input',function() {
    //     if($(this).val()==""){ $(this).val(0); }
    //     $(this).val(parseInt($(this).val()));
    //     if($(this).val() > parseInt($(this).attr('max'))){ $(this).val(parseInt($(this).attr('max'))); }
    //     refreshBYBX();
    // });

    var remove_popup = true;
    refreshBYBX(remove_popup);

    //New Functions for Mobile
    // $('.view-selected-pieces').click(function (){
    //     $(this).find('.text').toggleClass('hide');
    //     $(this).toggleClass('active');
    //     $('.box-top').toggleClass('active');
    // });


}

function refreshBYBX(remove_popup = true) {

    var totalweight = 0;
    var percentage = 0


    //Changed from id to class display updates on mobile too
    $('.bybx_item').each(function (index, elem) {

        $('.bybx_left_' + $(elem).attr('data-id')).find('.count span').html($(elem).find('input.qty').val());

        if ($(elem).find('input.qty').val() > 0) {
            $('.bybx_left_' + $(elem).attr('data-id')).slideDown();
        } else {
            $('.bybx_left_' + $(elem).attr('data-id')).slideUp();
        }
        totalweight = totalweight + $(elem).find('input.qty').val() * parseInt($(elem).attr('data-weight'));
    })
    percentage = parseInt(totalweight / parseInt($('#max_weight').val()) * 100);

    if (percentage == 0) {
        $('.view-selected-pieces').addClass('hide');
    } else {
        $('.view-selected-pieces').removeClass('hide');
    }

    $('.bybx_progress .bar .left').css({'width': percentage + '%'});

    if (percentage > 50) {
        $('.bybx_progress .bar .percentage').text('');
        $('.bybx_progress .bar .left').text(percentage + '%');
    } else {
        $('.bybx_progress .bar .percentage').text(percentage + '%');
        $('.bybx_progress .bar .left').text('');
    }
    $('.bybx_progress .bar .percentage').css({'width': 100 - percentage + '%'});
    remaining_weight = parseFloat($('#max_weight').val()) - parseFloat(totalweight);

    if (remaining_weight < 0) {
        remaining_weight = 0
    }

    $('.remaining_weight-span').each(function () {
        $(this).text(parseInt(remaining_weight));
    });

    updateQuantitiesBYBX(remaining_weight, remove_popup);
    updateLinksBYBX();
    setFieldMax(remaining_weight);


    $('.card-row').each(function (index, element) {
        var elem = $(element);
        if (elem.attr('data-quantity') == 0) {
            elem.slideUp(0);
        }
    });

    window.history.pushState({}, '', location.protocol + '//' + location.host + location.pathname + '?' + $('#BYBX_form').serialize());

}

function showPassword() {
    $(".toggle-password").click(function () {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
}

function submitQuery() {

    $('.filterForm').on('submit', function (e) {
        e.preventDefault();
        var formData = $(this).serialize();


        var fullUrl = window.location.href.split('filter')[0];

        var separator = "?";
        if (fullUrl.includes('?')) {
            separator = "&";
        }

        var link = fullUrl + separator + formData;
        // var finalUrl = fullUrl+"&"+formData;

        window.history.pushState({}, '', link);


        // $.get(link, function (data) {
        //     $('body').html(data);
        //     HideLoader();
        // $('body').scrollTo('.filters-closed-container', 200, {offset: -350});

        window.location = link;

        return false;

    })

}

function removeFromBox(id) {

    updateProductQuantity(id, 0);
    updateSidebbar();
    $('.lol .wrapclick').removeClass('active');
    window.history.pushState({}, '', location.protocol + '//' + location.host + location.pathname + '?' + $('#BYBX_form').serialize());

}

function EcomQuantityIncrease(elem, id) {

    var input = $(elem).parent(".lol").find("input");

    var val = parseInt(input.val());

    if (isNaN(val)) {
        val = 0;
    }
    var newval = val + 1;

    updateProductQuantity(id, newval);
    updateSidebbar();

    window.history.pushState({}, '', location.protocol + '//' + location.host + location.pathname + '?' + $('#BYBX_form').serialize());

}

function updateProductQuantity(id, qty) {

    // var currentURL = $(location).attr('href');

    $('input[name = cms_attributes_quantity' + id + ']').val(qty);

}

function updateSidebbar() {

    var inputs = $('#BYBX_form').serializeArray();

    var totalPrice = 0;
    var totalQuantity = 0;
    inputs.forEach(function (input) {

        if (input.name.includes('cms_attributes_quantity')) {

            var productId = parseInt(input.name.replace('cms_attributes_quantity', ''));
            var qty = parseInt(input.value);


            let sideElement = $('.card-row-' + productId).first();
            // console.log(sideElement.length);
            if (qty > 0) {

                sideElement.addClass('active');
            } else {
                sideElement.removeClass('active');

            }

            $('.card-row-' + productId).attr('data-quantity', qty);
            totalQuantity += qty;

            sideElement.find('.demo').text(qty);
            // sideElement.find('#vip').val(qty);
        }
    });
    totalPrice = calculatePrice();
    let lastItem = location.pathname.substring(location.pathname.lastIndexOf('/') + 1);

    var data = $('.limit').attr('id');

    if (totalQuantity < 1) {
        $('.addtocartinvisible').css('cursor', 'not-allowed');
        $('.add-wrap-link').css('transform', 'scale(0)');
        $('#postBox').css('background', '#80808073');
        $('#postBox').css('pointer-events', 'none');
    } else {
        $('#postBox').css('pointer-events', 'unset');
        $('.add-wrap-link').css('transform', 'scale(1)');
        $('.add-wrap-link').css('transition', '.5s');
        $('#postBox').css('background', 'none');
        $('.addtocartinvisible').css('cursor', 'pointer');
    }
    if (lastItem == 2) {

        if (totalQuantity > data - 1) {

            $('.container .lol a').css('background', '#80808073');
            $('.container .lol a').css('pointer-events', 'none');
        } else {
            $('.container .lol a').css('background', 'white');
            $('.container .lol a').css('pointer-events', 'unset');
        }

        formatPrice($('#subtotal').find('.value'), totalPrice);


        //console.log(inputs);
    } else if (lastItem == 5) {
        if (totalQuantity > data - 1) {

            $('.container .lol a').css('background', '#80808073');
            $('.container .lol a').css('pointer-events', 'none');
        } else {
            $('.container .lol a').css('background', 'white');
            $('.container .lol a').css('pointer-events', 'unset');
        }

        formatPrice($('#subtotal').find('.value'), totalPrice);


        //console.log(inputs);
    } else if (lastItem == 6) {
        if (totalQuantity > data - 1) {

            $('.container .lol a').css('background', '#80808073');
            $('.container .lol a').css('pointer-events', 'none');
        } else {
            $('.container .lol a').css('background', 'white');
            $('.container .lol a').css('pointer-events', 'unset');
        }

        formatPrice($('#subtotal').find('.value'), totalPrice);


        //console.log(inputs);
    } else {
        formatPrice($('#subtotal').find('.value'), totalPrice);
    }
}



function formatPrice(valueContainer, total) {

    let symbol = valueContainer.attr('data-currency-symbol');

    let rate = valueContainer.attr('data-currency-rate');

    var totalPriceFormated = (parseFloat(total) * parseFloat(rate));


    valueContainer.text(totalPriceFormated.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' ' + symbol);

}


function calculatePrice() {
    var total = 0;
    $('.inside-box-content .card-row.active').each(function () {
        total += parseFloat($(this).attr('data-unit-price')) * parseFloat($(this).attr('data-quantity'));
    });
    return total;
}

// function next() {
//     $('.nextclass').click(function () {
//         $(this).addClass('active');
//         $(this).parent('.add-to-cart').find('main').addClass('active');
//     })
// }


function chosenItems() {

    $('.chosen-icon').click(function () {
        $(this).parent('.chosen-items').toggleClass('expand');
    })
}

function appendElement() {

    $('.customzie-item').click(function () {

        $(this).parents('.card-row').find('.customzie-text').toggleClass('active').focus();

        if ($(this).parents('.card-row').find('input').prop('checked')) {
            $(this).parents('.card-row').find('input').attr('checked', false);
        } else {
            $(this).parents('.card-row').find('input').attr('checked', true);
        }
    });
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


function bybxFilter(elem) {

    var element = $(elem).parent("#formo").find("input[name=filter]").serializeArray();
    var store_id = $("meta[name=store-id]").attr("content");
    var lang = $("meta[name=lang]").attr("content");

    $.ajax({
        type: "GET",
        url: "?" + element,
        data: {
            filter: element,
            store: store_id,
            lang: lang,


        },
        success: function (data) {
            console.log(data)
            $("body").html(data);

        }
    });


}
function expandDiv(elem){
    $(elem).parents('.container').find('article').toggleClass('expand');
}
function onclickClass(){
    $('.filter-container-flat-responsive .filter-responsive').click(function(){
        $(this).parent('.filter-container-flat-responsive').toggleClass('active');
    });
}
// function checkIfExist(){
//     var sawan = document.querySelectorAll('small');
//     var sku =  document.querySelectorAll('data-sku');
//
//     $(document).ready(function() {
//
//     });
// }
//
//
// function applyVoucher(elem) {
//
//     var input = elem.parent().find("input[name=promo_code]");
//     var code = input.val();
//
//     var token = $("meta[name=csrf-token]").attr("content");
//     var store_id = $("meta[name=store-id]").attr("content");
//     var lang = $("meta[name=lang]").attr("content");
//
//     elem.prop("disabled", true);
//     elem.addClass("spin");
//
//     if (elem.attr("data-type") == "remove") {
//         var url = "/" + store_id + "/" + lang + "/voucher/remove";
//         var opp_type = "apply";
//         var input_disabled = false;
//     } else {
//         var url = "/" + store_id + "/" + lang + "/voucher/apply";
//         var opp_type = "remove";
//         var input_disabled = true;
//     }
//
//     var params = getParams();
//
//     $.ajax({
//         type: "POST",
//         url: url,
//         data: {
//             code: code,
//             store: store_id,
//             lang: lang,
//             _token: token,
//             ...params,
//         },
//         success: function (data) {
//             // console.log(data);
//             if (data.status || data.status == "true") {
//                 elem.attr("data-type", opp_type);
//
//                 elem.find(".btn-text").text(
//                     elem.attr("data-" + opp_type + "-text")
//                 );
//
//                 if (!input_disabled) {
//                     input.val("");
//                 }
//
//                 input.prop("disabled", input_disabled);
//
//             } else {
//                 ShowMessage('Invalid coupon code', 'Please enter the correct Coupon code');
//
//             }
//
//
//             $(".cart-breakdown").html(data.breakdown);
//
//
//             elem.prop("disabled", false);
//             elem.removeClass("spin");
//
//         },
//     });
// }
